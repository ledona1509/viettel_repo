-- Tạo partition cho bảng RPT_SALE_STATISTIC
-- B.1. Tạo 1 bảng tạm (DONATEST) bằng cách chép câu lệnh SQL từ bảng PRT_SALE_STATISTIC
-- bỏ 1 số phần không cần thiết đi.
-- Lưu ý: nhớ đổi tên khóa chính, đổi tên bảng ở phần comment
CREATE TABLE donatest
   (	RPT_SALE_STATISTIC_ID NUMBER(20,0) NOT NULL ENABLE, 
	SHOP_ID NUMBER(20,0), 
	STAFF_ID NUMBER(20,0), 
	PARENT_STAFF_ID NUMBER(20,0), 
	PRODUCT_ID NUMBER(20,0), 
	DAY_PLAN NUMBER(20,2), 
	DAY_QUANTITY NUMBER(20,2), 
	MONTH_PLAN NUMBER(20,2), 
	MONTH_QUANTITY NUMBER(20,2), 
	CREATE_DATE DATE DEFAULT sysdate, 
	UPDATE_DATE DATE, 
	CONVFACT NUMBER(10,0), 
	PARENT_PRODUCT_ID NUMBER(20,0), 
	DAY_PG_QUANTITY NUMBER(20,0), 
	MONTH_PG_QUANTITY NUMBER(20,0), 
	RPT_IN_DATE DATE DEFAULT sysdate,
	CONSTRAINT RPT_SALE_STATISTIC_DONA_PK PRIMARY KEY (RPT_SALE_STATISTIC_ID)
   )
  TABLESPACE VNM_DATA 
partition by range (RPT_IN_DATE)
(
PARTITION DATA201310 VALUES LESS THAN (TO_DATE('01/11/2013', 'DD/MM/YYYY')),
PARTITION DATA201311 VALUES LESS THAN (TO_DATE('01/12/2013', 'DD/MM/YYYY')),
PARTITION DATA201312 VALUES LESS THAN (TO_DATE('01/01/2014', 'DD/MM/YYYY')),
PARTITION DATA201401 VALUES LESS THAN (TO_DATE('01/02/2014', 'DD/MM/YYYY')),
PARTITION DATA201402 VALUES LESS THAN (TO_DATE('01/03/2014', 'DD/MM/YYYY')),
PARTITION DATA201403 VALUES LESS THAN (TO_DATE('01/04/2014', 'DD/MM/YYYY')),
PARTITION DATA201404 VALUES LESS THAN (TO_DATE('01/05/2014', 'DD/MM/YYYY')),
PARTITION DATA201405 VALUES LESS THAN (TO_DATE('01/06/2014', 'DD/MM/YYYY')),
PARTITION DATA201406 VALUES LESS THAN (TO_DATE('01/07/2014', 'DD/MM/YYYY')),
PARTITION DATA201407 VALUES LESS THAN (TO_DATE('01/08/2014', 'DD/MM/YYYY')),
PARTITION UNKNOWN VALUES LESS THAN (MAXVALUE)
);
COMMENT ON COLUMN DONATEST.PRODUCT_ID IS 'sản phẩm kế hoạch DVT : thùng/két';
 
COMMENT ON COLUMN DONATEST.DAY_PLAN IS 'kế hoạch theo ngày của product_id';

COMMENT ON COLUMN DONATEST.DAY_QUANTITY IS 'Thực hiện trong ngày, thùng hoặc két';

COMMENT ON COLUMN DONATEST.MONTH_PLAN IS 'Kế hoạch tháng  của product_id';

COMMENT ON COLUMN DONATEST.MONTH_QUANTITY IS 'Thực hiện trong tháng  của product_id';

COMMENT ON COLUMN DONATEST.CONVFACT IS 'giá trị quy đổi từ product_id sang parent_product_id
vd : CONVFACT = 24 y nghia 1 thung(product_id) = 24 lon(parent_product_id)';

COMMENT ON COLUMN DONATEST.PARENT_PRODUCT_ID IS 'sản phẩm gốc Lon/chai ';

COMMENT ON COLUMN DONATEST.DAY_PG_QUANTITY IS 'Thực hiện trong ngày dành cho NVPG bán lẻ, lưu số lượng lon/chai ứng với parent_product_id bán được trong ngày';

COMMENT ON COLUMN DONATEST.MONTH_PG_QUANTITY IS 'Thực hiện trong tháng dành cho NVPG bán lẻ, lưu số lượng lon/chai ứng với parent_product_id bán được trong tháng';

COMMENT ON COLUMN DONATEST.RPT_IN_DATE IS 'Ngày tổng hợp dữ liệu';


-- B.2. Đổi tên bảng: Đổi tên bảng gốc thành 1 bảng tạm (sau này sẽ xóa bảng này)
-- đổi tên bảng mới thành tên bảng gốc
alter table rpt_sale_statistic rename to draft_table;
alter table donatest rename to rpt_sale_statistic;

-- B.3. Chép dữ liệu từ bảng gốc sang bảng mới
insert into rpt_sale_statistic select * from draft_table;

-- B.4. Sửa lại tên khóa chính và tên index
alter table draft_table rename constraint RPT_SALE_STATISTIC_PK to draft_table_PK;
alter table rpt_sale_statistic rename constraint RPT_SALE_STATISTIC_DONA_PK to RPT_SALE_STATISTIC_PK;

ALTER INDEX RPT_SALE_STATISTIC_PK RENAME TO draft_table_pk;
ALTER INDEX RPT_SALE_STATISTIC_DONA_PK RENAME TO RPT_SALE_STATISTIC_PK;

-- B.4. Đổi tên trigger bảng cũ, thêm trigger bảng mới tạo
alter trigger RPT_SALE_STATISTIC_TRIG rename to draft_table_trig;

CREATE OR REPLACE 
TRIGGER RPT_SALE_STATISTIC_TRIG
	AFTER INSERT OR DELETE OR UPDATE ON RPT_SALE_STATISTIC 
	FOR EACH ROW 
	DECLARE 
	  v_shop_id staff.shop_id%TYPE; 
	  v_staff_id STAFF_POSITION_LOG.staff_id%TYPE; 
	  v_action database_log.action%TYPE; 
	  v_table_id database_log.table_id%TYPE; 
	BEGIN 
		v_staff_id:=null; 
    v_shop_id:=null; 
    IF INSERTING THEN  
		  v_table_id:=:new.RPT_SALE_STATISTIC_ID; 
      v_staff_id:=:new.staff_id;
      v_shop_id:=:new.shop_id;
		  v_action:=1; 
		ELSIF UPDATING THEN 
		  v_table_id:=:new.RPT_SALE_STATISTIC_ID; 
      v_staff_id:=:new.staff_id;
      v_shop_id:=:new.shop_id;
		  v_action:=2; 
		ELSIF DELETING THEN 
		  v_table_id:=:old.RPT_SALE_STATISTIC_ID; 
      v_staff_id:=:old.staff_id;
      v_shop_id:=:old.shop_id;
		  v_action:=3; 
		END IF; 
	 
	insert into database_log (database_log_id,table_name,table_id, action,  staff_id, shop_id, create_date) values 
		(database_log_seq.nextval,'RPT_SALE_STATISTIC',v_table_id,v_action,v_staff_id,v_shop_id,sysdate); 
END;