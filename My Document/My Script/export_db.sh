#@author: NaLD@viettel.com.vn

#Thong so cau hinh
ipOri=10.30.174.212
portOri=1521
sidOri=kunkun
psswdsysOri=Sys12345678
pathDumpdirOri=/home/oracle/dumpdir/
nameDumpdirOri=DUMPDIR

echo -n "Username database can export: "
read userdbOri
echo -n "Password database can export: "
read psswddbOri
echo "Bat dau export database"

sqlplus / as sysdba <<EOF
  CREATE OR REPLACE directory $nameDumpdirOri AS $pathDumpdirOri;
  GRANT read, write ON directory $nameDumpdirOri to system, $userdbOri;
  EXIT
EOF

# Thuc hien export & nen database
cd $pathDumpdirOri
if [ -f "$userdbOri.tar.gz" ];
then
rm -rf $userdbOri.tar.gz
fi
if [ -f "$userdbOri.dmp" ];
then
rm -rf $userdbOri.dmp
fi
expdp $userdbOri/$psswddbOri DIRECTORY=$nameDumpdirOri DUMPFILE=$userdbOri.dmp SCHEMAS=$userdbOri LOGFILE=$userdbOri.log
tar -czf $userdbOri.tar.gz $userdbOri.dmp
echo "Export database thanh cong. Duong dan: $pathDumpdirOri$userdbOri.tar.gz"