-- SCRIPT cap nhat sequence cho tat ca cac bang
  -- Cach 1
set serveroutput on; 
declare
  temp number;
  c number;
  query1 varchar(2000);
  tableName varchar(100);
  seqName varchar(100);
  CURSOR c_TableName
  IS
     select OBJECT_NAME from user_objects where object_type = 'TABLE' ;
  CURSOR c_SequenceName
  IS
    select sequence_name from USER_SEQUENCES;
begin
  -- Xoa nhung sequence khong dung den
  OPEN c_SequenceName;
  LOOP
    BEGIN
      fetch c_SequenceName into seqName;
      EXIT WHEN c_SequenceName%NOTFOUND;

      query1 := 'select count(1) from user_objects where object_type = ''TABLE'' 
                and upper(object_name) = (select UPPER(SUBSTR(sequence_name, 0, LENGTH(sequence_name) - 4)) from USER_SEQUENCES 
                where upper(sequence_name) = upper(''' || seqName || '''))';
      
      EXECUTE IMMEDIATE query1 into temp;

      IF temp = 0 THEN
        query1 := 'DROP SEQUENCE ' || seqName;
        EXECUTE IMMEDIATE query1;
        dbms_output.put_line('DROP SEQUENCE ' || seqName);
      END IF;

      exception WHEN OTHERS THEN NULL; 
    END;
  END LOOP;
  CLOSE c_SequenceName;

  -- Danh lai so sequence
  OPEN c_TableName;
  LOOP
    begin
      fetch c_TableName into tableName;
      EXIT WHEN c_TableName%NOTFOUND;
      
      query1 := 'select nvl(max(' || tableName || '_ID),0) from ' || tableName || ' where ' || tableName || '_ID < 100000000';
      
      EXECUTE IMMEDIATE query1 into temp;
      -- IF temp = 0 THEN
      --   query1 := 'select nvl(max(ID),0) from ' || tableName || ' where ID < 100000000';
       
      --   EXECUTE IMMEDIATE query1 into temp;
      -- END IF;
      
      query1 := 'select count(*) from user_sequences where sequence_name = ''' || tableName || '_SEQ''';
     
      EXECUTE IMMEDIATE query1 into c;
    
      if c > 0  then
           query1 := 'DROP SEQUENCE ' || tableName || '_SEQ';
           EXECUTE IMMEDIATE query1;
           dbms_output.put_line('drop');
      end if;
    
      query1 := 'CREATE SEQUENCE  ' || tableName ||'_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH ' || (temp+1) || ' NOCACHE  NOORDER  NOCYCLE';
      EXECUTE IMMEDIATE query1;
        
      exception WHEN OTHERS THEN NULL; 
    end;
  END LOOP;
  CLOSE c_TableName;
  dbms_output.put_line('done');
end;

  -- Cach 2
alter sequence seq_name
increment by 124;

select seq_name.nextval from dual;

alter sequence seq_name
increment by 1;

-- Lay nextval cua sequence
SELECT last_number
  FROM user_sequences
 WHERE sequence_name = 'SEQUENCE_NAME';
-- Tao sequence
  CREATE SEQUENCE  TABLENAME_SEQ  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
-- Kiem tra Partition cua bang
SELECT object_name,
  subobject_name
FROM user_objects
WHERE object_type = 'TABLE PARTITION'
AND object_name  IN ('ACTION_LOG');

-- Xoa file Partition cu
declare
str varchar(512);
begin
for x in(  SELECT object_name,
      subobject_name
    FROM user_objects
    WHERE object_type = 'TABLE PARTITION'
    AND object_name  IN ('PO_AUTO') and subobject_name not in('DATA201310','DATA201311','DATA201312'))  -- Loai bo nhung Partition khong muon bo
    loop
      begin
        str := 'alter table ' || x.object_name || ' drop partition ' || x.subobject_name;
        EXECUTE IMMEDIATE str;
      end;
    end loop;
end;

-- Cat chuoi SPLIT trong Oracle
SELECT REGEXP_SUBSTR('19;20','[^;]+', 1, level) FROM dual
  CONNECT BY REGEXP_SUBSTR('19;20', '[^;]+', 1, level) IS NOT NULL
;

-- Cong chuoi listagg
SELECT LISTAGG(dsHoaDon.ORDER_NUMBER, ', ') WITHIN GROUP (ORDER BY dsHoaDon.ORDER_NUMBER)
FROM dsHoaDon
where dsHoaDon.delivery_id = t4.maNVGH

-- User khoa lock bang
  select owner||'.'||object_name obj
  ,oracle_username||' ('||s.status||')' oruser
  ,os_user_name osuser
  ,l.process unix
  ,''''||s.sid||','||s.serial#||'''' ss
  ,r.name rs
  ,to_char(s.logon_time,'yyyy/mm/dd hh24:mi:ss') time
  from v$locked_object l
  ,dba_objects o
  ,v$session s
  ,v$transaction t
  ,v$rollname r
  where l.object_id = o.object_id
  and s.sid=l.session_id
  and s.taddr=t.addr
  and t.xidusn=r.usn
  order by osuser, ss, obj;

  select session_id "sid",SERIAL#  "Serial",
      substr(object_name,1,20) "Object",
        substr(os_user_name,1,10) "Terminal",
        substr(oracle_username,1,10) "Locker",
        nvl(lockwait,'active') "Wait",
        decode(locked_mode,
          2, 'row share',
          3, 'row exclusive',
          4, 'share',
          5, 'share row exclusive',
          6, 'exclusive',  'unknown') "Lockmode",
        OBJECT_TYPE "Type"
  FROM
    SYS.V_$LOCKED_OBJECT A,
    SYS.ALL_OBJECTS B,
    SYS.V_$SESSION c
  WHERE
    A.OBJECT_ID = B.OBJECT_ID AND
    C.SID = A.SESSION_ID
  ORDER BY 1 ASC, 5 Desc
  ;

-- Kill session lock bang
alter system kill session 'sid,pid';
 
-- Lay Log cac Jobs dang chay
select * from USER_SCHEDULER_JOB_RUN_DETAILS;
select * from USER_SCHEDULER_JOB_RUN_DETAILS where job_name = 'JOB_P_RPT_SALE_STATISTIC' order by Log_Date desc;

-- Lenh tao dll cua 1 job co san
select dbms_metadata.get_ddl( 'PROCOBJ', 'TEN_JOB') from dual;

-- Lenh tao Job
  BEGIN
    DBMS_SCHEDULER.CREATE_JOB ( JOB_NAME => 'JOB_P_DESTROY_UNAPPROVED_ORDER',
    JOB_TYPE => 'PLSQL_BLOCK',
    JOB_ACTION => 'BEGIN P_DESTROY_UNAPPROVED_ORDER(SYSDATE-1,1); END;',
    START_DATE => SYSTIMESTAMP,
    REPEAT_INTERVAL => 'FREQ=DAILY; BYHOUR=8;BYMINUTE=25;BYSECOND=0;',
    ENABLED => false );
  END;

-- Lay cot danh partition
  select * from DBA_PART_KEY_COLUMNS
  where owner = 'SABECO_DEMO'
  and name = 'RPT_STOCK_TOTAL_DAY'
  ;

-- Tao bang (CLONE) tu bang co san, khong chua du lieu
create table TEN_BANG_CAN_TAO as select * from TEN_BANG_GOC where 1=0;

-- Lay 12 thang trong nam hien tai
  SELECT ADD_MONTHS (TRUNC(SYSDATE , 'Year'), 1 * level -1) thang FROM Dual
  CONNECT BY level <= MONTHS_BETWEEN(LAST_DAY(ADD_MONTHS(TRUNC(sysdate , 'Year'),11)), TRUNC(sysdate , 'Year')) + 1;

-- Tao file bao cao AWR Report
  1. Khai bao bien moi truong (. oraenv -> kunkun)
  2. Ket noi vao DB :
  sqlplus / as sysdba
  3. Lenh tao bao cao : 
  @$ORACLE_HOME/rdbms/admin/awrrpt.sql

-- Lenh xem session/service chay cau query
  select * from v$sql where lower(sql_text) like '%customer%' and sql_fulltext like 'update%status%1%';

-- Rebuild / cap nhat lai sequence
  SET serveroutput on; 
  DECLARE
  maxID number;
  isExistSequence number;
  qGetMaxTableID varchar(200);
  qExistSequence varchar(200);
  qDropSequence varchar(200);
  qCreateSequence varchar(200);
  tableName varchar(100);

  -- lay ra ds ten tat ca cac table
  CURSOR c1
  IS
  SELECT OBJECT_NAME FROM user_objects WHERE object_type = 'TABLE' ;
  begin
  OPEN c1;
    LOOP
    begin
      -- lay ra ten tung bang
      fetch c1 into tableName;
      EXIT WHEN c1%NOTFOUND;
      dbms_output.put_line('Ten table: ' || tableName);
      -- lay ra max ID bang
      qGetMaxTableID := 'SELECT nvl(max(' || tableName || '_ID),0) FROM ' || tableName || ' WHERE ' || tableName || '_ID < 100000000';
      
      EXECUTE IMMEDIATE qGetMaxTableID into maxID;
      dbms_output.put_line('Max ' || tableName || '_ID: ' || maxID);
      -- IF maxID = 0 THEN
        -- qGetMaxTableID := 'SELECT nvl(max(ID),0) FROM ' || tableName || ' WHERE ID < 100000000';
       
        -- EXECUTE IMMEDIATE qGetMaxTableID into maxID;
      -- END IF;
      
      -- lay ra so luong sequence cua bang TABLENAME_SEQUENCE, thuong la 1
      qExistSequence := 'SELECT count(*) FROM user_sequences WHERE sequence_name = ''' || tableName || '_SEQ''';
       
      EXECUTE IMMEDIATE qExistSequence into isExistSequence;
      
      -- neu da ton tai sequence thi drop bo di
      if isExistSequence > 0  then
        dbms_output.put_line('Thuc hien xoa sequence ' || tableName || '_SEQ');
        qDropSequence := 'DROP SEQUENCE ' || tableName || '_SEQ';
        EXECUTE IMMEDIATE qDropSequence;
      end if;
      
      -- tao lai sequence, voi vi tri bat dau = max ID + 1
      dbms_output.put_line('Tao sequence ' || tableName || '_SEQ co chi so bat dau = ' || (maxID+1));
      qCreateSequence := 'CREATE SEQUENCE  ' || tableName ||'_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH ' || (maxID+1) || ' NOCACHE  NOORDER  NOCYCLE';
      EXECUTE IMMEDIATE qCreateSequence;
      
      exception
      WHEN OTHERS THEN NULL; 
      end;
      dbms_output.put_line('-----------------------');
     END LOOP;
     CLOSE c1;
     dbms_output.put_line('Xong');
  end;
   
-- Lay danh sach tat ca cac bang, lay danh sach tat ca cac cot trong bang
declare
TYPE EmpCurTyp  IS REF CURSOR;
query1 varchar(200);
tmp2 USER_TAB_COLUMNS%ROWTYPE;
tmp EmpCurTyp;
cursor c_lst_table
is
  SELECT table_name FROM user_tables ORDER BY table_name;

begin
  DBMS_OUTPUT.ENABLE(100000000000000);
  for x in c_lst_table
  loop
    begin
      dbms_output.put_line('');
      dbms_output.put_line(x.table_name);
      dbms_output.put_line('-----------');
      query1 := 'SELECT * FROM USER_TAB_COLUMNS WHERE table_name = ''' || x.table_name || '''';
      OPEN tmp FOR query1;
      LOOP
        FETCH tmp INTO tmp2;
          EXIT WHEN tmp%NOTFOUND;
          dbms_output.put_line(tmp2.column_name);
      END LOOP;
      CLOSE tmp;
    end;
  end loop;
end;

-- Sap xep ten Vietnamese
  ORDER BY NLSSORT(p.product_name,'NLS_SORT=vietnamese')

-- Chuyen tu chuoi sang ngay
  TO_DATE('31/01/2014', 'DD/MM/YYYY')

-- Chuyen ngay sang gio phut giay
  TO_CHAR(date,'yyyy/mm/dd hh24:mi:ss')

-- Lay comment cua 1 cot trong bang
  select comments
  from user_col_comments
  where table_name = 'PRO_INFO'
  and column_name = 'SHOP_ID';

-- Lay tablespace trong db
  SELECT username, profile, default_tablespace, temporary_tablespace
  FROM dba_users;

-- Cach lay user dang nhap vao he thong sai username / password
  http://blog.yannickjaquier.com/oracle/who-is-locking-your-accounts-ora-01017-and-ora-28000-errors.html

-- Recompile tat ca cac doi tuong trong User
  EXECUTE DBMS_UTILITY.COMPILE_SCHEMA('USER_NAME');

-- Lay cau truc bang
  SELECT table_name, column_name, data_type, data_length FROM USER_TAB_COLUMNS WHERE table_name = 'SALE_DAY';

-- Them cot vao trong bang
ALTER TABLE table_name ADD column_name column-definition;

-- Them comment vao trong bang
COMMENT ON COLUMN TABLE_NAME.COLUMN_NAME IS 'comment content';

-- Duong dan, dung luong datafile tuong ung cac tablespace
SELECT file_name, tablespace_name, ROUND(bytes/1024000) MB
FROM dba_data_files
ORDER BY 1;

-- Kiem tra dung luong tablespace
select df.tablespace_name "Tablespace",
totalusedspace "Used MB",
(df.totalspace - tu.totalusedspace) "Free MB",
df.totalspace "Total MB",
round(100 * ( (df.totalspace - tu.totalusedspace)/ df.totalspace))
"Pct. Free"
from
(select tablespace_name,
round(sum(bytes) / 1048576) TotalSpace
from dba_data_files 
group by tablespace_name) df,
(select round(sum(bytes)/(1024*1024)) totalusedspace, tablespace_name
from dba_segments 
group by tablespace_name) tu
where df.tablespace_name = tu.tablespace_name ;

-- Kiem tra dung luong temporary tablespace
select tablespace_name, ROUND(tablespace_size/1024000) Size_MB 
, ROUND(allocated_space/1024000) Used_MB 
, ROUND(free_space/1024000) Free_MB 
from dba_temp_free_space;

-- Cap phat datafile moi cho tablespace
ALTER TABLESPACE VNM_DATA
ADD DATAFILE '/home/oracle/app/datafiles/vnm/VNM_DATA_013.dbf'
SIZE 10M;

-- Extent datafile cho tablespace
ALTER DATABASE
DATAFILE '/home/oracle/app/datafiles/vnm/VNM_DATA_013.dbf'
RESIZE 8G;