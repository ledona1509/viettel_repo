﻿--truy van cau lenh cao tai
select a.sql_id, a.sql_text, a.ratio, a.executions
from  
(
  select sql_text,sql_id, buffer_gets / executions ratio, executions
  from v$sql
  where executions > 10
  order by executions desc
) a
where rownum <= 100;

--chay procedure
VARIABLE resultSet  REFCURSOR
EXEC PKG_SHOP_REPORT.DIMUONVESOM_REPORT(:resultSet, 15,78,null,to_date('01/01/2013','dd/mm/yyyy'),to_date('31/01/2013','dd/mm/yyyy'),8,0,15,0);
PRINT :resultSet

--pl/sql
set serveroutput on; 
DECLARE
  CURSOR cur
  IS
    select id, address from location where address_Text is null;
  addressText varchar2(2024);
BEGIN
  dbms_output.enable(null);
  FOR par IN cur
  LOOP
	addressText := lower(par.address);
	IF length(addressText) > 0 THEN
		addressText := REGEXP_REPLACE(addressText,'[àáạảãâầấậẩẫăằắặẳẵà]','a');
	END IF;
	
	update location set address_Text = addressText where id = par.id;
  END LOOP;
END;

--dieu chinh sequence
ALTER SEQUENCE seq_name INCREMENT BY xxx;

--tim constraint cua bang STOCK_TRANS_DETAIL
select *
from
    all_constraints
where
    r_constraint_name in
    (select       constraint_name
    from
       all_constraints
    where
       table_name='STOCK_TRANS_DETAIL')
;   
--drop contraints
alter table STOCK_TRANS drop constraint ST_STAFF_FREIKEY;
--truncate
truncate table STAFF drop storage;

--tim field thay doi trong trigger
set serveroutput on; 
declare
    CURSOR tableDef
    IS
      SELECT TO_CHAR(column_name) column_name,
        TO_CHAR(data_type) data_type
      FROM user_tab_cols
      WHERE table_name = 'CUSTOMER';
     smt     CLOB;
BEGIN
   
    smt  := '';
    
    FOR par  IN tableDef
    LOOP
     smt := smt || ' if ((:new.' || par.column_name ||' IS NULL AND :old.' || par.column_name ||' IS NOT NULL) OR (:new.' || par.column_name ||' IS NOT NULL AND :old.' || par.column_name ||' IS NULL) OR :new.' || par.column_name ||' <> :old.' || par.column_name ||') then';
     smt := smt || ' v_description := v_description || ''old.' || par.column_name ||' : '' || :old.' || par.column_name ||' || '' new.' || par.column_name ||' : '' || :new.' || par.column_name ||';';
     smt := smt || ' end if;';
    END LOOP;
    dbms_output.put_line(smt);
END;
--cap nhat sequence
set serveroutput on; 
declare
  temp number;
  c number;
  query1 varchar(200);
  temp1 varchar(100);
  CURSOR c1
  IS
     select OBJECT_NAME from user_objects where object_type = 'TABLE' ;
begin
OPEN c1;
   LOOP
      begin
        fetch c1 into temp1;
        EXIT WHEN c1%NOTFOUND;
        
        query1 := 'select nvl(max(' || temp1 || '_ID),0) from ' || temp1 || ' where ' || temp1 || '_ID < 100000000';
        
        EXECUTE IMMEDIATE query1 into temp;
        IF temp = 0 THEN
          query1 := 'select nvl(max(ID),0) from ' || temp1 || ' where ID < 100000000';
         
          EXECUTE IMMEDIATE query1 into temp;
        END IF;
        
        query1 := 'select count(*) from user_sequences where sequence_name = ''' || temp1 || '_SEQ''';
       
        EXECUTE IMMEDIATE query1 into c;
      
        if c > 0  then
             query1 := 'DROP SEQUENCE ' || temp1 || '_SEQ';
             EXECUTE IMMEDIATE query1;
             dbms_output.put_line('drop');
        end if;
      
        query1 := 'CREATE SEQUENCE  ' || temp1 ||'_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH ' || (temp+1) || ' NOCACHE  NOORDER  NOCYCLE';
        EXECUTE IMMEDIATE query1;
        
      exception
        WHEN OTHERS THEN
          NULL; 
      end;
   END LOOP;
   CLOSE c1;
   dbms_output.put_line('done');
end;

--drop sequence
declare
  temp number;
  c number;
  query1 varchar(200);
  begin
  for x in (select sequence_name from user_sequences)
  loop
      query1 := 'DROP SEQUENCE ' || x.sequence_name;
             EXECUTE IMMEDIATE query1;
  end loop;
  end;
  
  --tao lai sequence
  set serveroutput on; 
declare
  temp number;
  c number;
  query1 varchar(200);
  temp1 varchar(100);
  CURSOR c1
  IS
     select OBJECT_NAME from user_objects where object_type = 'TABLE' ;
begin
OPEN c1;
   LOOP
      begin
        fetch c1 into temp1;
        EXIT WHEN c1%NOTFOUND;
        
      
        query1 := 'CREATE SEQUENCE  ' || temp1 ||'_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE';
        EXECUTE IMMEDIATE query1;
        
      exception
        WHEN OTHERS THEN
          NULL; 
      end;
   END LOOP;
   CLOSE c1;
   dbms_output.put_line('done');
end;
 -- xoa partition
 
--cho phep chuyen row giua cac partition
alter table sales_order_detail enable row movement;

--chinh sequence
ALTER SEQUENCE RPT_STAFF_SALE_seq CACHE 1000 ORDER; 

--kiem tra trang thai job
select * from USER_SCHEDULER_JOB_RUN_DETAILS where job_name = 'JOB_RPT_STOCK_TOTAL_DAY';

SELECT * FROM dba_scheduler_jobs WHERE owner ='VINAMILK_RELEASE';and  job_name = 'JOB_RPT_STAFF_SALE_DETAIL_MD' order by Next_Run_Date desc;
SELECT * FROM dba_scheduler_job_log WHERE owner ='VINAMILK_RELEASE' and status='FAILED'  order by Log_Date desc;

select * from USER_SCHEDULER_JOB_RUN_DETAILS where job_name = 'JOB_RPT_STAFF_SALE_DETAIL_MT' order by Log_Date desc;

--chay job
BEGIN
  DBMS_SCHEDULER.CREATE_JOB ( JOB_NAME => 'JOB_RESET_PRESCRIPTION_CODE',
  JOB_TYPE => 'STORED_PROCEDURE',
  JOB_ACTION => 'RESET_PRESCRIPTION_CODE',
  START_DATE => SYSTIMESTAMP,
  REPEAT_INTERVAL => 'FREQ=MONTHLY; BYMONTHDAY=1;',
  ENABLED => true );
END;

--export job
select dbms_metadata.get_ddl( 'PROCOBJ', ‘JOB_RPT_KPI_MB2’) from dual;

--add them partition
ALTER TABLE SALE_ORDER_detail ADD partition DATA201312 VALUES less than (to_date('01/12/2013','dd/mm/yyyy'));

ALTER TABLE database_log ADD partition PRODUCT_INFO_MAP_PAR VALUES ('PRODUCT_INFO_MAP');

ALTER TABLE database_log MODIFY partition DATA20131018 ADD SUBPARTITION DISPLAY_PD_GROUP_DTL_1018 VALUES ('DISPLAY_PD_GROUP_DTL');

--TIM NHUNG THANG FOREIGN KEY MA KO DANH INDEX
SELECT * FROM (
SELECT c.table_name, cc.column_name, cc.position column_position
FROM   user_constraints c, user_cons_columns cc
WHERE  c.constraint_name = cc.constraint_name
AND    c.constraint_type = 'R'
MINUS
SELECT i.table_name, ic.column_name, ic.column_position
FROM   user_indexes i, user_ind_columns ic
WHERE  i.index_name = ic.index_name
)
ORDER BY table_name, column_position;

--XOA ARCHIVE LOG BANG RMAN
Dang nhap oracle
rman target /
delete archivelog until time 'sysdate-1';
--kiem tra user log bang
select owner||'.'||object_name obj
 ,oracle_username||' ('||s.status||')' oruser
 ,os_user_name osuser
 ,l.process unix
 ,''''||s.sid||','||s.serial#||'''' ss
 ,r.name rs
 ,to_char(s.logon_time,'yyyy/mm/dd hh24:mi:ss') time
 from v$locked_object l
 ,dba_objects o
 ,v$session s
 ,v$transaction t
 ,v$rollname r
 where l.object_id = o.object_id
 and s.sid=l.session_id
 and s.taddr=t.addr
 and t.xidusn=r.usn
 order by osuser, ss, obj;
