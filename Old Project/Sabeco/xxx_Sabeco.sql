-- Lay danh sach NVBH thuoc SHOP_ID truyen vao
SELECT s.STAFF_ID 
FROM STAFF s
join channel_type ct on s.STAFF_TYPE_ID = ct.channel_type_id
WHERE s.SHOP_ID IN ( SELECT   SHOP_ID
                   FROM   SHOP
                   CONNECT BY   PRIOR SHOP_ID = PARENT_SHOP_ID
                   START WITH   SHOP_ID = 1)
  and ct.type = 2  and ct.object_type = 1
 
-- Kiem tra job dang chay bang RPT_SALE_STATISTIC
select * from USER_SCHEDULER_JOB_RUN_DETAILS
where trunc(log_date) = trunc(sysdate)
and job_name = 'JOB_P_RPT_SALE_STATISTIC'
ORDER BY req_start_date desc

-- Lay tu ngay den ngay
select start_date + level - 1 as day
from (select TO_DATE('17/12/2013','DD/MM/YYYY') start_date, TO_DATE('27/12/2013','DD/MM/YYYY') end_date
                    from dual)
connect by level <= (trunc(TO_DATE('27/12/2013','DD/MM/YYYY') - TO_DATE('17/12/2013','DD/MM/YYYY'))) + 1
;

-- Script xoa ke hoach tieu thu trong nam 2014 (bảng SALE_PLAN va SALE_PLAN_VERSION)
DELETE FROM sale_plan_version
WHERE sale_plan_version_id IN (
SELECT spv.sale_plan_version_id
FROM sale_plan_version spv
JOIN sale_plan sp ON sp.sale_plan_id = spv.sale_plan_id
WHERE sp.type = 2 AND sp.object_type = 1 AND sp.year = EXTRACT(YEAR FROM sysdate)
);
COMMIT;
DELETE FROM sale_plan
WHERE sale_plan_id IN(
SELECT sp.sale_plan_id
FROM sale_plan sp
WHERE sp.type = 2 AND sp.object_type = 1 AND sp.year = EXTRACT(YEAR FROM sysdate)
);
COMMIT;