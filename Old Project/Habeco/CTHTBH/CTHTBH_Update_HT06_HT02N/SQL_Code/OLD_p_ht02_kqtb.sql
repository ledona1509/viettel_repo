 PROCEDURE P_HT02_KQTB(res OUT SYS_REFCURSOR , lsObjectID CLOB, proInfoID NUMBER, periodID NUMBER, objecttype NUMBER)
  AS
  -- @author: MiNTT
  -- HT02 - Bao cao Tong hop thuc hien chuong trinh
  -- objecttype: Loai doi tuong: 1: customer, 2: NPP
  sql1 CLOB;
  sql2 CLOB;

  CURSOR c_dsSPThucHien
  IS
    SELECT  p.product_id
    FROM product p
    WHERE p.product_id IN ( select sd.product_id FROM  pro_info i
                            JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
                            JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                            JOIN product p ON p.product_id = sd.product_id
                            WHERE i.pro_info_id = proInfoID AND s.type IN (1,2) AND sd.type = 1
                          )
    ORDER BY p.sort_order
    ;
  CURSOR c_dsSPHoTro
  IS
    SELECT DISTINCT proinfo.product_info_id, p.sort_order, giftd.product_id
    from pro_cus_reward rew
    JOIN pro_cus_reward_detail rewd ON rew.pro_cus_reward_id = rewd.pro_cus_reward_id
    JOIN pro_gift_detail giftd ON giftd.pro_gift_detail_id = rewd.pro_gift_detail_id
    JOIN  product_info proinfo ON proinfo.product_info_id = giftd.type_support --AND proinfo.type = 1-- and proinfo.object_type = 4
    JOIN pro_cus_map m ON m.pro_cus_map_id = rew.pro_cus_map_id
    JOIN product p ON p.product_id = giftd.product_id
    WHERE m.pro_info_id = proInfoID AND rew.pro_period_id = periodID AND (proinfo.type != 1 OR proinfo.object_type != 4)
    ORDER BY proinfo.product_info_id, p.sort_order
        
        
    ;
  BEGIN
    DBMS_OUTPUT.ENABLE(100000000000000);
    
    sql1:=sql1 || 'WITH';
    sql1:=sql1 || '    dsThamGia AS(';
    sql1:=sql1 || '      SELECT cm.pro_cus_map_id, cm.object_id, cr.level_auto';
    sql1:=sql1 || '      FROM pro_cus_map cm JOIN pro_cus_reward cr ON cr.pro_cus_map_id = cm.pro_cus_map_id';
    sql1:=sql1 || '      WHERE 1=1';
    sql1:=sql1 || '      AND cr.pro_period_id = '|| periodID ||'';
    sql1:=sql1 || '        AND cm.pro_info_id = '|| proInfoID ||' AND cm.object_type = '|| objecttype || ' AND cm.object_id IN (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB('''|| lsObjectID ||''')))';
    sql1:=sql1 || '    )';
    sql1:=sql1 || '    ,slTH AS(';
    sql1:=sql1 || '      SELECT cp.pro_cus_map_id, cp.product_id, cp.quantity soLuong';
    sql1:=sql1 || '      FROM pro_cus_process cp WHERE cp.pro_period_id = '|| periodID ||'';
    sql1:=sql1 || '    )';
    sql1:=sql1 || '    ,ht AS (';
    sql1:=sql1 || '      SELECT  cr.pro_cus_map_id, gd.product_id';
    sql1:=sql1 || '              ,crd.quantity_auto soLuong';
    sql1:=sql1 || '              ,crd.unit_cost_auto donGia';
    sql1:=sql1 || '              ,crd.amount thanhTien';
    --sql1:=sql1 || '              ,proinfo.product_info_id, p.sort_order';
    sql1:=sql1 || '      FROM pro_info i';
    sql1:=sql1 || '      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id';
    sql1:=sql1 || '      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id';
    sql1:=sql1 || '      JOIN pro_cus_map cm ON cm.pro_info_id = i.pro_info_id';
    sql1:=sql1 || '      JOIN pro_cus_reward cr ON cr.pro_cus_map_id = cm.pro_cus_map_id';
    sql1:=sql1 || '      LEFT JOIN pro_cus_reward_detail crd ON crd.pro_cus_reward_id = cr.pro_cus_reward_id';
    sql1:=sql1 || '      LEFT JOIN pro_gift_detail gd ON gd.pro_gift_detail_id = crd.pro_gift_detail_id';
    --sql1:=sql1 || '      JOIN  product_info proinfo ON proinfo.product_info_id = gd.type_support ';
    --sql1:=sql1 || '      LEFT JOIN product p ON p.product_id = gd.product_id';
    sql1:=sql1 || '      WHERE 1=1';
    sql1:=sql1 || '        AND ((cr.result_auto = 1 AND cr.result_manual IS NULL) OR (cr.result_manual = 1))';
    sql1:=sql1 || '        AND i.pro_info_id = '|| proInfoID ||' AND cr.pro_period_id = '|| periodID ||'';
    --sql1:=sql1 || '      ORDER BY proinfo.product_info_id,  p.sort_order';
    sql1:=sql1 || '    )';
    sql1:=sql1 || ' SELECT ';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '    CASE WHEN t.tmp = 0 THEN c.customer_name';
      sql1:=sql1 || '                WHEN t.tmp = 1 THEN npp.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 3 THEN cn.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 7 THEN kv.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 15 THEN ho.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 31 THEN hbc.shop_name';
    ELSE
      sql1:=sql1 || '    CASE WHEN t.tmp = 0 THEN npp.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 1 THEN cn.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 3 THEN kv.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 7 THEN ho.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 15 THEN hbc.shop_name';
    END IF;
    sql1:=sql1 || '          END tenDV';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN c.address ELSE NULL END diaChi';
    ELSE
      sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN npp.address ELSE NULL END diaChi';
    END IF;
     IF (objecttype = 1) THEN
      sql1:=sql1 || '   , (select db.level_auto from dsThamGia db where t.customer_id = db.object_id) soSuatDat';
    ELSE
      sql1:=sql1 || ' ,  (select db.level_auto from dsThamGia db where t.nppID = db.object_id) soSuatDat';
    END IF;
   
    FOR x IN c_dsSPThucHien
    LOOP
      sql1:=sql1 || '          , t.thucHien_'|| x.product_id;
    END LOOP;
     FOR x IN c_dsSPHoTro
    LOOP
      sql1:=sql1 || '          , t.soLuong_'|| x.product_id;
      sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN t.donGia_'|| x.product_id ||' ELSE NULL END donGia_'|| x.product_id ;
      sql1:=sql1 || '          , t.thanhTien_'|| x.product_id;
    END LOOP;
   -- sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN t.soLuong ELSE NULL END soLuong';
   -- sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN t.donGia ELSE NULL END donGia';
    
    sql1:=sql1 || '          , t.thanhTien, t.tongTienHoTro, t.tmp';
    sql1:=sql1 || '    FROM (';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '        SELECT t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID, t.customer_id  ';
    ELSE
      sql1:=sql1 || '        SELECT t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID ';
    END IF;
  
    FOR x IN c_dsSPThucHien
    LOOP
      sql1:=sql1 || '              , SUM(t.thucHien_'|| x.product_id ||') thucHien_'|| x.product_id;
    END LOOP;
    FOR x IN c_dsSPHoTro
    LOOP
      sql1:=sql1 || '              , SUM(t.soLuong_'|| x.product_id ||') soLuong_'|| x.product_id ||', SUM(t.donGia_'|| x.product_id ||') donGia_'|| x.product_id ||', SUM(t.thanhTien_'|| x.product_id ||') thanhTien_'|| x.product_id;
    END LOOP;
    sql1:=sql1 || '              , SUM(t.soLuong) soLuong, SUM(t.donGia) donGia, SUM(t.thanhTien) thanhTien, SUM(t.tongTienHoTro) tongTienHoTro';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '              , GROUPING_ID (t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID, t.customer_id) tmp';
    ELSE
      sql1:=sql1 || '              , GROUPING_ID (t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID) tmp';
    END IF;
    sql1:=sql1 || '        FROM (';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '            SELECT hbc.shop_id hbcID, ho.shop_id hoID, kv.shop_id kvID, cn.shop_id cnID, npp.shop_id nppID, c.customer_id';
    ELSE
      sql1:=sql1 || '            SELECT hbc.shop_id hbcID, ho.shop_id hoID, kv.shop_id kvID, cn.shop_id cnID, npp.shop_id nppID';
    END IF;
   --    sql1:=sql1 || '                  ,db.pro_cus_map_id';
    FOR x IN c_dsSPThucHien
    LOOP
      sql1:=sql1 || '                  ,(SELECT t.soLuong FROM slTH t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id = '|| x.product_id ||') thucHien_'|| x.product_id;
    END LOOP;
    FOR x IN c_dsSPHoTro
    LOOP
    sql1:=sql1 || '                  ,(SELECT SUM(t.soLuong) FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id = '|| x.product_id ||') soLuong_'|| x.product_id;
    sql1:=sql1 || '                  ,(SELECT t.donGia FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id = '|| x.product_id ||' and rownum = 1) donGia_'|| x.product_id;
    sql1:=sql1 || '                  ,(SELECT SUM(t.thanhTien) FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id = '|| x.product_id ||') thanhTien_'|| x.product_id;
    END LOOP;
    sql1:=sql1 || '                  ,(SELECT t.soLuong FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id IS NULL and rownum = 1) soLuong';
    sql1:=sql1 || '                  ,(SELECT SUM(t.donGia) FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id IS NULL) donGia';
    sql1:=sql1 || '                  ,(SELECT SUM(t.thanhTien) FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id IS NULL) thanhTien';
    sql1:=sql1 || '                  ,(SELECT t.total_amount FROM pro_cus_reward t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.pro_period_id = '|| periodID ||') tongTienHoTro';
    sql1:=sql1 || '            FROM dsThamGia db';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '            JOIN customer c ON c.customer_id = db.object_id';
      sql1:=sql1 || '            JOIN shop npp ON npp.shop_id = c.shop_id';
    ELSE
      sql1:=sql1 || '            JOIN shop npp ON npp.shop_id = db.object_id';
    END IF;
    sql1:=sql1 || '            JOIN shop cn ON cn.shop_id = npp.parent_shop_id';
    sql1:=sql1 || '            JOIN shop kv ON kv.shop_id = cn.parent_shop_id';
    sql1:=sql1 || '            JOIN shop ho ON ho.shop_id = kv.parent_shop_id';
    sql1:=sql1 || '            JOIN shop hbc ON hbc.shop_id = ho.parent_shop_id';
    sql1:=sql1 || '            )t';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '        GROUP BY ROLLUP (t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID, t.customer_id)';
    ELSE
      sql1:=sql1 || '        GROUP BY ROLLUP (t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID)';
    END IF;
    sql1:=sql1 || '        )t';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '    LEFT JOIN customer c ON c.customer_id = t.customer_id';
    END IF;
    sql1:=sql1 || '    LEFT JOIN shop npp ON npp.shop_id = t.nppID';
    sql1:=sql1 || '    LEFT JOIN shop cn ON cn.shop_id = t.cnID';
    sql1:=sql1 || '    LEFT JOIN shop kv ON kv.shop_id = t.kvID';
    sql1:=sql1 || '    LEFT JOIN shop ho ON ho.shop_id = t.hoID';
    sql1:=sql1 || '    LEFT JOIN shop hbc ON hbc.shop_id = t.hbcID';
    sql1:=sql1 || '    WHERE t.hbcID IS NOT NULL';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '    ORDER BY  ho.shop_name, kv.shop_name, cn.shop_name, npp.shop_name,  c.customer_name';
    ELSE
      sql1:=sql1 || '    ORDER BY  ho.shop_name, kv.shop_name, cn.shop_name, npp.shop_name';
    END IF;
    DBMS_OUTPUT.PUT_LINE(sql1);
    OPEN res FOR (sql1);

  END P_HT02_KQTB;