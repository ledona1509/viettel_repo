PROCEDURE PRO_HT06_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  -- Tinh thuong HT06 luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  IS
    CURSOR c_DsSPDieuKien
    IS
      select sd.product_id
      from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      where s.pro_info_id = proinfoid and sd.type = 1 and sd.product_id is not NULL
      union
      select p.product_id
      from product p where p.convfact > 1 and p.status = 1 and p.cat_id = 1
      and exists (select 1 from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                  where s.pro_info_id = proinfoid and sd.type = 2);

    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_DieuKien
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Lay danh sach SP trong dieu kien huong ho tro
      -- Neu ton tai sd.type = 2 -> lay tat ca san pham
      ,spDK AS(
        select sd.product_id
        from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        where s.pro_info_id = proinfoid and sd.type = 1 and sd.product_id is not NULL
        union
        select p.product_id
        from product p where p.convfact > 1 and p.status = 1 and p.cat_id = 1
        and exists (select 1 from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                    where s.pro_info_id = proinfoid and sd.type = 2)
      )
      ,dsNgay AS(
        SELECT TO_CHAR(tmp2.thang, 'MM/YYYY') thangStr, tmp2.thang, tmp2.soNgayTrongThang,
              CASE
                  WHEN TRUNC(start_date, 'MM') = TRUNC(end_date, 'MM') THEN TRUNC(end_date) - TRUNC(start_date) + 1
                  WHEN TRUNC(tmp2.thang, 'MM') = TRUNC(end_date, 'MM') THEN EXTRACT(DAY FROM tmp2.thang)
                  ELSE (TRUNC(LAST_DAY(tmp2.thang)) - TRUNC(tmp2.thang) + 1)
              END AS soNgayCanTinh
        FROM
            (SELECT
                    CASE
                        WHEN (TRUNC(start_date, 'MM') = TRUNC(end_date, 'MM')) THEN end_date
                        WHEN (TRUNC(tmp.month) = TRUNC(start_date)) THEN tmp.month
                        WHEN (TRUNC(tmp.month, 'MM') = TRUNC(end_date, 'MM')) THEN end_date
                        ELSE TRUNC(tmp.month, 'MM')
                    END AS thang,
                    TO_NUMBER(TO_CHAR(LAST_DAY(tmp.month), 'DD')) AS soNgayTrongThang,
                    start_date, end_date
            FROM
                (SELECT add_months( start_date, level-1 ) AS month, start_date, end_date
                FROM
                    (select pp.from_date start_date, pp.to_date end_date
                    from pro_period pp where pp.pro_period_id = periodid)
                CONNECT BY level <= months_between(TRUNC(end_date,'MM'), TRUNC(start_date,'MM')) + 1
                ) tmp
            ) tmp2
      )
      ,slKeHoach AS(
        select NVL(SUM(t.khKetThung), -1) khKetThung
        --, SUM(t.khLit) khLit
        from (
          SELECT tmp.product_id, SUM((tmp.khKetThung/tmp.soNgayTrongThang)*tmp.soNgayCanTinh) AS khKetThung, SUM((tmp.khLit/tmp.soNgayTrongThang)*tmp.soNgayCanTinh) AS khLit
          FROM
            (SELECT sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh, SUM(sp.quantity) AS khKetThung, SUM(sp.quantity_lit) AS khLit
            FROM sale_plan sp, dsNgay tmp
            WHERE sp.object_type = 1 AND sp.type = 2
            and sp.object_id = (select cm.object_id from pro_cus_map cm where cm.pro_cus_map_id = procusmapid)
            AND sp.month = extract(MONTH FROM tmp.thang) AND sp.year = extract(YEAR FROM tmp.thang)
            and sp.product_id in (select t.product_id from spDK t)
            GROUP BY sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh
            ) tmp
          WHERE 1=1
            and exists (select 1 from pro_structure s where s.pro_info_id = proinfoid and s.register_type = 1)
          GROUP BY tmp.product_id
          )t
      )
      ,slThucTe AS(
        -- SELECT SUM(rpt.xuat_ban - rpt.nhap_tra_hang) xuatBan, SUM(rpt.nhap_sbc) nhapTra
        select NVL(sum(thucHien), -1) thucHien
        from(
          select case when (select s.type from pro_structure s where s.pro_info_id = proinfoid) = 6 then (rpt.xuat_ban - rpt.nhap_tra_hang)
                      when (select s.type from pro_structure s where s.pro_info_id = proinfoid) = 5 then rpt.nhap_sbc
                else 0
                end thucHien
          FROM rpt_stock_total_day rpt
          WHERE 1=1
            AND rpt.shop_id IN (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid AND object_type = 2)
            AND rpt.rpt_in_date >= (SELECT t.fromDate FROM tg t) AND rpt.rpt_in_date < (SELECT t.toDate FROM tg t) + 1
            AND rpt.product_id in (select t.product_id from spDK t)
          )
      )
      ,mucHoTro AS(
        select NVL(level_number, -1) giaTri
        from (
          select sd.level_number
          FROM pro_cus_process cp
          JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.type = 3 
            and (((s.type = 5 and cp.quantity_return >= sd.min_limit and cp.quantity_return <= sd.max_limit)
                or(s.type = 5 and cp.quantity_return >= sd.min_limit and sd.max_limit is null))
                or
                ((s.type = 6 and cp.quantity >= sd.min_limit and cp.quantity <= sd.max_limit)
                or(s.type = 6 and cp.quantity >= sd.min_limit and sd.max_limit is null)))
            and rownum = 1
            )
      )
      select case when s.register_type = 0 and (select t.giaTri from mucHoTro t) != -1 then 1
            when s.register_type = 1 
              and ((NVL((select t.thucHien from slThucTe t), 0)/NVL((select t.khKetThung from slKeHoach t), 1) >= s.register_quantity)
                  OR ((select t.thucHien from slThucTe t) !=-1 AND (select t.khKetThung from slKeHoach t) = -1))
              and (select t.giaTri from mucHoTro t) != -1 then 1
            when s.register_type = 2 and (select t.thucHien from slThucTe t) >= s.register_quantity and (select t.giaTri from mucHoTro t) != -1 then 1
          else 0 end ketQua
          ,case when (select t.thucHien from slThucTe t) = -1 and (select t.khKetThung from slKeHoach t) = -1 then 0
              when (select t.thucHien from slThucTe t) != -1 and (select t.khKetThung from slKeHoach t) = -1 then 100
          else (select t.thucHien from slThucTe t)/NVL((select t.khKetThung from slKeHoach t), 1) 
          end phanTramHoanThanh
          ,(select t.thucHien from slThucTe t) sanLuongThucHien
      from pro_structure s
      where s.pro_info_id = proinfoid 
      ;
    
    -- Tinh he so
    CURSOR c_MucHoTro
    IS
      select NVL(level_number, -1) giaTri
      from (
        select sd.level_number
        FROM pro_cus_process cp
        JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.type = 3 
          and (((s.type = 5 and cp.quantity_return >= sd.min_limit and cp.quantity_return <= sd.max_limit)
              or(s.type = 5 and cp.quantity_return >= sd.min_limit and sd.max_limit is null))
              or
              ((s.type = 6 and cp.quantity >= sd.min_limit and cp.quantity <= sd.max_limit)
              or(s.type = 6 and cp.quantity >= sd.min_limit and sd.max_limit is null)))
          and rownum = 1
          );


    -- Thong tin ho tro theo suat
    CURSOR c_HoTro(v_level_number IN NUMBER)
    IS
      SELECT gd.pro_gift_detail_id, gd.unit_cost donGia, 0 soLuong
      FROM pro_structure s
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
      JOIN product_info pi ON pi.product_info_id = gd.type_support
      WHERE s.pro_info_id = proinfoid AND pi.type = 1 AND gd.level_number = v_level_number
      ;

    -- Lay ra han muc, han muc co the = NULL hoac > 0 (khong co han muc = 0)
    CURSOR c_HanMuc
    IS
      SELECT s.quota giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;

    CURSOR c_LoaiDangKy
    IS
      SELECT s.register_type giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;
    
    rewardID NUMBER(20,0);
    rewardDetailID NUMBER(20,0);
    soLuong NUMBER(20,0);
    donGia NUMBER(20,0);
    result NUMBER(1,0) := 0;
    totalAmount NUMBER(20,0);
    isAchieved NUMBER(20, 0) := 0;
    phanTramHoanThanh NUMBER(20,2);
    sanLuongThucHien NUMBER(20,0);
    mucHoTro NUMBER(10,0) := 0;
    loaiDangKy NUMBER(1,0) := 0;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT06_CUS_REWARD started');
    OPEN c_MucHoTro;
    FETCH c_MucHoTro INTO mucHoTro;
    CLOSE c_MucHoTro;

    totalAmount := 0;
    DBMS_OUTPUT.put_line ('Chay theo tien trinh');
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    
    -- Neu con han muc
    OPEN c_DieuKien;
    FETCH c_DieuKien INTO result, phanTramHoanThanh, sanLuongThucHien;
    CLOSE c_DieuKien;

    -- Suat dat
    IF (result = 1) THEN
      OPEN c_MucHoTro;
      FETCH c_MucHoTro INTO mucHoTro;
      CLOSE c_MucHoTro;

      DBMS_OUTPUT.put_line ('Dat thuong');
      isAchieved := 1;
      
      FOR v IN c_HoTro(mucHoTro)
      LOOP
        totalAmount := totalAmount + v.soLuong*v.donGia;
        INSERT INTO pro_cus_reward_detail (pro_cus_reward_detail_id, pro_cus_reward_id, pro_gift_detail_id, quantity_auto, unit_cost_auto, amount,                  create_date, create_user)
                          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID,        v.pro_gift_detail_id, v.soLuong,      v.donGia,       v.soLuong*v.donGia,     sysdate,     username);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
      END LOOP;
    ELSE
      DBMS_OUTPUT.put_line ('Khong dat thuong');
    END IF;

    INSERT INTO pro_cus_reward(pro_cus_reward_id, pro_cus_map_id, total_amount, LEVEL_AUTO,   CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
                        VALUES(rewardID,          procusmapid,    totalAmount,  1,            sysdate,      username,   periodid,       isAchieved, proinfoid);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    DBMS_OUTPUT.put_line ('Ket qua ' || isAchieved);

    OPEN c_LoaiDangKy;
    FETCH c_LoaiDangKy INTO loaiDangKy;
    CLOSE c_LoaiDangKy;

    IF(loaiDangKy = 1) THEN
      DBMS_OUTPUT.put_line ('Loai ho tro: % san luong');

      update pro_cus_reward set register_quantity = phanTramHoanThanh where pro_cus_reward_id = rewardID;
      DBMS_OUTPUT.put_line ('updated 1 row into PRO_CUS_REWARD field register_quantity = ' || phanTramHoanThanh);

    ELSIF (loaiDangKy = 2) THEN
      DBMS_OUTPUT.put_line ('Loai ho tro: san luong');
      
      update pro_cus_reward set register_quantity = sanLuongThucHien where pro_cus_reward_id = rewardID;
      DBMS_OUTPUT.put_line ('updated 1 row into PRO_CUS_REWARD field register_quantity = ' || sanLuongThucHien);
               
    END IF;

    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT06_CUS_REWARD finished');
  END PRO_HT06_CUS_REWARD;