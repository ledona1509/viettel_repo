PROCEDURE PRO_HT02_N_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  -- Tinh thuong HT02_N luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  IS
    -- Xet dieu kien Dat hay Khong dat cho Customer
    CURSOR c_DieuKien4KhachHang
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Lay danh sach SP trong dieu kien huong ho tro
      -- Neu ton tai sd.type = 2 -> lay tat ca san pham
      ,spDK AS(
        select sd.product_id
        from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        where s.pro_info_id = proinfoid and sd.type = 1 and sd.product_id is not NULL
        union
        select p.product_id
        from product p where p.convfact > 1 and p.status = 1 and p.cat_id = 1
        and exists (select 1 from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                    where s.pro_info_id = proinfoid and sd.type = 2)
      )
      ,xuatBan AS(
        SELECT sod.product_id, SUM(sod.quantity) giaTri
        FROM sale_order so
        JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
        WHERE so.approved = 1 AND type IN (0,1)
            AND so.Approved_Date >= (SELECT t.fromDate FROM tg t) AND so.Approved_Date < (SELECT t.toDate FROM tg t) + 1
            AND so.customer_id in (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid) AND sod.is_free_item = 0
            and sod.product_id in (select t.product_id from spDK t)
        GROUP BY sod.product_id
      )
      ,nhapTraHang AS(
        SELECT sod.product_id, SUM(sod.quantity) giaTri
        FROM sale_order so
        JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
        WHERE so.ORDER_TYPE = 'CM' AND so.approved = 1 AND type = 2
            AND so.CREATE_DATE >= (SELECT t.fromDate FROM tg t) AND so.CREATE_DATE < (SELECT t.toDate FROM tg t) + 1
            AND sod.CREATE_DATE >= (SELECT t.fromDate FROM tg t) AND sod.CREATE_DATE < (SELECT t.toDate FROM tg t) + 1
            AND so.customer_id in (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid) AND sod.is_free_item = 0
            and sod.product_id in (select t.product_id from spDK t)
        GROUP BY sod.product_id
      )
      ,slThucTe AS(
          SELECT sp.product_id
                ,(NVL((SELECT t.giaTri FROM xuatBan t WHERE t.product_id = sp.product_id), 0) 
                    - NVL((SELECT t.giaTri FROM nhapTraHang t WHERE t.product_id = sp.product_id), 0)) thucHien
          FROM spDK sp
      )
      select case when s.register_type = 0 then 1
            when s.register_type = 2 and (select SUM(t.thucHien) from slThucTe t) >= s.register_quantity then 1
          else 0 end ketQua
          , 0 phanTramHoanThanh
          , (select SUM(t.thucHien) from slThucTe t) sanLuongThucHien
          
      from pro_structure s
      where s.pro_info_id = proinfoid 
      ;

    -- Xet dieu kien Dat hay Khong dat cho NPP
    CURSOR c_DieuKien4NPP
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Lay danh sach SP trong dieu kien huong ho tro
      -- Neu ton tai sd.type = 2 -> lay tat ca san pham
      ,spDK AS(
        select sd.product_id
        from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        where s.pro_info_id = proinfoid and sd.type = 1 and sd.product_id is not NULL
        union
        select p.product_id
        from product p where p.convfact > 1 and p.status = 1 and p.cat_id = 1
        and exists (select 1 from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                    where s.pro_info_id = proinfoid and sd.type = 2)
      )
      ,dsNgay AS(
        SELECT TO_CHAR(tmp2.thang, 'MM/YYYY') thangStr, tmp2.thang, tmp2.soNgayTrongThang,
              CASE
                  WHEN TRUNC(start_date, 'MM') = TRUNC(end_date, 'MM') THEN TRUNC(end_date) - TRUNC(start_date) + 1
                  WHEN TRUNC(tmp2.thang, 'MM') = TRUNC(end_date, 'MM') THEN EXTRACT(DAY FROM tmp2.thang)
                  ELSE (TRUNC(LAST_DAY(tmp2.thang)) - TRUNC(tmp2.thang) + 1)
              END AS soNgayCanTinh
        FROM
            (SELECT
                    CASE
                        WHEN (TRUNC(start_date, 'MM') = TRUNC(end_date, 'MM')) THEN end_date
                        WHEN (TRUNC(tmp.month) = TRUNC(start_date)) THEN tmp.month
                        WHEN (TRUNC(tmp.month, 'MM') = TRUNC(end_date, 'MM')) THEN end_date
                        ELSE TRUNC(tmp.month, 'MM')
                    END AS thang,
                    TO_NUMBER(TO_CHAR(LAST_DAY(tmp.month), 'DD')) AS soNgayTrongThang,
                    start_date, end_date
            FROM
                (SELECT add_months( start_date, level-1 ) AS month, start_date, end_date
                FROM
                    (select pp.from_date start_date, pp.to_date end_date
                    from pro_period pp where pp.pro_period_id = periodid)
                CONNECT BY level <= months_between(TRUNC(end_date,'MM'), TRUNC(start_date,'MM')) + 1
                ) tmp
            ) tmp2
      )
      ,slKeHoach AS(
        select NVL(SUM(t.khKetThung), -1) khKetThung
        --, SUM(t.khLit) khLit
        from (
          SELECT tmp.product_id, SUM((tmp.khKetThung/tmp.soNgayTrongThang)*tmp.soNgayCanTinh) AS khKetThung, SUM((tmp.khLit/tmp.soNgayTrongThang)*tmp.soNgayCanTinh) AS khLit
          FROM
            (SELECT sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh, SUM(sp.quantity) AS khKetThung, SUM(sp.quantity_lit) AS khLit
            FROM sale_plan sp, dsNgay tmp
            WHERE sp.object_type = 1 AND sp.type = 2
            and sp.object_id = (select cm.object_id from pro_cus_map cm where cm.pro_cus_map_id = procusmapid)
            AND sp.month = extract(MONTH FROM tmp.thang) AND sp.year = extract(YEAR FROM tmp.thang)
            and sp.product_id in (select t.product_id from spDK t)
            GROUP BY sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh
            ) tmp
          WHERE 1=1
            and exists (select 1 from pro_structure s where s.pro_info_id = proinfoid and s.register_type = 1)
          GROUP BY tmp.product_id
          )t
      )
      ,thucHien AS(
          SELECT SUM(rpt.xuat_ban - rpt.nhap_tra_hang) xuatBan, SUM(rpt.nhap_sbc) nhapTra
          FROM rpt_stock_total_day rpt
          WHERE 1=1
            AND rpt.shop_id IN (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid AND object_type = 2)
            AND rpt.rpt_in_date >= (SELECT t.fromDate FROM tg t) AND rpt.rpt_in_date < (SELECT t.toDate FROM tg t) + 1
            and rpt.product_id in (select t.product_id from spDK t)
      )
      ,slThucTe AS(
          select case when s.type = 5 then (select t.nhapTra from thucHien t)
                        when s.type = 6 then (select t.xuatBan from thucHien t)
                        else 0 end thucHien
          from pro_structure s where s.pro_info_id = proinfoid
      )
      select case when s.register_type = 0 then 1
            when s.register_type = 1 
              and ((NVL((select t.thucHien from slThucTe t), 0)/NVL((select t.khKetThung from slKeHoach t), 1) >= s.register_quantity)
                  OR ((select t.thucHien from slThucTe t) != -1 AND (select t.khKetThung from slKeHoach t) = -1)) then 1
            when s.register_type = 2 and (select t.thucHien from slThucTe t) >= s.register_quantity then 1
          else 0 end ketQua
          ,case when (select t.thucHien from slThucTe t) = -1 and (select t.khKetThung from slKeHoach t) = -1 then 0
              when (select t.thucHien from slThucTe t) != -1 and (select t.khKetThung from slKeHoach t) = -1 then 100
            else (select t.thucHien from slThucTe t)/NVL((select t.khKetThung from slKeHoach t), 1) 
          end phanTramHoanThanh
          ,(select t.thucHien from slThucTe t) sanLuongThucHien
      from pro_structure s
      where s.pro_info_id = proinfoid 
      ;
    
    -- Tinh he so
    CURSOR c_HeSo(v_sosuat IN NUMBER)
    IS
      SELECT MIN(heSo) heSo
      FROM (
      SELECT case when s.type = 5 then FLOOR(NVL(cp.quantity_return, 0) / sd.min_limit)
                  when s.type = 6 then FLOOR(NVL(cp.quantity, 0) / sd.min_limit)
                else 0 end heSo
      FROM pro_cus_process cp
      JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
      WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid 
        AND sd.type = 3 AND sd.min_limit != 0 and sd.min_limit is not null
        and sd.level_number = v_sosuat
        );

    -- Thong tin ho tro theo suat
    CURSOR c_HoTro(v_sosuat IN NUMBER)
    IS
      SELECT gd.pro_gift_detail_id, gd.unit_cost donGia, gd.quantity soLuong, pi.object_type loaiHoTro
      FROM pro_structure s
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
      JOIN product_info pi ON pi.product_info_id = gd.type_support
      WHERE s.pro_info_id = proinfoid AND gd.level_number = v_sosuat AND pi.type = 1
      ;

    -- Lay ra tong so suat
    CURSOR c_Suat
    IS
      SELECT t.soSuat
      FROM (
        SELECT DISTINCT sd.level_number soSuat
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE s.pro_info_id = proinfoid
        )t
      ORDER BY t.soSuat
      ;

    -- Lay ra han muc, han muc co the = NULL hoac > 0 (khong co han muc = 0)
    CURSOR c_HanMuc
    IS
      SELECT s.quota giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;  
    
    CURSOR c_LoaiDT
    IS
      SELECT object_type FROM pro_cus_map WHERE pro_cus_map_id = procusmapid;

    CURSOR c_LoaiDangKy
    IS
      SELECT s.register_type giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;

    rewardID NUMBER(20,0);
    rewardDetailID NUMBER(20,0);
    soLuong NUMBER(20,0);
    donGia NUMBER(20,0);
    result NUMBER(1,0);
    totalAmount NUMBER(20,0);
    hanMuc NUMBER(20, 0);
    heSoTmp NUMBER(20,0);
    isAchieved NUMBER(1, 0) := 0;
    heSo NUMBER(20,0);
    soSuatThuong NUMBER(20,0) := 0;
    loaiDoiTuong NUMBER(3,0);
    phanTramHoanThanh NUMBER(20,2);
    sanLuongThucHien NUMBER(20,0);
    loaiDangKy NUMBER(1,0) := 0;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT02_N_CUS_REWARD started');

    OPEN c_LoaiDT;
    FETCH c_LoaiDT INTO loaiDoiTuong;
    CLOSE c_LoaiDT;

    OPEN c_HanMuc;
    FETCH c_HanMuc INTO hanMuc;
    CLOSE c_HanMuc;

    totalAmount := 0;
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    
    IF(loaiDoiTuong = 1) THEN
      DBMS_OUTPUT.put_line ('Loai khach hang');
      -- customer
      OPEN c_DieuKien4KhachHang;
      FETCH c_DieuKien4KhachHang INTO result, phanTramHoanThanh, sanLuongThucHien;
      IF c_DieuKien4KhachHang%notfound THEN
        result := 0;
        phanTramHoanThanh := 0;
        sanLuongThucHien := 0;
      END IF;
      DBMS_OUTPUT.put_line ('phanTramHoanThanh = ' || phanTramHoanThanh);
      DBMS_OUTPUT.put_line ('sanLuongThucHien = ' || sanLuongThucHien);
      CLOSE c_DieuKien4KhachHang;

    ELSE
      DBMS_OUTPUT.put_line ('Loai NPP');
      -- npp
      OPEN c_DieuKien4NPP;
      FETCH c_DieuKien4NPP INTO result, phanTramHoanThanh, sanLuongThucHien;
      IF c_DieuKien4NPP%notfound THEN 
        result := 0;
        phanTramHoanThanh := 0;
        sanLuongThucHien := 0;
      END IF;
      DBMS_OUTPUT.put_line ('phanTramHoanThanh = ' || phanTramHoanThanh);
      DBMS_OUTPUT.put_line ('sanLuongThucHien = ' || sanLuongThucHien);
      CLOSE c_DieuKien4NPP;

    END IF;

    IF(result = 1) THEN
      FOR s IN c_Suat
      LOOP
        -- Neu con han muc
        IF (hanMuc > 0 OR hanMuc IS NULL) THEN
          OPEN c_HeSo(s.soSuat);
          FETCH c_HeSo INTO heSoTmp;
          CLOSE c_HeSo;

          IF(heSoTmp > 0) THEN
            DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Dat thuong');
            isAchieved := 1;

            IF (hanMuc IS NULL) THEN
              heSo := heSoTmp;
            ELSIF (hanMuc < heSoTmp AND hanMuc IS NOT NULL) THEN
              heSo := hanMuc;
              hanMuc := 0;
            ELSIF (hanMuc >= heSoTmp AND hanMuc IS NOT NULL) THEN
              hanMuc := hanMuc - heSoTmp;
              heSo := heSoTmp;
            END IF;

            soSuatThuong := soSuatThuong + heSo;  
            DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ' he so dat thuong : ' || heSo);
            FOR v IN c_HoTro(s.soSuat)
            LOOP
              totalAmount := totalAmount + v.soLuong*heSo*v.donGia;
              IF (v.loaiHoTro = 4) THEN
                -- Ho tro tien
                INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
                VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia*heSo, v.soLuong*heSo*v.donGia, sysdate, username);
              ELSE
                INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
                VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong*heSo, v.donGia, v.soLuong*heSo*v.donGia, sysdate, username);
              END IF;
              DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
            END LOOP;
          ELSE
            DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Khong dat thuong');
          END IF;

        END IF;
      END LOOP;
    ELSE
      DBMS_OUTPUT.put_line ('Chuong trinh khong dat thuong');
    END IF;

    INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, LEVEL_AUTO, CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
    VALUES(rewardID, procusmapid, totalAmount, soSuatThuong, sysdate, username, periodid, isAchieved, proinfoid);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    DBMS_OUTPUT.put_line ('Ket qua ' || isAchieved);

    OPEN c_LoaiDangKy;
    FETCH c_LoaiDangKy INTO loaiDangKy;
    CLOSE c_LoaiDangKy;

    IF(loaiDangKy = 1) THEN
      DBMS_OUTPUT.put_line ('Loai ho tro: % san luong');

      update pro_cus_reward set register_quantity = phanTramHoanThanh where pro_cus_reward_id = rewardID;
      DBMS_OUTPUT.put_line ('updated 1 row into PRO_CUS_REWARD field register_quantity = ' || phanTramHoanThanh);

    ELSIF (loaiDangKy = 2) THEN
      DBMS_OUTPUT.put_line ('Loai ho tro: san luong');
      
      update pro_cus_reward set register_quantity = sanLuongThucHien where pro_cus_reward_id = rewardID;
      DBMS_OUTPUT.put_line ('updated 1 row into PRO_CUS_REWARD field register_quantity = ' || sanLuongThucHien);
               
    END IF;

    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT02_N_CUS_REWARD finished');

  END PRO_HT02_N_CUS_REWARD;