SELECT value FROM ap_param WHERE ap_param_code = 'LAST_DAY_REWARD';
SELECT value FROM ap_param WHERE ap_param_code = 'START_DAY_REWARD';
update ap_param set value = 5 where ap_param_code = 'LAST_DAY_REWARD';

select * from pro_info where pro_info_id = 280;

-- 1116
select * from pro_period where pro_period_id = 1134;
select * from pro_period where pro_info_id = 62;
update pro_period set to_date = sysdate - 10 where pro_period_id = 1116;

select * from pro_cus_process where pro_info_id = 67;
select * from pro_cus_process where pro_info_id = 67 and pro_cus_map_id = 171 and pro_period_id = 155;
delete from pro_cus_process where pro_info_id = 271;

-- 996, 997, 998
select * from pro_cus_map where pro_info_id = 274;
select * from pro_cus_map where pro_cus_map_id = 137;

-- 727
select * from pro_structure  where pro_info_id = 67;

select * from pro_structure_detail where pro_structure_id = (select pro_structure_id from pro_structure  where pro_info_id = 67);

select * from pro_cus_reward where pro_info_id = 67;
delete from pro_cus_reward where pro_cus_reward_id in ( select pro_cus_reward_id from pro_cus_reward where pro_info_id = 67);

select * from pro_cus_reward_detail where pro_cus_reward_id in ( select pro_cus_reward_id from pro_cus_reward where pro_info_id = 67);
delete from pro_cus_reward_detail where pro_cus_reward_id in ( select pro_cus_reward_id from pro_cus_reward where pro_info_id = 268);

-- PRO_CUS_PROCESS
WITH
proPeriod AS(
  SELECT pp.pro_period_id, pp.pro_info_id, pp.from_date, pp.to_date
  FROM pro_period pp
  WHERE 1=1
    AND pp.to_date < sysdate AND pp.pro_period_id NOT IN (SELECT t.pro_period_id FROM pro_cus_process t)
    )
,proCusMap AS(
  SELECT cm.pro_cus_map_id, cm.pro_info_id, ch.from_date, ch.to_date
  FROM pro_cus_map cm
  JOIN pro_cus_history ch ON ch.pro_cus_map_id = cm.pro_cus_map_id
  WHERE ch.type = 1
  )
SELECT pp.pro_info_id, pp.pro_period_id, cm.pro_cus_map_id, pp.from_date, pp.to_date
FROM proPeriod pp, proCusMap cm
WHERE pp.pro_info_id = cm.pro_info_id AND TRUNC(cm.from_date) <= TRUNC(pp.from_date)
AND (cm.to_date IS NULL OR (TRUNC(cm.to_date) >= TRUNC(pp.to_date)))
;

-- PROCESS_FOR_SHOP
WITH
  tg AS(
    SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = 1116
    )
  -- Chi lay nhung san pham nam trong Muc khach hang dang ky thuc hien
  -- Doi voi loai khong co Muc, thi LEVEL_NUMBER IS NULL trong bang PRO_CUS_MAP
  ,dsSP AS(
    SELECT sd.product_id
    FROM pro_cus_map cm
    JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
    JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
    WHERE 1=1 AND cm.pro_info_id = 271 AND cm.pro_cus_map_id = 996 AND s.type IN (1, 2)
      AND sd.product_id IS NOT NULL AND sd.type =1 AND (sd.level_number = cm.level_number OR cm.level_number IS NULL)
    union
    -- Ap dung HT02_N & HT06
    SELECT sd.product_id
    FROM pro_structure s
    JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
    WHERE 1=1 AND s.pro_info_id = 271 AND s.type IN (5, 6)
      AND sd.product_id IS NOT NULL AND sd.type IN (3)
  )
  ,thucHien AS(
    SELECT rpt.product_id, SUM(rpt.xuat_ban - rpt.nhap_tra_hang) xuatBan, SUM(rpt.nhap_sbc) nhapTra
    FROM rpt_stock_total_day rpt
    WHERE 1=1
      AND rpt.shop_id IN (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = 996 AND object_type = 2)
      AND rpt.rpt_in_date >= (SELECT t.fromDate FROM tg t) AND rpt.rpt_in_date < (SELECT t.toDate FROM tg t) + 1
    GROUP BY rpt.product_id
  )
  SELECT sp.product_id
        ,NVL((SELECT t.xuatBan FROM thucHien t WHERE t.product_id = sp.product_id), 0) xuatBan
        ,NVL((SELECT t.nhapTra FROM thucHien t WHERE t.product_id = sp.product_id), 0) nhapTra
  FROM dsSP sp
  ;