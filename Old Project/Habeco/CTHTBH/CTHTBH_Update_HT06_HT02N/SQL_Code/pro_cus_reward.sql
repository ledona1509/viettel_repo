PROCEDURE PRO_CUS_REWARD (inputDate DATE)
  IS
    CURSOR c_ProCusProcess
    IS
      SELECT DISTINCT cp.pro_cus_map_id, cp.pro_period_id, cp.pro_info_id, pp.to_date
      FROM pro_cus_process cp
      JOIN pro_period pp ON pp.pro_period_id = cp.pro_period_id
      JOIN pro_info pi ON pi.pro_info_id = cp.pro_info_id
      WHERE 1=1
        AND (cp.pro_cus_map_id, cp.pro_period_id) NOT IN (SELECT cr.pro_cus_map_id, cr.pro_period_id FROM pro_cus_reward cr)
      ;

    y_param NUMBER(3,0);
    y_Date DATE;
    loaiCT NUMBER(3,0);
    loaiKH NUMBER(3,0);
  BEGIN
    dbms_output.enable(100000000000000);
    DBMS_OUTPUT.put_line ('PRO_CUS_REWARD started');
    SELECT value INTO y_param FROM ap_param WHERE ap_param_code = 'LAST_DAY_REWARD';

    FOR v IN c_ProCusProcess
    LOOP
      SELECT pi.type INTO loaiCT FROM pro_info pi WHERE pi.pro_info_id = v.pro_info_id;
      BEGIN
        SELECT pic.type INTO loaiKH FROM pro_info_cus pic WHERE pic.pro_info_id = v.pro_info_id AND pic.type = -2;
        EXCEPTION
          WHEN 
            NO_DATA_FOUND
          THEN 
            loaiKH := 1;
      END;
      y_Date := v.to_date + y_param;
      
      IF (inputDate >= TRUNC(y_Date) + 1) THEN
      DBMS_OUTPUT.put_line ('Here');
        IF (loaiCT = 1) THEN
          DBMS_OUTPUT.put_line ('Here');
          PKG_HTBH.PRO_HT01_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 0, 'tien trinh');
        ELSIF (loaiCT = 2) and (loaiKH <> -2) THEN
          PKG_HTBH.PRO_HT02_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 3) THEN
          PKG_HTBH.PRO_HT03_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 4) and (loaiKH <> -2) THEN
          PKG_HTBH.PRO_HT04_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 5) THEN
          PKG_HTBH.PRO_HT05_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 6) THEN
          PKG_HTBH.PRO_HT06_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 7) THEN
          PKG_HTBH.PRO_HT02_N_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        END IF;
      END IF;
    END LOOP;
    DBMS_OUTPUT.put_line ('PRO_CUS_REWARD finished');
  END PRO_CUS_REWARD;