-- Bo sung register_quantity truong pro_structure
ALTER TABLE pro_structure ADD register_quantity NUMBER(20, 2);
COMMENT ON COLUMN pro_structure.register_quantity IS 'Gia tri dang ky hoan thanh theo ke hoach';

-- Bo sung register_type truong pro_structure
ALTER TABLE pro_structure ADD register_type NUMBER(2,0);
COMMENT ON COLUMN pro_structure.register_type IS 'Loai dang ky hoan thanh theo ke hoach: 0: khong dang ky, 1: % san luong, 2: san luong, 3: doanh so';

-- Bo sung type = 5,6 cho pro_info
COMMENT ON COLUMN pro_info.type IS 'Loai chuong trinh: 1: HT01, 2: HT02, 3: HT03, 4: HT04, 5: HT05, 6: HT06, 7: HT02_N';


COMMENT ON COLUMN pro_structure.type IS 'Loai ap dung: 1: toan SP (HT01,02,03,05); 2: SP chua khai bao (HT01,02,03,05); 3,4: danh cho HT04; 5: sale in(HT06, HT02_N); 6: sale out(HT06, HT02_N)';
COMMENT ON COLUMN pro_structure_detail.type IS 'Loai: 1: tung san pham, 2: tong san pham, 3: khac (HT04, HT06, HT02_N)';

-- Bo sung register_quantity truong pro_cus_reward
ALTER TABLE pro_cus_reward ADD register_quantity NUMBER(20, 2);
COMMENT ON COLUMN pro_cus_reward.register_quantity IS 'Gia tri hoan thanh theo ke hoach thuc te';

-- Bo sung quantity_return truong pro_cus_process
ALTER TABLE pro_cus_process ADD quantity_return NUMBER(20, 0);
COMMENT ON COLUMN pro_cus_process.quantity_return IS 'San luong nhap tra hang (danh cho NPP)';

-- Bo sung quantity_pkg_manual truong pro_cus_process
ALTER TABLE pro_cus_process ADD quantity_pkg_manual NUMBER(20, 0);
COMMENT ON COLUMN pro_cus_process.quantity_pkg_manual IS 'San luong vo chai ket thu hoi thuc te (HT06)';


-- Cap nhat procedure Bao cao tong hop thuc hien P_HT02_KQTB (PKG_WEB_SUPPORT)
PROCEDURE P_HT02_KQTB(res OUT SYS_REFCURSOR , lsObjectID CLOB, proInfoID NUMBER, periodID NUMBER, objecttype NUMBER)
  AS
  -- @author: MiNTT
  -- HT02 - Bao cao Tong hop thuc hien chuong trinh
  -- objecttype: Loai doi tuong: 1: customer, 2: NPP
  sql1 CLOB;
  sql2 CLOB;

  CURSOR c_dsSPThucHien
  IS
    SELECT  p.product_id
    FROM product p
    WHERE p.product_id IN ( select sd.product_id FROM  pro_info i
                            JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
                            JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                            JOIN product p ON p.product_id = sd.product_id
                            WHERE i.pro_info_id = proInfoID AND s.type IN (5, 6) AND sd.type = 3
                          )
    ORDER BY p.sort_order
    ;
  CURSOR c_dsSPHoTro
  IS
    SELECT DISTINCT proinfo.product_info_id, p.sort_order, giftd.product_id
    from pro_cus_reward rew
    JOIN pro_cus_reward_detail rewd ON rew.pro_cus_reward_id = rewd.pro_cus_reward_id
    JOIN pro_gift_detail giftd ON giftd.pro_gift_detail_id = rewd.pro_gift_detail_id
    JOIN  product_info proinfo ON proinfo.product_info_id = giftd.type_support --AND proinfo.type = 1-- and proinfo.object_type = 4
    JOIN pro_cus_map m ON m.pro_cus_map_id = rew.pro_cus_map_id
    JOIN product p ON p.product_id = giftd.product_id
    WHERE m.pro_info_id = proInfoID AND rew.pro_period_id = periodID AND (proinfo.type != 1 OR proinfo.object_type != 4)
    ORDER BY proinfo.product_info_id, p.sort_order
        
        
    ;
  BEGIN
    DBMS_OUTPUT.ENABLE(100000000000000);
    
    sql1:=sql1 || 'WITH';
    sql1:=sql1 || '    dsThamGia AS(';
    sql1:=sql1 || '      SELECT cm.pro_cus_map_id, cm.object_id, cr.level_auto';
    sql1:=sql1 || '      FROM pro_cus_map cm JOIN pro_cus_reward cr ON cr.pro_cus_map_id = cm.pro_cus_map_id';
    sql1:=sql1 || '      WHERE 1=1';
    sql1:=sql1 || '      AND cr.pro_period_id = '|| periodID ||'';
    sql1:=sql1 || '        AND cm.pro_info_id = '|| proInfoID ||' AND cm.object_type = '|| objecttype || ' AND cm.object_id IN (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB('''|| lsObjectID ||''')))';
    sql1:=sql1 || '    )';
    sql1:=sql1 || '    ,slTH AS(';
    sql1:=sql1 || '      SELECT cp.pro_cus_map_id, cp.product_id, cp.quantity soLuong';
    sql1:=sql1 || '      FROM pro_cus_process cp WHERE cp.pro_period_id = '|| periodID ||'';
    sql1:=sql1 || '    )';
    sql1:=sql1 || '    ,ht AS (';
    sql1:=sql1 || '      SELECT  cr.pro_cus_map_id, gd.product_id';
    sql1:=sql1 || '              ,crd.quantity_auto soLuong';
    sql1:=sql1 || '              ,crd.unit_cost_auto donGia';
    sql1:=sql1 || '              ,crd.amount thanhTien';
    --sql1:=sql1 || '              ,proinfo.product_info_id, p.sort_order';
    sql1:=sql1 || '      FROM pro_info i';
    sql1:=sql1 || '      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id';
    sql1:=sql1 || '      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id';
    sql1:=sql1 || '      JOIN pro_cus_map cm ON cm.pro_info_id = i.pro_info_id';
    sql1:=sql1 || '      JOIN pro_cus_reward cr ON cr.pro_cus_map_id = cm.pro_cus_map_id';
    sql1:=sql1 || '      LEFT JOIN pro_cus_reward_detail crd ON crd.pro_cus_reward_id = cr.pro_cus_reward_id';
    sql1:=sql1 || '      LEFT JOIN pro_gift_detail gd ON gd.pro_gift_detail_id = crd.pro_gift_detail_id';
    --sql1:=sql1 || '      JOIN  product_info proinfo ON proinfo.product_info_id = gd.type_support ';
    --sql1:=sql1 || '      LEFT JOIN product p ON p.product_id = gd.product_id';
    sql1:=sql1 || '      WHERE 1=1';
    sql1:=sql1 || '        AND ((cr.result_auto = 1 AND cr.result_manual IS NULL) OR (cr.result_manual = 1))';
    sql1:=sql1 || '        AND i.pro_info_id = '|| proInfoID ||' AND cr.pro_period_id = '|| periodID ||'';
    --sql1:=sql1 || '      ORDER BY proinfo.product_info_id,  p.sort_order';
    sql1:=sql1 || '    )';
    sql1:=sql1 || ' SELECT ';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '    CASE WHEN t.tmp = 0 THEN c.customer_name';
      sql1:=sql1 || '                WHEN t.tmp = 1 THEN npp.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 3 THEN cn.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 7 THEN kv.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 15 THEN ho.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 31 THEN hbc.shop_name';
    ELSE
      sql1:=sql1 || '    CASE WHEN t.tmp = 0 THEN npp.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 1 THEN cn.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 3 THEN kv.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 7 THEN ho.shop_name';
      sql1:=sql1 || '                WHEN t.tmp = 15 THEN hbc.shop_name';
    END IF;
    sql1:=sql1 || '          END tenDV';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN c.address ELSE NULL END diaChi';
    ELSE
      sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN npp.address ELSE NULL END diaChi';
    END IF;
     IF (objecttype = 1) THEN
      sql1:=sql1 || '   , (select db.level_auto from dsThamGia db where t.customer_id = db.object_id) soSuatDat';
    ELSE
      sql1:=sql1 || ' ,  (select db.level_auto from dsThamGia db where t.nppID = db.object_id) soSuatDat';
    END IF;
   
    FOR x IN c_dsSPThucHien
    LOOP
      sql1:=sql1 || '          , t.thucHien_'|| x.product_id;
    END LOOP;
     FOR x IN c_dsSPHoTro
    LOOP
      sql1:=sql1 || '          , t.soLuong_'|| x.product_id;
      sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN t.donGia_'|| x.product_id ||' ELSE NULL END donGia_'|| x.product_id ;
      sql1:=sql1 || '          , t.thanhTien_'|| x.product_id;
    END LOOP;
   -- sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN t.soLuong ELSE NULL END soLuong';
   -- sql1:=sql1 || '          , CASE WHEN t.tmp = 0 THEN t.donGia ELSE NULL END donGia';
    
    sql1:=sql1 || '          , t.thanhTien, t.tongTienHoTro, t.tmp';
    sql1:=sql1 || '    FROM (';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '        SELECT t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID, t.customer_id  ';
    ELSE
      sql1:=sql1 || '        SELECT t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID ';
    END IF;
  
    FOR x IN c_dsSPThucHien
    LOOP
      sql1:=sql1 || '              , SUM(t.thucHien_'|| x.product_id ||') thucHien_'|| x.product_id;
    END LOOP;
    FOR x IN c_dsSPHoTro
    LOOP
      sql1:=sql1 || '              , SUM(t.soLuong_'|| x.product_id ||') soLuong_'|| x.product_id ||', SUM(t.donGia_'|| x.product_id ||') donGia_'|| x.product_id ||', SUM(t.thanhTien_'|| x.product_id ||') thanhTien_'|| x.product_id;
    END LOOP;
    sql1:=sql1 || '              , SUM(t.soLuong) soLuong, SUM(t.donGia) donGia, SUM(t.thanhTien) thanhTien, SUM(t.tongTienHoTro) tongTienHoTro';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '              , GROUPING_ID (t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID, t.customer_id) tmp';
    ELSE
      sql1:=sql1 || '              , GROUPING_ID (t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID) tmp';
    END IF;
    sql1:=sql1 || '        FROM (';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '            SELECT hbc.shop_id hbcID, ho.shop_id hoID, kv.shop_id kvID, cn.shop_id cnID, npp.shop_id nppID, c.customer_id';
    ELSE
      sql1:=sql1 || '            SELECT hbc.shop_id hbcID, ho.shop_id hoID, kv.shop_id kvID, cn.shop_id cnID, npp.shop_id nppID';
    END IF;
   --    sql1:=sql1 || '                  ,db.pro_cus_map_id';
    FOR x IN c_dsSPThucHien
    LOOP
      sql1:=sql1 || '                  ,(SELECT t.soLuong FROM slTH t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id = '|| x.product_id ||') thucHien_'|| x.product_id;
    END LOOP;
    FOR x IN c_dsSPHoTro
    LOOP
    sql1:=sql1 || '                  ,(SELECT SUM(t.soLuong) FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id = '|| x.product_id ||') soLuong_'|| x.product_id;
    sql1:=sql1 || '                  ,(SELECT t.donGia FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id = '|| x.product_id ||' and rownum = 1) donGia_'|| x.product_id;
    sql1:=sql1 || '                  ,(SELECT SUM(t.thanhTien) FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id = '|| x.product_id ||') thanhTien_'|| x.product_id;
    END LOOP;
    sql1:=sql1 || '                  ,(SELECT t.soLuong FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id IS NULL and rownum = 1) soLuong';
    sql1:=sql1 || '                  ,(SELECT SUM(t.donGia) FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id IS NULL) donGia';
    sql1:=sql1 || '                  ,(SELECT SUM(t.thanhTien) FROM ht t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.product_id IS NULL) thanhTien';
    sql1:=sql1 || '                  ,(SELECT t.total_amount FROM pro_cus_reward t WHERE t.pro_cus_map_id = db.pro_cus_map_id AND t.pro_period_id = '|| periodID ||') tongTienHoTro';
    sql1:=sql1 || '            FROM dsThamGia db';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '            JOIN customer c ON c.customer_id = db.object_id';
      sql1:=sql1 || '            JOIN shop npp ON npp.shop_id = c.shop_id';
    ELSE
      sql1:=sql1 || '            JOIN shop npp ON npp.shop_id = db.object_id';
    END IF;
    sql1:=sql1 || '            JOIN shop cn ON cn.shop_id = npp.parent_shop_id';
    sql1:=sql1 || '            JOIN shop kv ON kv.shop_id = cn.parent_shop_id';
    sql1:=sql1 || '            JOIN shop ho ON ho.shop_id = kv.parent_shop_id';
    sql1:=sql1 || '            JOIN shop hbc ON hbc.shop_id = ho.parent_shop_id';
    sql1:=sql1 || '            )t';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '        GROUP BY ROLLUP (t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID, t.customer_id)';
    ELSE
      sql1:=sql1 || '        GROUP BY ROLLUP (t.hbcID, t.hoID, t.kvID, t.cnID, t.nppID)';
    END IF;
    sql1:=sql1 || '        )t';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '    LEFT JOIN customer c ON c.customer_id = t.customer_id';
    END IF;
    sql1:=sql1 || '    LEFT JOIN shop npp ON npp.shop_id = t.nppID';
    sql1:=sql1 || '    LEFT JOIN shop cn ON cn.shop_id = t.cnID';
    sql1:=sql1 || '    LEFT JOIN shop kv ON kv.shop_id = t.kvID';
    sql1:=sql1 || '    LEFT JOIN shop ho ON ho.shop_id = t.hoID';
    sql1:=sql1 || '    LEFT JOIN shop hbc ON hbc.shop_id = t.hbcID';
    sql1:=sql1 || '    WHERE t.hbcID IS NOT NULL';
    IF (objecttype = 1) THEN
      sql1:=sql1 || '    ORDER BY  ho.shop_name, kv.shop_name, cn.shop_name, npp.shop_name,  c.customer_name';
    ELSE
      sql1:=sql1 || '    ORDER BY  ho.shop_name, kv.shop_name, cn.shop_name, npp.shop_name';
    END IF;
    DBMS_OUTPUT.PUT_LINE(sql1);
    OPEN res FOR (sql1);

  END P_HT02_KQTB;

-- Khai bao Header package PKG_HTBH
create or replace 
PACKAGE PKG_HTBH AS

  -- Tinh thuc hien cho Customer luu vao bang PRO_CUS_PROCESS
  PROCEDURE PROCESS_FOR_CUSTOMER (proinfoid NUMBER, procusmapid NUMBER, objectId NUMBER, periodid NUMBER);

  -- Tinh thuc hien cho SHOP luu vao bang PRO_CUS_PROCESS
  PROCEDURE PROCESS_FOR_SHOP (proinfoid NUMBER, procusmapid NUMBER, objectId NUMBER, periodid NUMBER);

  -- Tien trinh tinh thuong
  PROCEDURE PRO_CUS_PROCESS (inputDate DATE);
  PROCEDURE PRO_CUS_REWARD (inputDate DATE);

  -- Tinh thuong HT01, luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  -- typeprocess: 0: tien trinh; 1: nhap tay manual
  PROCEDURE PRO_HT01_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, typeprocess NUMBER DEFAULT 0, username STRING DEFAULT 'tien trinh');

  -- Tinh thuong HT01 loai ho tro Tien/Ket,Thung, luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  PROCEDURE PRO_HT01_CUS_REWARD_01 (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, typeprocess NUMBER, username STRING);

  -- Tinh thuong HT01 loai ho tro Muc Tien, luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  PROCEDURE PRO_HT01_CUS_REWARD_02 (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, typeprocess NUMBER, username STRING);

  -- Tinh thuong HT02 luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  PROCEDURE PRO_HT02_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING);

  PROCEDURE PRO_HT03_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING);

  -- Tinh thuong HT05 luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  PROCEDURE PRO_HT05_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING);

  -- Tinh thuong HT04 luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  PROCEDURE PRO_HT04_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING);
  
  -- Tinh thuong HT06 luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  PROCEDURE PRO_HT06_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING);
  
  -- Tinh thuong HT02_N luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  PROCEDURE PRO_HT02_N_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING);
END PKG_HTBH;

\

-- Khai bao Body package PKG_HTBH
create or replace 
PACKAGE BODY PKG_HTBH AS
  PROCEDURE PROCESS_FOR_CUSTOMER (proinfoid NUMBER, procusmapid NUMBER, objectId NUMBER, periodid NUMBER)
  -- Tinh thuc hien cho Customer luu vao bang PRO_CUS_PROCESS
  IS
    CURSOR c_Customer
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Chi lay nhung san pham nam trong Muc khach hang dang ky thuc hien
      -- Doi voi loai khong co Muc, thi LEVEL_NUMBER IS NULL trong bang PRO_CUS_MAP
      ,dsSP AS(
        SELECT sd.product_id
        FROM pro_cus_map cm
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1 AND cm.pro_info_id = proinfoid AND cm.pro_cus_map_id = procusmapid AND s.type IN (1, 2)
          AND sd.product_id IS NOT NULL AND sd.type =1 AND (sd.level_number = cm.level_number OR cm.level_number IS NULL)
        union
        -- Ap dung HT02_N & HT06
        SELECT sd.product_id
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1 AND s.pro_info_id = proinfoid AND s.type IN (5, 6)
          AND sd.product_id IS NOT NULL AND sd.type IN (3)

      )
      ,xuatBan AS(
        SELECT sod.product_id, SUM(sod.quantity) giaTri
        FROM sale_order so
        JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
        WHERE so.approved = 1 AND type IN (0,1)
            AND so.Approved_Date >= (SELECT t.fromDate FROM tg t) AND so.Approved_Date < (SELECT t.toDate FROM tg t) + 1
            AND so.customer_id = objectId AND sod.is_free_item = 0
        GROUP BY sod.product_id
      )
      ,nhapTraHang AS(
        SELECT sod.product_id, SUM(sod.quantity) giaTri
        FROM sale_order so
        JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
        WHERE so.ORDER_TYPE = 'CM' AND so.approved = 1 AND type = 2
            AND so.CREATE_DATE >= (SELECT t.fromDate FROM tg t) AND so.CREATE_DATE < (SELECT t.toDate FROM tg t) + 1
            AND sod.CREATE_DATE >= (SELECT t.fromDate FROM tg t) AND sod.CREATE_DATE < (SELECT t.toDate FROM tg t) + 1
            AND so.customer_id = objectId AND sod.is_free_item = 0
        GROUP BY sod.product_id
      )
      SELECT sp.product_id
            ,(NVL((SELECT t.giaTri FROM xuatBan t WHERE t.product_id = sp.product_id), 0) 
                - NVL((SELECT t.giaTri FROM nhapTraHang t WHERE t.product_id = sp.product_id), 0)) muaTuNPP
            ,NVL((
                SELECT ROUND(SUM(psod.quantity/psod.convfact))
                FROM pg_sale_order pso
                JOIN pg_sale_order_detail psod ON psod.pg_sale_order_id = pso.pg_sale_order_id
                WHERE pso.order_date >= (SELECT t.fromDate FROM tg t) AND pso.order_date < (SELECT t.toDate FROM tg t) + 1
                    AND psod.order_date >= (SELECT t.fromDate FROM tg t) AND psod.order_date < (SELECT t.toDate FROM tg t) + 1
                    AND pso.customer_id = objectId AND psod.product_id = sp.product_id
            ),0) slTuPG
      FROM dsSP sp
      ;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PROCESS_FOR_CUSTOMER started');
    DBMS_OUTPUT.put_line ('Customer ID = ' || objectId);
    FOR v IN c_Customer
    LOOP
        DELETE FROM pro_cus_process WHERE pro_cus_map_id = procusmapid AND product_id = v.product_id AND pro_info_id = proinfoid AND pro_period_id = periodid;
        
        INSERT INTO pro_cus_process(pro_cus_process_id,  pro_info_id, pro_cus_map_id, product_id,   quantity_shop,  quantity_pg,  quantity, create_date, create_user, object_id, object_type, pro_period_id)
                    VALUES (PRO_CUS_PROCESS_SEQ.nextval, proinfoid,   procusmapid,    v.product_id, v.muaTuNPP,     v.slTuPG,     v.slTuPG, sysdate,    'tien trinh', objectId  , 1,          periodid);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_PROCESS');
    END LOOP;
    COMMIT;
    DBMS_OUTPUT.put_line ('PROCESS_FOR_CUSTOMER finished');
  END PROCESS_FOR_CUSTOMER;
  
  PROCEDURE PROCESS_FOR_SHOP (proinfoid NUMBER, procusmapid NUMBER, objectId NUMBER, periodid NUMBER)
  -- Tinh thuc hien cho SHOP luu vao bang PRO_CUS_PROCESS
  IS
    CURSOR c_NPP
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Chi lay nhung san pham nam trong Muc khach hang dang ky thuc hien
      -- Doi voi loai khong co Muc, thi LEVEL_NUMBER IS NULL trong bang PRO_CUS_MAP
      ,dsSP AS(
        SELECT sd.product_id
        FROM pro_cus_map cm
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1 AND cm.pro_info_id = proinfoid AND cm.pro_cus_map_id = procusmapid AND s.type IN (1, 2)
          AND sd.product_id IS NOT NULL AND sd.type =1 AND (sd.level_number = cm.level_number OR cm.level_number IS NULL)
        union
        -- Ap dung HT02_N & HT06
        SELECT sd.product_id
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1 AND s.pro_info_id = proinfoid AND s.type IN (5, 6)
          AND sd.product_id IS NOT NULL AND sd.type IN (3)
      )
      ,thucHien AS(
        SELECT rpt.product_id, SUM(rpt.xuat_ban - rpt.nhap_tra_hang) xuatBan, SUM(rpt.nhap_sbc) nhapTra
        FROM rpt_stock_total_day rpt
        WHERE 1=1
          AND rpt.shop_id IN (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid AND object_type = 2)
          AND rpt.rpt_in_date >= (SELECT t.fromDate FROM tg t) AND rpt.rpt_in_date < (SELECT t.toDate FROM tg t) + 1
        GROUP BY rpt.product_id
      )
      SELECT sp.product_id
            ,NVL((SELECT t.xuatBan FROM thucHien t WHERE t.product_id = sp.product_id), 0) xuatBan
            ,NVL((SELECT t.nhapTra FROM thucHien t WHERE t.product_id = sp.product_id), 0) nhapTra
      FROM dsSP sp
      ;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PROCESS_FOR_SHOP started');
    DBMS_OUTPUT.put_line ('Shop ID = ' || objectId);
    FOR v IN c_NPP
    LOOP
      DELETE FROM pro_cus_process WHERE pro_cus_map_id = procusmapid AND product_id = v.product_id AND pro_info_id = proinfoid AND pro_period_id = periodid;
      
      -- 18/03 Bo sung them insert nhap tra hang HT02_N, HT06
      INSERT INTO pro_cus_process(pro_cus_process_id, pro_info_id, pro_cus_map_id, product_id,  quantity, quantity_return, CREATE_DATE,CREATE_USER, OBJECT_ID, OBJECT_TYPE, PRO_PERIOD_ID)
                 VALUES (PRO_CUS_PROCESS_SEQ.nextval, proinfoid  , procusmapid  , v.product_id, v.xuatBan, v.nhapTra     , sysdate   , 'tien trinh', objectId, 2          , periodid);
      DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_PROCESS');
    END LOOP;
    COMMIT;
    DBMS_OUTPUT.put_line ('PROCESS_FOR_SHOP finished');
  END PROCESS_FOR_SHOP;
  
  PROCEDURE PRO_CUS_PROCESS (inputDate DATE)
  -- Duyet qua bang PRO_PERIOD neu TO_DATE < inputDate va PRO_PERIOD_ID chua co du lieu trong bang PRO_CUS_PROCESS thi thuc hien
  IS
    CURSOR c_ProPeriod
    IS
      WITH
      proPeriod AS(
        SELECT pp.pro_period_id, pp.pro_info_id, pp.from_date, pp.to_date
        FROM pro_period pp
        WHERE 1=1
          AND pp.to_date < inputDate AND pp.pro_period_id NOT IN (SELECT t.pro_period_id FROM pro_cus_process t)
          )
      ,proCusMap AS(
        SELECT cm.pro_cus_map_id, cm.pro_info_id, ch.from_date, ch.to_date
        FROM pro_cus_map cm
        JOIN pro_cus_history ch ON ch.pro_cus_map_id = cm.pro_cus_map_id
        WHERE ch.type = 1
        )
      SELECT pp.pro_info_id, pp.pro_period_id, cm.pro_cus_map_id, pp.from_date, pp.to_date
      FROM proPeriod pp, proCusMap cm
      WHERE pp.pro_info_id = cm.pro_info_id AND TRUNC(cm.from_date) <= TRUNC(pp.from_date)
      AND (cm.to_date IS NULL OR (TRUNC(cm.to_date) >= TRUNC(pp.to_date)))
      ;

    -- Loai doi tuong: 1: customer, 2: npp
    CURSOR c_LoaiDT(v_CusMapId NUMBER)
    IS
      SELECT object_type, object_id FROM pro_cus_map WHERE pro_cus_map_id = v_CusMapId;
  
    loaiDoiTuong NUMBER(3,0);
    idDoiTuong NUMBER(20,0);
    x_param NUMBER(3,0);
    y_param NUMBER(3,0);
    x_Date Date;
    y_Date DATE;
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_CUS_PROCESS started');
    SELECT value INTO x_param FROM ap_param WHERE ap_param_code = 'START_DAY_REWARD';
    SELECT value INTO y_param FROM ap_param WHERE ap_param_code = 'LAST_DAY_REWARD';

    FOR v IN c_ProPeriod
    LOOP
      x_Date := v.to_date + x_param;
      y_Date := v.to_date + y_param;
      OPEN c_LoaiDT(v.pro_cus_map_id);
      FETCH c_LoaiDT INTO loaiDoiTuong, idDoiTuong;
      CLOSE c_LoaiDT;
      -- Loai doi tuong Customer
      IF (loaiDoiTuong = 1 AND inputDate >= TRUNC(x_Date)) THEN
        DBMS_OUTPUT.put_line ('inputDate >= trunc()' || x_Date);
        PROCESS_FOR_CUSTOMER (v.pro_info_id, v.pro_cus_map_id, idDoiTuong, v.pro_period_id);
      -- Loai doi tuong Shop
      ELSIF (loaiDoiTuong = 2 AND inputDate >= TRUNC(y_Date) + 1) THEN
        DBMS_OUTPUT.put_line ('inputDate >= trunc()' || y_Date || ' + 1');
        PROCESS_FOR_SHOP (v.pro_info_id, v.pro_cus_map_id, idDoiTuong, v.pro_period_id);
      END IF;
    END LOOP;
    DBMS_OUTPUT.put_line ('PRO_CUS_PROCESS finished');
  END PRO_CUS_PROCESS;
  
  PROCEDURE PRO_HT01_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, typeprocess NUMBER DEFAULT 0, username STRING DEFAULT 'tien trinh')
  -- Tinh thuong HT01, luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  -- typeprocess: 0: tien trinh; 1: nhap tay manual
  IS
    -- Loai ho tro HT01: 1: Tien/ket,thung; 2: Muc tien
    CURSOR c_LoaiHoTro
    IS
      SELECT g.type giaTri
      FROM pro_structure s
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      WHERE s.pro_info_id = proinfoid AND s.type IN (1, 2);
  
    loaiHoTro NUMBER(3,0);
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT01_CUS_REWARD started');
    OPEN c_LoaiHoTro;
    FETCH c_LoaiHoTro INTO loaiHoTro;
    CLOSE c_LoaiHoTro;
  
    IF (loaiHoTro = 1) THEN
      DBMS_OUTPUT.put_line ('Loai ho tro: Tien/Ket, thung');
      PKG_HTBH.PRO_HT01_CUS_REWARD_01(proinfoid, procusmapid, periodid, typeprocess, username);
    ELSE
      DBMS_OUTPUT.put_line ('Loai ho tro: Muc tien');
      PKG_HTBH.PRO_HT01_CUS_REWARD_02(proinfoid, procusmapid, periodid, typeprocess, username);
    END IF;    
    DBMS_OUTPUT.put_line ('PRO_HT01_CUS_REWARD finished');
  END PRO_HT01_CUS_REWARD;
  
  PROCEDURE PRO_HT01_CUS_REWARD_01 (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, typeprocess NUMBER, username STRING)
  -- Tinh thuong HT01 loai ho tro Tien/Ket,Thung, luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  -- typeprocess: 0: tien trinh; 1: nhap tay manual
  IS
    -- Loai 1: Tinh thuong chuong trinh HT01 Ho tro Ket/Thung,Tien cho Tong SL khong co TONG
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_KTongDK
    IS
      WITH
      spDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND (cp.quantity >= sd.min_limit)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh thuong
    CURSOR c_KTong
    IS
      -- Chi lay nhung san pham co khai bao
      SELECT gd.pro_gift_detail_id
            , cp.product_id
            ,CASE WHEN cp.quantity <= NVL(sd.max_limit, 0) THEN cp.quantity
              ELSE NVL(sd.max_limit, 0) END soLuong
            ,CASE WHEN NVL(cp.quantity, 0) <= NVL(sd.max_limit, 0) THEN cp.quantity
              ELSE NVL(sd.max_limit, 0) END soLuongManual
            ,NVL(gd.unit_cost, 0) donGia
      FROM pro_cus_process cp
      JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
      JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
      WHERE 1=1
        AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
      ;
  
    -- Loai 2: Tinh thuong chuong trinh HT01 Ho tro Ket/Thung,Tien cho Tong SL co TONG tung SP chua khai bao
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChuaKBDK
    IS
      WITH
      tongSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 1 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1
      )
      ,spDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND (cp.quantity >= sd.min_limit)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,dongTong AS (
        SELECT NVL(sd.min_limit, 0) chanDuoi
        FROM pro_cus_map cm
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND cm.pro_cus_map_id = procusmapid
          AND s.type IN (2)
          AND sd.type = 2 AND sd.unit_type = 1
      )
      ,spKhongDK AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND cp.product_id NOT IN (SELECT t.product_id FROM spDK t)
            )t
      )
--      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
--                        AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
--                  ELSE 1
--            END ketQua
--      FROM dual;
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE 
                        ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t)
                        OR
                        (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT count(1) FROM spDK) = (SELECT count(1) FROM tongSP))
                        ) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh thuong
    CURSOR c_TongChuaKB
    IS
      WITH
      -- Tinh thuong cho nhung san pham co khai bao (max_limit NOT NULL)
      spKhaiBao AS(
        SELECT gd.pro_gift_detail_id, cp.product_id
              , CASE WHEN cp.quantity <= sd.max_limit THEN cp.quantity
                ELSE sd.max_limit END soLuong
              , CASE WHEN NVL(cp.quantity, 0) <= NVL(sd.max_limit, 0) THEN cp.quantity
                ELSE NVL(sd.max_limit, 0) END soLuongManual
              , NVL(gd.unit_cost, 0) donGia
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
        JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND sd.max_limit IS NOT NULL
      )
      -- Tinh thuong cho nhung san pham chua khai bao (max_limit IS NULL)
      ,spChuaKhaiBao AS(
        SELECT t.pro_gift_detail_id, NULL product_id
              ,CASE WHEN t.giaTri <= t.max_limit THEN t.giaTri
              ELSE t.max_limit END soLuong
              ,CASE WHEN t.giaTriManual <= t.max_limit THEN t.giaTriManual
              ELSE t.max_limit END soLuongManual
              ,t.donGia
        FROM(  
          -- Chi co 1 dong
          SELECT gd.pro_gift_detail_id, sd.min_limit, sd.max_limit
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id NOT IN (SELECT t.product_id FROM spKhaiBao t)), 0) giaTri
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id NOT IN (SELECT t.product_id FROM spKhaiBao t)), 0) giaTriManual
                ,NVL(gd.unit_cost, 0) donGia
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
          JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid
            AND s.type IN (2) 
            AND g.type = 1
            AND sd.type = 2 AND sd.unit_type = 1
            AND i.type = 1 AND i.status = 1
            AND gd.type_apply = 2
        )t
      )
      SELECT * FROM spKhaiBao
      UNION ALL
      SELECT * FROM spChuaKhaiBao
      ;
  
    -- Loai 3: Tinh thuong chuong trinh HT01 Ho tro Ket/Thung,Tien cho Tong SL co TONG chung cho tat ca SP
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChungDK
    IS
      WITH
      spDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
          AND (cp.quantity >= sd.min_limit)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,dongTong AS (
        SELECT NVL(sd.min_limit, 0) chanDuoi
        FROM pro_cus_map cm
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND cm.pro_cus_map_id = procusmapid
          AND s.type IN (1)
          AND sd.type = 2 AND sd.unit_type = 1
      )
      ,tatCaSP AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
                        AND (SELECT t.soLuong FROM tatCaSP t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
  
    -- Tinh thuong
    CURSOR c_TongChung
    IS
      WITH
      -- Tinh thuong cho nhung san pham co khai bao (max_limit NOT NULL)
      spKhaiBao AS(
        SELECT gd.pro_gift_detail_id, cp.product_id
              , CASE WHEN cp.quantity <= sd.max_limit THEN cp.quantity
                ELSE sd.max_limit END soLuong
              , CASE WHEN NVL(cp.quantity, 0) <= NVL(sd.max_limit, 0) THEN cp.quantity
                ELSE NVL(sd.max_limit, 0) END soLuongManual
              , NVL(gd.unit_cost, 0) donGia
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
        JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND sd.max_limit IS NOT NULL
      )
      -- Tinh thuong cho nhung san pham chua khai bao (max_limit IS NULL)
      ,spChuaKhaiBao AS(
        SELECT t.pro_gift_detail_id, NULL product_id
              ,CASE WHEN t.giaTri <= (t.max_limit - NVL((SELECT SUM(t.soLuong) FROM spKhaiBao t), 0)) THEN t.giaTri
              ELSE (t.max_limit - NVL((SELECT SUM(t.soLuong) FROM spKhaiBao t), 0)) END soLuong
              ,CASE WHEN t.giaTriManual <= (t.max_limit - NVL((SELECT SUM(t.soLuongManual) FROM spKhaiBao t), 0)) THEN t.giaTriManual
              ELSE (t.max_limit - NVL((SELECT SUM(t.soLuongManual) FROM spKhaiBao t), 0)) END soLuongManual
              ,t.donGia
        FROM(  
          -- Chi co 1 dong
          SELECT gd.pro_gift_detail_id, sd.min_limit, sd.max_limit
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id NOT IN (SELECT t.product_id FROM spKhaiBao t)), 0) giaTri
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id NOT IN (SELECT t.product_id FROM spKhaiBao t)), 0) giaTriManual
                ,NVL(gd.unit_cost, 0) donGia
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
          JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid
            AND s.type IN (1)
            AND g.type = 1
            AND sd.type = 2 AND sd.unit_type = 1
            AND i.type = 1 AND i.status = 1
            AND gd.type_apply = 2
        )t
      )
      SELECT * FROM spKhaiBao
      UNION ALL
      SELECT * FROM spChuaKhaiBao
      ;
  
    -- Loai ap dung: 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
    CURSOR c_LoaiAD
    IS
      SELECT CASE WHEN sd.min_limit IS NULL AND sd.max_limit IS NULL THEN 1
                  WHEN s.type = 2 THEN 2
              ELSE 3 END loaiAD
      FROM pro_cus_map cm
      JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND  cm.pro_cus_map_id = procusmapid and sd.type = 2;
  
    -- Danh cho loai cap nhat bang tay: Lay PRO_CUS_REWARD_ID
    CURSOR c_ProCusReward
    IS
      SELECT pro_cus_reward_id
      FROM pro_cus_reward
      WHERE pro_cus_map_id = procusmapid AND pro_period_id = periodid;
    
    checkinsert NUMBER(1);
    loaiApDung NUMBER(3,0);
    idDoiTuong NUMBER(20,0);
  
    rewardID NUMBER(20,0);
    giftDetailID NUMBER(20,0);
    productID NUMBER(20,0);
    soLuong NUMBER(20,0);
    donGia NUMBER(20,0);
    result NUMBER(1,0);
    totalAmount NUMBER(20,0);
    cusrewardid NUMBER(20,0);
    cusrewarddetailid NUMBER(20,0);
    
  BEGIN
    OPEN c_LoaiAD;
    FETCH c_LoaiAD INTO loaiApDung;
    CLOSE c_LoaiAD;
  
    -- Khong co tong
    IF (loaiApDung = 1) THEN
    BEGIN
      DBMS_OUTPUT.put_line ('Loai khong co Tong');
      OPEN c_KTongDK;
      FETCH c_KTongDK INTO result;
      IF c_KTongDK%notfound THEN result := 0;
      END IF;
      CLOSE c_KTongDK;
  
      IF (typeprocess = 1) THEN
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo cach sua tay');
        OPEN c_ProCusReward;
        FETCH c_ProCusReward INTO cusrewardid;
        CLOSE c_ProCusReward;
  
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          checkinsert := 0;
          BEGIN
            SELECT crd.pro_cus_reward_detail_id INTO cusrewarddetailid
            FROM pro_cus_reward_detail crd WHERE crd.pro_cus_reward_id = cusrewardid;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN checkinsert := 1;
              WHEN TOO_MANY_ROWS THEN checkinsert := 0;
          END;
          FOR v IN c_KTong
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuongManual;
            IF (checkinsert = 0) THEN
            BEGIN
              UPDATE pro_cus_reward_detail SET quantity_manual = v.soLuongManual, update_date = sysdate, update_user = username, amount = v.soLuongManual*v.donGia
              WHERE pro_cus_reward_id = cusrewardid AND pro_gift_detail_id = v.pro_gift_detail_id;
              DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD_DETAIL where pro_cus_reward_id = ' || cusrewardid);
            END;
            ELSE
            BEGIN
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_MANUAL, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, cusrewardid, v.pro_gift_detail_id, v.soLuongManual, v.donGia, v.soLuongManual*v.donGia, sysdate, username);
              DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
            END;
            END IF;
          END LOOP;
        END;
        END IF;
        UPDATE pro_cus_reward SET total_amount = totalAmount, update_date = sysdate, update_user = username, result_manual = result
        WHERE pro_cus_reward_id = cusrewardid;
        DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD where pro_cus_reward_id = ' || cusrewardid);
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      ELSE
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo tien trinh');
        rewardID := PRO_CUS_REWARD_SEQ.nextval;
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          FOR v IN c_KTong
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuong;
            INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
            VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
            DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
          END LOOP;
        END;
        END IF;
        INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, CREATE_DATE, CREATE_user, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
        VALUES(rewardID, procusmapid, totalAmount, sysdate, username, periodid, result, proinfoid);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
      END;
      END IF;
    END;
    COMMIT;
  
    -- Tong ap dung cho SP chua khai bao
    ELSIF (loaiApDung = 2) THEN
    BEGIN
      DBMS_OUTPUT.put_line ('Loai co Tong ap dung cho SP chua khai bao');
      OPEN c_TongChuaKBDK;
      FETCH c_TongChuaKBDK INTO result;
      IF c_TongChuaKBDK%notfound THEN result := 0;
      END IF;
      CLOSE c_TongChuaKBDK;
  
      IF (typeprocess = 1) THEN
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo cach sua tay');
        OPEN c_ProCusReward;
        FETCH c_ProCusReward INTO cusrewardid;
        CLOSE c_ProCusReward;
  
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          checkinsert := 0;
          BEGIN
            SELECT crd.pro_cus_reward_detail_id INTO cusrewarddetailid
            FROM pro_cus_reward_detail crd WHERE crd.pro_cus_reward_id = cusrewardid;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN checkinsert := 1;
              WHEN TOO_MANY_ROWS THEN checkinsert := 0;
          END;
          FOR v IN c_TongChuaKB
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuongManual;
            IF (checkinsert = 0) THEN
            BEGIN
              UPDATE pro_cus_reward_detail SET quantity_manual = v.soLuongManual, update_date = sysdate, update_user = username, amount = v.soLuongManual*v.donGia
              WHERE pro_cus_reward_id = cusrewardid AND pro_gift_detail_id = v.pro_gift_detail_id;
              DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD_DETAIL where pro_cus_reward_id = ' || cusrewardid);
            END;
            ELSE
            BEGIN
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_MANUAL, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, cusrewardid, v.pro_gift_detail_id, v.soLuongManual, v.donGia, v.soLuongManual*v.donGia, sysdate, username);
              DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
            END;
            END IF;
          END LOOP;
        END;
        END IF;
        UPDATE pro_cus_reward SET total_amount = totalAmount, update_date = sysdate, update_user = username, result_manual = result
        WHERE pro_cus_reward_id = cusrewardid;
        DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD where pro_cus_reward_id = ' || cusrewardid);
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      ELSE
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo tien trinh');
        rewardID := PRO_CUS_REWARD_SEQ.nextval;
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          FOR v IN c_TongChuaKB
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuong;
            INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
            VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
            DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
          END LOOP;
        END;
        END IF;
        INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, CREATE_DATE, CREATE_user, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
        VALUES(rewardID, procusmapid, totalAmount, sysdate, username, periodid, result, proinfoid);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      END IF;
    END;
    COMMIT;
  
    -- Tong ap dung chung cho tat ca SP
    ELSE
    BEGIN
      DBMS_OUTPUT.put_line ('Loai co Tong ap dung chung cho tat ca SP');
      OPEN c_TongChungDK;
      FETCH c_TongChungDK INTO result;
      IF c_TongChungDK%notfound THEN result := 0;
      END IF;
      CLOSE c_TongChungDK;
  
      IF (typeprocess = 1) THEN
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo cach sua tay');
        OPEN c_ProCusReward;
        FETCH c_ProCusReward INTO cusrewardid;
        CLOSE c_ProCusReward;
  
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          checkinsert := 0;
          BEGIN
            SELECT crd.pro_cus_reward_detail_id INTO cusrewarddetailid
            FROM pro_cus_reward_detail crd WHERE crd.pro_cus_reward_id = cusrewardid;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN checkinsert := 1;
              WHEN TOO_MANY_ROWS THEN checkinsert := 0;
          END;
          FOR v IN c_TongChung
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuongManual;
            IF (checkinsert = 0) THEN
            BEGIN
              UPDATE pro_cus_reward_detail SET quantity_manual = v.soLuongManual, update_date = sysdate, update_user = username, amount = v.soLuongManual*v.donGia
              WHERE pro_cus_reward_id = cusrewardid AND pro_gift_detail_id = v.pro_gift_detail_id;
              DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD_DETAIL where pro_cus_reward_id = ' || cusrewardid);
            END;
            ELSE
            BEGIN
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_MANUAL, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, cusrewardid, v.pro_gift_detail_id, v.soLuongManual, v.donGia, v.soLuongManual*v.donGia, sysdate, username);
              DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
            END;
            END IF;
          END LOOP;
        END;
        END IF;
        UPDATE pro_cus_reward SET total_amount = totalAmount, update_date = sysdate, update_user = username, result_manual = result
        WHERE pro_cus_reward_id = cusrewardid;
        DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD where pro_cus_reward_id = ' || cusrewardid);
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      ELSE
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo tien trinh');
        rewardID := PRO_CUS_REWARD_SEQ.nextval;
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          FOR v IN c_TongChung
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuong;
            INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
            VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
            DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
          END LOOP;
        END;
        END IF;
        INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, CREATE_DATE, CREATE_user, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
        VALUES(rewardID, procusmapid, totalAmount, sysdate, username, periodid, result, proinfoid);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      END IF;
    END;
    END IF;
    COMMIT;
  END PRO_HT01_CUS_REWARD_01;
  
  PROCEDURE PRO_HT01_CUS_REWARD_02 (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, typeprocess NUMBER, username STRING)
  -- Tinh thuong HT01 loai ho tro Muc Tien, luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  -- typeprocess: 0: tien trinh; 1: nhap tay manual
  IS
    -- Loai 1: Tinh thuong chuong trinh HT01 Ho tro Muc Tien cho Tong SL khong co TONG
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_KTongDK
    IS
      WITH
      spDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND (cp.quantity >= sd.min_limit)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh thuong
    CURSOR c_KTong
    IS
      -- Chi lay nhung san pham co khai bao
      SELECT gd.pro_gift_detail_id
            , cp.product_id
            ,CASE WHEN cp.quantity <= NVL(sd.max_limit, 0) THEN cp.quantity
              ELSE NVL(sd.max_limit, 0) END soLuong
            ,CASE WHEN NVL(cp.quantity, 0) <= NVL(sd.max_limit, 0) THEN cp.quantity
              ELSE NVL(sd.max_limit, 0) END soLuongManual
            ,NVL(gd.unit_cost, 0) donGia
      FROM pro_cus_process cp
      JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
      JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
      WHERE 1=1
        AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
      ;
  
    -- Loai 2: Tinh thuong chuong trinh HT01 Ho tro Muc Tien cho Tong SL co TONG tung SP chua khai bao
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChuaKBDK
    IS
      WITH
      tongSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 1 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1
      )
      ,spDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND (cp.quantity >= sd.min_limit)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,dongTong AS (
        SELECT NVL(sd.min_limit, 0) chanDuoi
        FROM pro_cus_map cm
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND cm.pro_cus_map_id = procusmapid
          AND s.type IN (2)
          AND sd.type = 2 AND sd.unit_type = 1
      )
      ,spKhongDK AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND cp.product_id NOT IN (SELECT t.product_id FROM spDK t)
            )t
      )
--      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
--                        AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
--                  ELSE 1
--            END ketQua
--      FROM dual;
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE 
                        ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t)
                        OR
                        (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT count(1) FROM spDK) = (SELECT count(1) FROM tongSP))
                        ) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh thuong
    CURSOR c_TongChuaKB
    IS
      WITH
      -- Tinh thuong cho nhung san pham co khai bao (max_limit NOT NULL)
      spKhaiBao AS(
        SELECT gd.pro_gift_detail_id, cp.product_id
              , CASE WHEN cp.quantity <= sd.max_limit THEN cp.quantity
                ELSE sd.max_limit END soLuong
              , CASE WHEN NVL(cp.quantity, 0) <= NVL(sd.max_limit, 0) THEN cp.quantity
                ELSE NVL(sd.max_limit, 0) END soLuongManual
              , NVL(gd.unit_cost, 0) donGia
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
        JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND sd.max_limit IS NOT NULL
      )
      -- Tinh thuong cho nhung san pham chua khai bao (max_limit IS NULL)
      ,spChuaKhaiBao AS(
        SELECT t.pro_gift_detail_id, NULL product_id
              ,CASE WHEN t.giaTri <= t.max_limit THEN t.giaTri
              ELSE t.max_limit END soLuong
              ,CASE WHEN t.giaTriManual <= t.max_limit THEN t.giaTriManual
              ELSE t.max_limit END soLuongManual
              ,t.donGia
        FROM(  
          -- Chi co 1 dong
          SELECT gd.pro_gift_detail_id, sd.min_limit, sd.max_limit
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id NOT IN (SELECT t.product_id FROM spKhaiBao t)), 0) giaTri
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id NOT IN (SELECT t.product_id FROM spKhaiBao t)), 0) giaTriManual
                ,NVL(gd.unit_cost, 0) donGia
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
          JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid
            AND s.type IN (2) 
            AND g.type = 2
            AND sd.type = 2 AND sd.unit_type = 1
            AND i.type = 1 AND i.status = 1
            AND gd.type_apply = 2
        )t
      )
      SELECT * FROM spKhaiBao
      UNION ALL
      SELECT * FROM spChuaKhaiBao
      ;
  
    -- Loai 3: Tinh thuong chuong trinh HT01 Ho tro Muc Tien cho Tong SL co TONG chung cho tat ca SP
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChungDK
    IS
      WITH
      spDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
          AND (cp.quantity >= sd.min_limit)
          AND sd.type = 1 AND sd.unit_type = 1 AND sd.is_required_product = 1 AND sd.min_limit IS NOT NULL
      )
      ,dongTong AS (
        SELECT NVL(sd.min_limit, 0) chanDuoi
        FROM pro_cus_map cm
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND cm.pro_cus_map_id = procusmapid
          AND s.type IN (1)
          AND sd.type = 2 AND sd.unit_type = 1
      )
      ,tatCaSP AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
                        AND (SELECT t.soLuong FROM tatCaSP t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
  
    -- Tinh thuong
    CURSOR c_TongChung
    IS
      WITH
      -- Tinh thuong cho nhung san pham co khai bao (max_limit NOT NULL)
      spKhaiBao AS(
        SELECT gd.pro_gift_detail_id, cp.product_id
              , CASE WHEN cp.quantity <= sd.max_limit THEN cp.quantity
                ELSE sd.max_limit END soLuong
              , CASE WHEN NVL(cp.quantity, 0) <= NVL(sd.max_limit, 0) THEN cp.quantity
                ELSE NVL(sd.max_limit, 0) END soLuongManual
              , NVL(gd.unit_cost, 0) donGia
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
        JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND sd.max_limit IS NOT NULL
      )
      -- Tinh thuong cho nhung san pham chua khai bao (max_limit IS NULL)
      ,spChuaKhaiBao AS(
        SELECT t.pro_gift_detail_id, NULL product_id
              ,CASE WHEN t.giaTri <= (t.max_limit - NVL((SELECT SUM(t.soLuong) FROM spKhaiBao t), 0)) THEN t.giaTri
              ELSE (t.max_limit - NVL((SELECT SUM(t.soLuong) FROM spKhaiBao t), 0)) END soLuong
              ,CASE WHEN t.giaTriManual <= (t.max_limit - NVL((SELECT SUM(t.soLuongManual) FROM spKhaiBao t), 0)) THEN t.giaTriManual
              ELSE (t.max_limit - NVL((SELECT SUM(t.soLuongManual) FROM spKhaiBao t), 0)) END soLuongManual
              ,t.donGia
        FROM(  
          -- Chi co 1 dong
          SELECT gd.pro_gift_detail_id, sd.min_limit, sd.max_limit
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id NOT IN (SELECT t.product_id FROM spKhaiBao t)), 0) giaTri
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id NOT IN (SELECT t.product_id FROM spKhaiBao t)), 0) giaTriManual
                ,NVL(gd.unit_cost, 0) donGia
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
          JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid
            AND s.type IN (1)
            AND g.type = 2
            AND sd.type = 2 AND sd.unit_type = 1
            AND i.type = 1 AND i.status = 1
            AND gd.type_apply = 2
        )t
      )
      SELECT * FROM spKhaiBao
      UNION ALL
      SELECT * FROM spChuaKhaiBao
      ;
  
    -- Loai ap dung: 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
    CURSOR c_LoaiAD
    IS
      SELECT CASE WHEN sd.min_limit IS NULL AND sd.max_limit IS NULL THEN 1
                  WHEN s.type = 2 THEN 2
              ELSE 3 END loaiAD
      FROM pro_cus_map cm
      JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND  cm.pro_cus_map_id = procusmapid AND sd.type = 2;
  
    -- Danh cho loai cap nhat bang tay: Lay PRO_CUS_REWARD_ID
    CURSOR c_ProCusReward
    IS
      SELECT pro_cus_reward_id
      FROM pro_cus_reward
      WHERE pro_cus_map_id = procusmapid AND pro_period_id = periodid;
  
    CURSOR c_MucTien
    IS
      SELECT g.cost mucTien
      FROM pro_cus_map cm
      JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND  cm.pro_cus_map_id = procusmapid;
    
    checkinsert NUMBER(1);
    loaiApDung NUMBER(3,0);
    idDoiTuong NUMBER(20,0);
  
    rewardID NUMBER(20,0);
    giftDetailID NUMBER(20,0);
    productID NUMBER(20,0);
    soLuong NUMBER(20,0);
    donGia NUMBER(20,0);
    result NUMBER(1,0);
    totalAmount NUMBER(20,0);
    cusrewardid NUMBER(20,0);
    cusrewarddetailid NUMBER(20,0);
    mucTien NUMBER(20,0) := 0;
    
  BEGIN
    OPEN c_LoaiAD;
    FETCH c_LoaiAD INTO loaiApDung;
    CLOSE c_LoaiAD;
  
    OPEN c_MucTien;
    FETCH c_MucTien INTO mucTien;
    CLOSE c_MucTien;
  
    -- Khong co tong
    IF (loaiApDung = 1) THEN
    BEGIN
      DBMS_OUTPUT.put_line ('Loai khong co Tong');
      OPEN c_KTongDK;
      FETCH c_KTongDK INTO result;
      IF c_KTongDK%notfound THEN result := 0;
      END IF;
      CLOSE c_KTongDK;
  
      IF (typeprocess = 1) THEN
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo cach sua tay');
        OPEN c_ProCusReward;
        FETCH c_ProCusReward INTO cusrewardid;
        CLOSE c_ProCusReward;
  
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          checkinsert := 0;
          BEGIN
            SELECT crd.pro_cus_reward_detail_id INTO cusrewarddetailid
            FROM pro_cus_reward_detail crd WHERE crd.pro_cus_reward_id = cusrewardid;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN checkinsert := 1;
              WHEN TOO_MANY_ROWS THEN checkinsert := 0;
          END;
          FOR v IN c_KTong
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuongManual;
            IF (checkinsert = 0) THEN
            BEGIN
              UPDATE pro_cus_reward_detail SET quantity_manual = v.soLuongManual, update_date = sysdate, update_user = username, amount = v.soLuongManual*v.donGia
              WHERE pro_cus_reward_id = cusrewardid AND pro_gift_detail_id = v.pro_gift_detail_id;
              DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD_DETAIL where pro_cus_reward_id = ' || cusrewardid);
            END;
            ELSE
            BEGIN
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_MANUAL, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, cusrewardid, v.pro_gift_detail_id, v.soLuongManual, v.donGia, v.soLuongManual*v.donGia, sysdate, username);
              DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
            END;
            END IF;
          END LOOP;
          totalAmount := totalAmount + mucTien;
        END;
        END IF;
        UPDATE pro_cus_reward SET total_amount = totalAmount, update_date = sysdate, update_user = username, result_manual = result
        WHERE pro_cus_reward_id = cusrewardid;
        DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD where pro_cus_reward_id = ' || cusrewardid);
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      ELSE
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo tien trinh');
        rewardID := PRO_CUS_REWARD_SEQ.nextval;
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          FOR v IN c_KTong
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuong;
            INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
            VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
            DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
          END LOOP;
          totalAmount := totalAmount + mucTien;
        END;
        END IF;
        INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
        VALUES(rewardID, procusmapid, totalAmount, sysdate, username, periodid, result, proinfoid);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      END IF;
    END;
    COMMIT;
  
    -- Tong ap dung cho SP chua khai bao
    ELSIF (loaiApDung = 2) THEN
    BEGIN
      DBMS_OUTPUT.put_line ('Loai co Tong ap dung cho SP chua khai bao');
      OPEN c_TongChuaKBDK;
      FETCH c_TongChuaKBDK INTO result;
      IF c_TongChuaKBDK%notfound THEN result := 0;
      END IF;
      CLOSE c_TongChuaKBDK;
  
      IF (typeprocess = 1) THEN
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo cach sua tay');
        OPEN c_ProCusReward;
        FETCH c_ProCusReward INTO cusrewardid;
        CLOSE c_ProCusReward;
  
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          checkinsert := 0;
          BEGIN
            SELECT crd.pro_cus_reward_detail_id INTO cusrewarddetailid
            FROM pro_cus_reward_detail crd WHERE crd.pro_cus_reward_id = cusrewardid;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN checkinsert := 1;
              WHEN TOO_MANY_ROWS THEN checkinsert := 0;
          END;
          FOR v IN c_TongChuaKB
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuongManual;
            IF (checkinsert = 0) THEN
            BEGIN
              UPDATE pro_cus_reward_detail SET quantity_manual = v.soLuongManual, update_date = sysdate, update_user = username, amount = v.soLuongManual*v.donGia
              WHERE pro_cus_reward_id = cusrewardid AND pro_gift_detail_id = v.pro_gift_detail_id;
              DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD_DETAIL where pro_cus_reward_id = ' || cusrewardid);
            END;
            ELSE
            BEGIN
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_MANUAL, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, cusrewardid, v.pro_gift_detail_id, v.soLuongManual, v.donGia, v.soLuongManual*v.donGia, sysdate, username);
              DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
            END;
            END IF;
          END LOOP;
          totalAmount := totalAmount + mucTien;
        END;
        END IF;
        UPDATE pro_cus_reward SET total_amount = totalAmount, update_date = sysdate, update_user = username, result_manual = result
        WHERE pro_cus_reward_id = cusrewardid;
        DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD where pro_cus_reward_id = ' || cusrewardid);
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      ELSE
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo tien trinh');
        rewardID := PRO_CUS_REWARD_SEQ.nextval;
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          FOR v IN c_TongChuaKB
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuong;
            INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
            VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
            DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
          END LOOP;
          totalAmount := totalAmount + mucTien;
        END;
        END IF;
        INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
        VALUES(rewardID, procusmapid, totalAmount, sysdate, username, periodid, result, proinfoid);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      END IF;
    END;
    COMMIT;
  
    -- Tong ap dung chung cho tat ca SP
    ELSE
    BEGIN
      DBMS_OUTPUT.put_line ('Loai co Tong ap dung chung cho tat ca SP');
      OPEN c_TongChungDK;
      FETCH c_TongChungDK INTO result;
      IF c_TongChungDK%notfound THEN result := 0;
      END IF;
      CLOSE c_TongChungDK;
  
      IF (typeprocess = 1) THEN
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo cach sua tay');
        OPEN c_ProCusReward;
        FETCH c_ProCusReward INTO cusrewardid;
        CLOSE c_ProCusReward;
  
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          checkinsert := 0;
          BEGIN
            SELECT crd.pro_cus_reward_detail_id INTO cusrewarddetailid
            FROM pro_cus_reward_detail crd WHERE crd.pro_cus_reward_id = cusrewardid;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN checkinsert := 1;
              WHEN TOO_MANY_ROWS THEN checkinsert := 0;
          END;
          FOR v IN c_TongChung
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuongManual;
            IF (checkinsert = 0) THEN
            BEGIN
              UPDATE pro_cus_reward_detail SET quantity_manual = v.soLuongManual, update_date = sysdate, update_user = username, amount = v.soLuongManual*v.donGia
              WHERE pro_cus_reward_id = cusrewardid AND pro_gift_detail_id = v.pro_gift_detail_id;
              DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD_DETAIL where pro_cus_reward_id = ' || cusrewardid);
            END;
            ELSE
            BEGIN
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_MANUAL, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, cusrewardid, v.pro_gift_detail_id, v.soLuongManual, v.donGia, v.soLuongManual*v.donGia, sysdate, username);
              DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
            END;
            END IF;
          END LOOP;
          totalAmount := totalAmount + mucTien;
        END;
        END IF;
        UPDATE pro_cus_reward SET total_amount = totalAmount, update_date = sysdate, update_user = username, result_manual = result
        WHERE pro_cus_reward_id = cusrewardid;
        DBMS_OUTPUT.put_line ('updated 1 row in PRO_CUS_REWARD where pro_cus_reward_id = ' || cusrewardid);
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      ELSE
      BEGIN
        DBMS_OUTPUT.put_line ('Chay theo tien trinh');
        rewardID := PRO_CUS_REWARD_SEQ.nextval;
        totalAmount := 0;
        IF (result = 1) THEN
        BEGIN
          FOR v IN c_TongChung
          LOOP
            totalAmount := totalAmount + v.donGia*v.soLuong;
            INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
            VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
            DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
          END LOOP;
          totalAmount := totalAmount + mucTien;
        END;
        END IF;
        INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
        VALUES(rewardID, procusmapid, totalAmount, sysdate, username, periodid, result, proinfoid);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
        DBMS_OUTPUT.put_line ('Ket qua ' || result);
      END;
      END IF;
    END;
    END IF;
    COMMIT;
  END PRO_HT01_CUS_REWARD_02;
  
  PROCEDURE PRO_HT02_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  -- Tinh thuong HT02 luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  IS
    -- Loai 1: Tinh thuong chuong trinh HT02 cho loai khong co dong Tong
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_KTongDK (v_sosuat IN NUMBER)
    IS
      WITH
      -- Lay danh sach SP co chan duoi SO LUONG (MIN_LIMIT IS NOT NULL)
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (1, 2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND (
              (cp.quantity >= sd.min_limit AND sd.unit_type = 1)
            OR 
              (cp.quantity*sd.convfact >= sd.min_limit AND sd.unit_type = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh he so
    CURSOR c_KTong (v_sosuat IN NUMBER)
    IS
      SELECT MIN(t.heSo) heSo
      FROM (
        SELECT s.pro_structure_id
              , sd.pro_structure_detail_id
              , CASE WHEN sd.unit_type = 1 THEN FLOOR(NVL(cp.quantity, 0) / sd.min_limit) 
                     WHEN sd.unit_type = 2 THEN FLOOR(NVL(cp.quantity, 0) * sd.convfact / sd.min_limit) 
                END heSo
              , sd.level_number soSuat
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.min_limit != 0 AND sd.level_number = v_sosuat
          )t;

    -- Loai 2: Tinh thuong chuong trinh HT02 co TONG tung SP chua khai bao
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChuaKBDK (v_sosuat IN NUMBER)
    IS
      WITH
      tongSoSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.level_number = v_sosuat
      )
      ,spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND (
              (cp.quantity >= sd.min_limit AND sd.unit_type = 1)
            OR 
              (cp.quantity*sd.convfact >= sd.min_limit AND sd.unit_type = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,dongTong AS (
        SELECT sd.min_limit chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type IN (2)
          AND sd.type = 2 AND sd.level_number = v_sosuat
      )
      ,spKhongDK AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT CASE WHEN sd.unit_type = 1 THEN cp.quantity
                      WHEN sd.unit_type = 2 THEN cp.quantity * sd.convfact
                 END soLuong
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND cp.product_id NOT IN (SELECT t.product_id FROM spDK t)
            AND s.type IN (2)
            AND sd.type = 1 AND sd.level_number = v_sosuat
            )t
      )
      -- Ket qua Dat, neu cac SP dieu kien thoa dieu kien, cac SP chua khai bao thoa dieu kien Tong hoac khong co SP chua khai bao
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE 
                          ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t))
                          OR
                          ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT count(1) FROM spDK) = (SELECT count(1) FROM tongSoSP))
                        ) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh he so
    CURSOR c_TongChuaKB (v_sosuat IN NUMBER)
    IS
      WITH
      -- MIN_LIMIT IS NULL
      spChuaKB AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NULL AND sd.level_number = v_sosuat
      )
      -- MIN_LIMIT IS NOT NULL
      ,spKhaiBao AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,heSoSPKB AS(
        SELECT MIN(t.heSo) heSo
        FROM (
          SELECT sd.pro_structure_detail_id
                , CASE WHEN sd.unit_type = 1 THEN FLOOR(NVL(cp.quantity, 0) / sd.min_limit) 
                       WHEN sd.unit_type = 2 THEN FLOOR(NVL(cp.quantity, 0) * sd.convfact / sd.min_limit) 
                  END heSo
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.min_limit != 0 AND sd.level_number = v_sosuat
            AND cp.product_id IN (SELECT t.product_id FROM spKhaiBao t)
            )t
      )
      ,heSoSPChuaKB AS(
        SELECT FLOOR(NVL(t.heSo, 0) / t.min_limit) heSo
        FROM(  
          -- Chi co 1 dong
          SELECT sd.min_limit
                , CASE WHEN sd.unit_type = 1 THEN
                        NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM spChuaKB t)), 0)
                        WHEN sd.unit_type = 2 THEN
                        NVL((SELECT SUM(cp.quantity * sd.convfact) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM spChuaKB t)), 0)
                  END heSo
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND s.type IN (2) AND sd.type = 2 AND i.type = 2 AND i.status = 1 AND sd.min_limit != 0  AND sd.level_number = v_sosuat
        )t
      )
      SELECT CASE WHEN (SELECT product_id FROM spChuaKB  WHERE ROWNUM = 1) IS NULL THEN (SELECT heSo FROM heSoSPKB)
                WHEN (SELECT product_id FROM spKhaiBao WHERE ROWNUM = 1) IS NULL THEN (SELECT heSo FROM heSoSPChuaKB)
                ELSE LEAST((SELECT heSo FROM heSoSPKB), (SELECT heSo FROM heSoSPChuaKB))
            END heSo
      FROM dual;

    -- Loai 3: Tinh thuong chuong trinh HT02 co TONG chung cho tat ca SP
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChungDK (v_sosuat IN NUMBER)
    IS
      WITH
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
          AND (
              (cp.quantity >= sd.min_limit AND sd.unit_type = 1)
            OR 
              (cp.quantity*sd.convfact >= sd.min_limit AND sd.unit_type = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,dongTong AS (
        SELECT sd.min_limit chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type IN (1)
          AND sd.type = 2 AND sd.level_number = v_sosuat
      )
      ,tatCaSP AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT CASE WHEN sd.unit_type = 1 THEN cp.quantity
                      WHEN sd.unit_type = 2 THEN cp.quantity * sd.convfact
                 END soLuong
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND s.type IN (1)
            AND sd.type = 1 AND sd.level_number = v_sosuat
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
                        AND (SELECT t.soLuong FROM tatCaSP t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh he so
    CURSOR c_TongChung (v_sosuat IN NUMBER)
    IS
      WITH
      tongSoSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.level_number = v_sosuat
      )
      ,spKhaiBao AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,heSoSPKB AS(
        SELECT MIN(t.heSo) heSo
        FROM (
          SELECT sd.pro_structure_detail_id
                , CASE WHEN sd.unit_type = 1 THEN FLOOR(NVL(cp.quantity, 0) / sd.min_limit) 
                       WHEN sd.unit_type = 2 THEN FLOOR(NVL(cp.quantity, 0) * sd.convfact / sd.min_limit) 
                  END heSo
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.min_limit != 0 AND sd.level_number = v_sosuat
            AND cp.product_id IN (SELECT t.product_id FROM spKhaiBao t)
            )t
      )
      ,heSoSPChuaKB AS(
        SELECT FLOOR(NVL(t.heSo, 0) / t.min_limit) heSo
        FROM(  
          -- Chi co 1 dong
          SELECT sd.min_limit
                , CASE WHEN sd.unit_type = 1 THEN
                        NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM tongSoSP t)), 0)
                        WHEN sd.unit_type = 2 THEN
                        NVL((SELECT SUM(cp.quantity * sd.convfact) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM tongSoSP t)), 0)
                  END heSo
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND s.type IN (1) AND sd.type = 2 AND i.type = 2 AND i.status = 1 AND sd.min_limit != 0 AND sd.level_number = v_sosuat
        )t
      )
      SELECT
        CASE 
            WHEN (SELECT product_id FROM spKhaiBao WHERE ROWNUM = 1) IS NULL THEN (SELECT heSo FROM heSoSPChuaKB)
            ELSE LEAST((SELECT heSo FROM heSoSPKB), (SELECT heSo FROM heSoSPChuaKB))
        END heSo
      FROM dual;

    -- Thong tin ho tro theo suat
    CURSOR c_HoTro (v_sosuat IN NUMBER)
    IS
      SELECT gd.pro_gift_detail_id, gd.unit_cost donGia, gd.quantity soLuong, pi.object_type loaiHoTro
      FROM pro_structure s
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
      JOIN product_info pi ON pi.product_info_id = gd.type_support
      WHERE s.pro_info_id = proinfoid AND gd.level_number = v_sosuat AND pi.type = 1
      ;
    -- Lay ra tong so suat
    CURSOR c_Suat
    IS
      SELECT t.soSuat
      FROM (
        SELECT DISTINCT sd.level_number soSuat
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE s.pro_info_id = proinfoid
        )t
      ORDER BY t.soSuat
      ;

    -- Lay ra han muc, han muc co the = NULL hoac > 0 (khong co han muc = 0)
    CURSOR c_HanMuc
    IS
      SELECT s.quota giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;

    -- Loai ap dung: 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
    CURSOR c_LoaiAD (v_sosuat IN NUMBER)
    IS
      SELECT CASE WHEN sd.min_limit IS NULL THEN 1
                  WHEN s.type = 2 THEN 2
                  ELSE 3 END loaiAD
      FROM pro_info i
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND i.pro_info_id = proinfoid AND sd.level_number = v_sosuat AND sd.type = 2;
  
    -- Danh cho loai cap nhat bang tay: Lay PRO_CUS_REWARD_ID
    CURSOR c_ProCusReward
    IS
      SELECT pro_cus_reward_id
      FROM pro_cus_reward
      WHERE pro_cus_map_id = procusmapid AND pro_period_id = periodid;
    
    loaiApDung NUMBER(3,0);
    rewardID NUMBER(20,0);
    rewardDetailID NUMBER(20,0);
    soLuong NUMBER(20,0);
    donGia NUMBER(20,0);
    result NUMBER(1,0);
    totalAmount NUMBER(20,0);
    isAchieved NUMBER(20, 0) := 0;
    hanMuc NUMBER(20, 0);
    heSoTmp NUMBER(20,0);
    heSo NUMBER(20,0);
    soSuatThuong NUMBER(20,0) := 0;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT02_CUS_REWARD started');
    OPEN c_HanMuc;
    FETCH c_HanMuc INTO hanMuc;
    CLOSE c_HanMuc;

    totalAmount := 0;
    DBMS_OUTPUT.put_line ('Chay theo tien trinh');
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    
    FOR s IN c_Suat
    LOOP
      -- Neu con han muc
      IF (hanMuc > 0 OR hanMuc IS NULL) THEN
        OPEN c_LoaiAD (s.soSuat);
        FETCH c_LoaiAD INTO loaiApDung;
        CLOSE c_LoaiAD;

        -- Khong co tong
        IF (loaiApDung = 1) THEN
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Loai khong co Tong');
          OPEN c_KTongDK (s.soSuat);
          FETCH c_KTongDK INTO result;
          IF c_KTongDK%notfound THEN result := 0;
          END IF;
          CLOSE c_KTongDK;

          OPEN c_KTong(s.soSuat);
          FETCH c_KTong INTO heSoTmp;
          CLOSE c_KTong;

        -- Tong ap dung cho SP chua khai bao
        ELSIF (loaiApDung = 2) THEN
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Loai co Tong ap dung cho SP chua khai bao');
          OPEN c_TongChuaKBDK (s.soSuat);
          FETCH c_TongChuaKBDK INTO result;
          IF c_TongChuaKBDK%notfound THEN result := 0;
          END IF;
          CLOSE c_TongChuaKBDK;

          OPEN c_TongChuaKB(s.soSuat);
          FETCH c_TongChuaKB INTO heSoTmp;
          CLOSE c_TongChuaKB;

        -- Tong ap dung chung cho tat ca SP
        ELSE
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Loai co Tong ap dung chung cho tat ca SP');
          OPEN c_TongChungDK (s.soSuat);
          FETCH c_TongChungDK INTO result;
          IF c_TongChungDK%notfound THEN result := 0;
          END IF;
          CLOSE c_TongChungDK;

          OPEN c_TongChung(s.soSuat);
          FETCH c_TongChung INTO heSoTmp;
          CLOSE c_TongChung;
        END IF;

        -- Suat dat
        IF (result = 1) THEN
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Dat thuong');
          isAchieved := 1;
          
          IF (hanMuc IS NULL) THEN
            heSo := heSoTmp;
          ELSIF (hanMuc < heSoTmp AND hanMuc IS NOT NULL) THEN
            heSo := hanMuc;
            hanMuc := 0;
          ELSIF (hanMuc >= heSoTmp AND hanMuc IS NOT NULL) THEN
            hanMuc := hanMuc - heSoTmp;
            heSo := heSoTmp;
          END IF;

          soSuatThuong := soSuatThuong + heSo;

          FOR v IN c_HoTro (s.soSuat)
          LOOP
            totalAmount := totalAmount + v.soLuong*heSo*v.donGia;
            IF (v.loaiHoTro = 4) THEN
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia*heSo, v.soLuong*heSo*v.donGia, sysdate, username);
            ELSE
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong*heSo, v.donGia, v.soLuong*heSo*v.donGia, sysdate, username);
            END IF;
            DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
          END LOOP;
        ELSE
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Khong dat thuong');
        END IF;
      END IF;
    END LOOP;
    INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, LEVEL_AUTO, CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
    VALUES(rewardID, procusmapid, totalAmount, soSuatThuong, sysdate, username, periodid, isAchieved, proinfoid);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    DBMS_OUTPUT.put_line ('Ket qua ' || isAchieved);
    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT02_CUS_REWARD finished');
  END PRO_HT02_CUS_REWARD;
  
  PROCEDURE PRO_HT03_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  IS
    -- Loai 1: Tinh thuong chuong trinh HT03 loai khong co TONG
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_KTongDK (v_soMuc NUMBER)
    IS
      WITH
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (1,2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND cp.quantity >= (cmd.quantity*sd.min_limit/100)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh thuong
    CURSOR c_KTong (v_soMuc NUMBER)
    IS
      SELECT gd.pro_gift_detail_id , cp.product_id 
            ,CASE WHEN cp.quantity <= (cmd.quantity + (cmd.quantity*sd.max_limit/100)) THEN cp.quantity
              ELSE ROUND(cmd.quantity + (cmd.quantity*sd.max_limit/100)) END soLuong
            ,NVL(gd.unit_cost, 0) donGia
      FROM pro_cus_process cp
      JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
      JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
      JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
      WHERE 1=1
        AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.type = 1 AND sd.level_number = v_soMuc
        AND cm.level_number = v_soMuc AND gd.level_number = v_soMuc;
  
    -- Loai 2: Tinh thuong chuong trinh HT03 co TONG tung SP chua khai bao
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChuaKBDK (v_soMuc NUMBER)
    IS
      WITH
      tongSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.level_number = v_soMuc
      )
      -- MIN_LIMIT IS NOT NULL
      ,spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND cp.quantity >= (cmd.quantity*sd.min_limit/100)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc
      )
      ,dongTong AS (
        SELECT (t1.slDangKy*t2.chanDuoi/100) chanDuoi
        FROM(
          SELECT SUM(cmd.quantity) slDangKy
          FROM pro_cus_map cm
          JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND cm.level_number = v_soMuc
            AND cmd.product_id NOT IN (SELECT t.product_id FROM spDK t)
            )t1,
            (
          SELECT NVL(sd.min_limit, 0) chanDuoi
          FROM pro_structure s
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND s.pro_info_id = proinfoid AND s.type IN (2)
            AND sd.type = 2 AND sd.level_number = v_soMuc
            )t2
      )
      ,spKhongDK AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND cp.product_id NOT IN (SELECT t.product_id FROM spDK t)
            AND sd.type = 1 AND sd.level_number = v_soMuc
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE 
                        ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t)
                        OR
                        (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT count(1) FROM spDK) = (SELECT count(1) FROM tongSP))
                        ) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh thuong
    CURSOR c_TongChuaKB (v_soMuc NUMBER)
    IS
      WITH
      -- MAX_LIMIT IS NULL
      dsSPChuaKBMax AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.max_limit IS NULL AND sd.level_number = v_soMuc
      )
      -- Tinh thuong cho nhung san pham co khai bao (max_limit NOT NULL)
      ,spKhaiBao AS(
        SELECT gd.pro_gift_detail_id, cp.product_id
              ,CASE WHEN cp.quantity <= (cmd.quantity + (cmd.quantity*sd.max_limit/100)) THEN cp.quantity
              ELSE ROUND(cmd.quantity + (cmd.quantity*sd.max_limit/100)) END soLuong
              , NVL(gd.unit_cost, 0) donGia
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
        JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND sd.max_limit IS NOT NULL AND sd.type = 1 AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc AND gd.level_number = v_soMuc
      )
      -- Tinh thuong cho nhung san pham chua khai bao (max_limit IS NULL)
      ,spChuaKhaiBao AS(
        SELECT t.pro_gift_detail_id, NULL product_id
              ,CASE WHEN t.slThucHien <= (t.slDangKy + t.slDangKy*t.max_limit/100) THEN t.slThucHien
              ELSE ROUND(t.slDangKy + t.slDangKy*t.max_limit/100) END soLuong
              ,t.donGia
        FROM(  
          -- Chi co 1 dong
          SELECT gd.pro_gift_detail_id, sd.min_limit, sd.max_limit
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id IN (SELECT t.product_id FROM dsSPChuaKBMax t)), 0) slThucHien
                , (SELECT SUM(cmd.quantity) FROM pro_cus_map cm
                  JOIN pro_cus_map_detail cmd ON cm.pro_cus_map_id = cmd.pro_cus_map_id
                  WHERE cm.pro_cus_map_id = procusmapid AND cm.level_number = v_soMuc AND cmd.product_id IN (SELECT t.product_id FROM dsSPChuaKBMax t)) slDangKy
                ,NVL(gd.unit_cost, 0) donGia
          FROM pro_info i
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
          JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
          WHERE 1=1
            AND i.pro_info_id = proinfoid
            AND s.type IN (2)
            AND sd.type = 2 AND sd.level_number = v_soMuc
            AND gd.type_apply = 2 AND gd.level_number = v_soMuc
        )t
      )
      SELECT t1.pro_gift_detail_id, t1.product_id, NVL(t1.soLuong, 0) soLuong, NVL(t1.donGia, 0) donGia FROM spKhaiBao t1
      UNION ALL
      SELECT t2.pro_gift_detail_id, t2.product_id, NVL(t2.soLuong, 0) soLuong, NVL(t2.donGia, 0) donGia FROM spChuaKhaiBao t2
      ;
  
    -- Loai 3: Tinh thuong chuong trinh HT03 Ho tro Ket/Thung,Tien cho Tong SL co TONG chung cho tat ca SP
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChungDK (v_soMuc NUMBER)
    IS
      WITH
      -- MIN_LIMIT IS NOT NULL
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
          AND cp.quantity >= (cmd.quantity*sd.min_limit/100)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc
      )
      ,dongTong AS (
        SELECT (t1.slDangKy*t2.chanDuoi/100) chanDuoi
        FROM(
          SELECT SUM(cmd.quantity) slDangKy
          FROM pro_cus_map cm 
          JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND cm.level_number = v_soMuc
            )t1,
            (
          SELECT NVL(sd.min_limit, 0) chanDuoi
          FROM pro_structure s
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND s.pro_info_id = proinfoid AND s.type IN (1)
            AND sd.type = 2 AND sd.level_number = v_soMuc
            )t2
      )
      -- Do trong bang PRO_CUS_PROCESS chi co 1 muc nen khong can xet muc o day
      ,tatCaSP AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
                        AND (SELECT t.soLuong FROM tatCaSP t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
  
    -- Tinh thuong
    CURSOR c_TongChung (v_soMuc NUMBER)
    IS
      WITH
      dsSPChuaKBMax AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.max_limit IS NULL AND sd.level_number = v_soMuc
      )
      -- Tinh thuong cho nhung san pham co khai bao (max_limit NOT NULL)
      ,spKhaiBao AS(
        SELECT gd.pro_gift_detail_id, cp.product_id
              ,CASE WHEN cp.quantity <= (cmd.quantity + (cmd.quantity*sd.max_limit/100)) THEN cp.quantity
              ELSE ROUND(cmd.quantity + (cmd.quantity*sd.max_limit/100)) END soLuong
              , NVL(gd.unit_cost, 0) donGia
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
        JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND sd.max_limit IS NOT NULL AND sd.type = 1 AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc AND gd.level_number = v_soMuc
      )
      -- Tinh thuong cho nhung san pham chua khai bao (max_limit IS NULL)
      ,spChuaKhaiBao AS(
        SELECT t.pro_gift_detail_id, NULL product_id
              ,CASE WHEN t.slThucHien <= ((t.slDangKy + t.slDangKy*t.max_limit/100) - NVL((SELECT SUM(t.soLuong) FROM spKhaiBao t), 0)) THEN t.slThucHien
              ELSE (ROUND(t.slDangKy + t.slDangKy*t.max_limit/100) - NVL((SELECT SUM(t.soLuong) FROM spKhaiBao t), 0)) END soLuong
              ,t.donGia
        FROM(  
          -- Chi co 1 dong
          SELECT gd.pro_gift_detail_id, sd.min_limit, sd.max_limit
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND cp.product_id IN (SELECT * FROM dsSPChuaKBMax) ), 0) slThucHien
                , (SELECT SUM(cmd.quantity) FROM pro_cus_map cm
                  JOIN pro_cus_map_detail cmd ON cm.pro_cus_map_id = cmd.pro_cus_map_id
                  WHERE cm.pro_cus_map_id = procusmapid AND cm.level_number = v_soMuc) slDangKy
                ,NVL(gd.unit_cost, 0) donGia
          FROM pro_info i
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
          JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
          WHERE 1=1
            AND i.pro_info_id = proinfoid
            AND s.type IN (1)
            AND sd.type = 2 AND sd.level_number = v_soMuc
            AND gd.type_apply = 2 AND gd.level_number = v_soMuc
        )t
      )
      SELECT * FROM spKhaiBao
      UNION ALL
      SELECT * FROM spChuaKhaiBao
      ;
  
    -- Loai ap dung: 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
    CURSOR c_LoaiAD (v_soMuc NUMBER)
    IS
      SELECT CASE WHEN sd.min_limit IS NULL AND sd.max_limit IS NULL THEN 1
                  WHEN s.type = 2 THEN 2
              ELSE 3 END loaiAD
      FROM pro_info i
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND i.pro_info_id = proinfoid AND sd.level_number = v_soMuc AND sd.type = 2;
    
    -- So muc do nguoi dung dang ky
    CURSOR c_SoMuc
    IS
      SELECT cm.level_number soMuc FROM pro_cus_map cm WHERE cm.pro_cus_map_id = procusmapid;

    loaiApDung NUMBER(3,0);
    rewardID NUMBER(20,0);
    result NUMBER(1,0);
    totalAmount NUMBER(20,0);
    soMuc NUMBER(20,0);
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT03_CUS_REWARD started');
    OPEN c_SoMuc;
    FETCH c_SoMuc INTO soMuc;
    CLOSE c_SoMuc;

    OPEN c_LoaiAD (soMuc);
    FETCH c_LoaiAD INTO loaiApDung;
    CLOSE c_LoaiAD;
  
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    totalAmount := 0;

    -- Khong co tong
    IF (loaiApDung = 1) THEN
      DBMS_OUTPUT.put_line ('Loai khong co Tong');
      OPEN c_KTongDK (soMuc);
      FETCH c_KTongDK INTO result;
      IF c_KTongDK%notfound THEN result := 0;
      END IF;
      CLOSE c_KTongDK;

      IF (result = 1) THEN
        FOR v IN c_KTong (soMuc)
        LOOP
          totalAmount := totalAmount + v.donGia*v.soLuong;
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
          DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
        END LOOP;
      END IF;
    ELSIF (loaiApDung = 2) THEN
      DBMS_OUTPUT.put_line ('Loai co Tong ap dung cho SP chua khai bao');
      OPEN c_TongChuaKBDK (soMuc);
      FETCH c_TongChuaKBDK INTO result;
      IF c_TongChuaKBDK%notfound THEN result := 0;
      END IF;
      CLOSE c_TongChuaKBDK;

      IF (result = 1) THEN
        FOR v IN c_TongChuaKB (soMuc)
        LOOP
          totalAmount := totalAmount + v.donGia*v.soLuong;
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
          DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
        END LOOP;
      END IF;
    ELSE
      DBMS_OUTPUT.put_line ('Loai co Tong ap dung chung cho tat ca SP');
      OPEN c_TongChungDK (soMuc);
      FETCH c_TongChungDK INTO result;
      IF c_TongChungDK%notfound THEN result := 0;
      END IF;
      CLOSE c_TongChungDK;

      IF (result = 1) THEN
        FOR v IN c_TongChung (soMuc)
        LOOP
          totalAmount := totalAmount + v.donGia*v.soLuong;
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
          DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
        END LOOP;
      END IF;
    END IF;
    
    INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, CREATE_DATE, CREATE_user, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
    VALUES(rewardID, procusmapid, totalAmount, sysdate, username, periodid, result, proinfoid);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    DBMS_OUTPUT.put_line ('Ket qua (0: khong dat, 1: dat): ' || result);
    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT03_CUS_REWARD finished');
  END PRO_HT03_CUS_REWARD;
  
  PROCEDURE PRO_HT05_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  -- Tinh thuong HT05 luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  IS
    -- Loai 1: Tinh thuong chuong trinh HT05 cho loai khong co dong Tong Mua
    -- Xet dieu kien so luong mua Dat hay Khong dat
    CURSOR c_KTongDKMua
    IS
      WITH
      -- Lay danh sach SP Mua co chan duoi SO LUONG (MIN_LIMIT IS NOT NULL)
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (1, 2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL 
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND (
              (cp.quantity >= sd.min_limit AND sd.unit_type = 1)
            OR 
              (cp.quantity*sd.convfact >= sd.min_limit AND sd.unit_type = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
      
      -- Xet dieu kien so luong trung bay Dat hay Khong dat
    CURSOR c_KTongDKTB
    IS
      WITH
      -- Lay danh sach SPTB co SO LUONG TRUNG BAY (DISPLAY_QUANTITY IS NOT NULL)
      spDKTB AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (1, 2)
          AND sd.type = 1 AND sd.display_quantity IS NOT NULL 
      )
      -- ds lan cham TB va so luong san pham trung bay dat
      ,spThoaDKTB AS(
        SELECT dp.pro_display_program_id, count(*) soSPTBDat --dp.pro_display_program_id, dpd.product_id, dpd.quantity, dp.image_display, sd.display_quantity
        from pro_display_program dp 
        JOIN pro_display_program_detail dpd 
        on dp.pro_display_program_id = dpd.pro_display_program_id
        JOIN pro_cus_map m ON dp.customer_id = m.object_id and dp.pro_info_id =  m.pro_info_id 
        join pro_structure s ON s.pro_info_id = m.pro_info_id
        join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id and  sd.product_id = dpd.product_id
        where dp.pro_info_id = proinfoid and m.pro_cus_map_id = procusmapid and m.object_type = 1
        and dp.create_date >= TRUNC((select from_date from pro_period where pro_period_id = periodid))
        and dp.create_date < TRUNC((select to_date from pro_period where pro_period_id = periodid)+ 1)
        and sd.display_quantity <= dpd.quantity and dp.image_display = 1
        AND s.type IN (1, 2) AND sd.type = 1 AND sd.display_quantity IS NOT NULL 
        group by dp.pro_display_program_id
      )
      -- ket qua Dat neu tong so lan cham TB Dat >= so lan cham trung bay quy dinh trong co cau
      select case when (  select count(*)      from spThoaDKTB      where spThoaDKTB.soSPTBDat  = (SELECT count(1) FROM spDKTB)) 
                      >= 
                       (  select QUANLIFIED_QUANTITY from pro_structure where pro_info_id = proinfoid) 
                  then 1 else 0 end ketQua
      FROM dual;
    
    -- Tinh he so
    CURSOR c_KTong 
    IS
      SELECT MIN(t.heSo) heSo
      FROM (
        SELECT s.pro_structure_id
              , sd.pro_structure_detail_id
              , CASE WHEN sd.unit_type = 1 THEN FLOOR(NVL(cp.quantity, 0) / sd.min_limit) 
                     WHEN sd.unit_type = 2 THEN FLOOR(NVL(cp.quantity, 0) * sd.convfact / sd.min_limit) 
                END heSo
              , sd.level_number soSuat
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.min_limit != 0 
          )t;

    -- Loai 2: Tinh thuong chuong trinh HT05 co TONG tung SP chua khai bao
    -- Xet dieu kien so luong mua Dat hay Khong dat
    CURSOR c_TongMuaChuaKBDK
    IS
      WITH
      -- lay danh sach tat ca san pham trong co cau
      tongSoSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 
      )
      -- lay ra danh sach san pham co dieu kien min_limit IS NOT NULL
      ,spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND (
              (cp.quantity >= sd.min_limit AND sd.unit_type = 1)
            OR 
              (cp.quantity*sd.convfact >= sd.min_limit AND sd.unit_type = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL 
      )
      ,dongTong AS (
        SELECT sd.min_limit chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type = 2
          AND sd.type = 2 
      )
      ,spKhongDK AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT CASE WHEN sd.unit_type = 1 THEN cp.quantity
                      WHEN sd.unit_type = 2 THEN cp.quantity * sd.convfact
                 END soLuong
          FROM pro_cus_process cp
          --JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND cp.product_id NOT IN (SELECT t.product_id FROM spDK t)
            AND s.type = 2
            AND sd.type = 1 
            )t
      )
      -- Ket qua Dat, neu cac SP dieu kien thoa dieu kien, cac SP chua khai bao thoa dieu kien Tong hoac khong co SP chua khai bao
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE 
                          ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t))
                          OR
                          ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT count(1) FROM spDK) = (SELECT count(1) FROM tongSoSP))
                        ) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
     -- Xet dieu kien so luong trung bay Dat hay Khong dat
    CURSOR c_TongTBChuaKBDK
    IS
      WITH
      -- lay danh sach tat ca san pham trong co cau
      tongSoSPTB AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 
      )
      -- lay ra danh sach san pham co dieu kien min_limit IS NOT NULL
      ,spDKTB AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.display_quantity IS NOT NULL
      )
      ,spThoaDKTB AS(
        SELECT dp.pro_display_program_id, count(*) soSPTBDat 
        from pro_display_program dp 
        JOIN pro_display_program_detail dpd 
        on dp.pro_display_program_id = dpd.pro_display_program_id
        JOIN pro_cus_map m ON dp.customer_id = m.object_id and dp.pro_info_id =  m.pro_info_id 
        join pro_structure s ON s.pro_info_id = m.pro_info_id
        join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id and  sd.product_id = dpd.product_id
        where dp.pro_info_id = proinfoid and m.pro_cus_map_id = procusmapid and m.object_type = 1
         AND s.type IN (2)
          AND sd.type = 1 AND sd.display_quantity IS NOT NULL
        and dp.create_date >= TRUNC((select from_date from pro_period where pro_period_id = periodid))
        and dp.create_date < TRUNC((select to_date from pro_period where pro_period_id = periodid)+ 1)
        and sd.display_quantity <= dpd.quantity and dp.image_display = 1
        group by dp.pro_display_program_id
      )
      ,dongTongTB AS (
        SELECT sd.display_quantity chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type = 2
          AND sd.type = 2 
      )
      -- tong san pham khong dieu kien trung bay dat
      ,spKhongDKTB AS(
      SELECT  dp.pro_display_program_id, sum(dpd.quantity) tongSLSPTBDat 
      from pro_display_program dp 
      JOIN pro_display_program_detail dpd on dp.pro_display_program_id = dpd.pro_display_program_id
      JOIN pro_cus_map m ON dp.customer_id = m.object_id and dp.pro_info_id =  m.pro_info_id 
      join pro_structure s ON s.pro_info_id = m.pro_info_id
      join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id and  sd.product_id = dpd.product_id
      where dp.pro_info_id = proinfoid and m.pro_cus_map_id = procusmapid and m.object_type = 1
          AND dpd.product_id NOT IN (SELECT t.product_id FROM spDKTB t)
          AND s.type = 2
          AND sd.type = 1 
          and dp.create_date >= TRUNC((select from_date from pro_period where pro_period_id = periodid))
          and dp.create_date < TRUNC((select to_date from pro_period where pro_period_id = periodid)+ 1)   
      group by dp.pro_display_program_id          
      )
      -- trong tung lan cham tb: tb dat khi SP dieu kien thoa dieu kien, cac SP chua khai bao thoa dieu kien Tong hoac khong co SP chua khai bao
      -- hoac khong co sp dieu kien va cac SP chua khai bao thoa dieu kien Tong 
      -- ket qua Dat khi: so lan cham tb dat >= so lan cham trung bay quy dinh 
      select case when (
      select count(*)
      from spThoaDKTB FULL JOIN spKhongDKTB ON spThoaDKTB.pro_display_program_id = spKhongDKTB.pro_display_program_id
      where (soSPTBDat = (SELECT count(1) FROM spDKTB) and tongSLSPTBDat >=(SELECT t.chanDuoi FROM dongTongTB t))
      or (soSPTBDat = (SELECT count(1) FROM spDKTB) and (SELECT count(1) FROM spDKTB) = (SELECT count(1) FROM tongSoSPTB))
      or (0 = (SELECT count(1) FROM spDKTB) and tongSLSPTBDat >=(SELECT t.chanDuoi FROM dongTongTB t))
      )    >= 
                       (  select QUANLIFIED_QUANTITY from pro_structure where pro_info_id = proinfoid) 
                  then 1 else 0 end ketQua
      FROM dual;
   
    -- Tinh he so
    CURSOR c_TongChuaKB
    IS
      WITH
      -- MIN_LIMIT IS NULL
      spChuaKB AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NULL 
      )
      -- MIN_LIMIT IS NOT NULL
      ,spKhaiBao AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL 
      )
      ,heSoSPKB AS(
        SELECT MIN(t.heSo) heSo
        FROM (
          SELECT sd.pro_structure_detail_id
                , CASE WHEN sd.unit_type = 1 THEN FLOOR(NVL(cp.quantity, 0) / sd.min_limit) 
                       WHEN sd.unit_type = 2 THEN FLOOR(NVL(cp.quantity, 0) * sd.convfact / sd.min_limit) 
                  END heSo
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.min_limit != 0
            AND cp.product_id IN (SELECT t.product_id FROM spKhaiBao t)
            )t
      )
      ,heSoSPChuaKB AS(
        SELECT FLOOR(NVL(t.heSo, 0) / t.min_limit) heSo
        FROM(  
          -- Chi co 1 dong
          SELECT sd.min_limit
                , CASE WHEN sd.unit_type = 1 THEN
                        NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM spChuaKB t)), 0)
                        WHEN sd.unit_type = 2 THEN
                        NVL((SELECT SUM(cp.quantity * sd.convfact) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM spChuaKB t)), 0)
                  END heSo
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND s.type IN (2) AND sd.type = 2 AND i.type = 5 AND i.status = 1 AND sd.min_limit != 0 
        )t
      )
      SELECT CASE WHEN (SELECT product_id FROM spChuaKB  WHERE ROWNUM = 1) IS NULL THEN (SELECT heSo FROM heSoSPKB)
                WHEN (SELECT product_id FROM spKhaiBao WHERE ROWNUM = 1) IS NULL THEN (SELECT heSo FROM heSoSPChuaKB)
                ELSE LEAST((SELECT heSo FROM heSoSPKB), (SELECT heSo FROM heSoSPChuaKB))
            END heSo
      FROM dual;

    -- Loai 3: Tinh thuong chuong trinh HT05 co TONG  chung cho tat ca SP
    -- Xet dieu kien so luong mua Dat hay Khong dat
    CURSOR c_TongMuaChungDK 
    IS
      WITH
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL 
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
          AND (
              (cp.quantity >= sd.min_limit AND sd.unit_type = 1)
            OR 
              (cp.quantity*sd.convfact >= sd.min_limit AND sd.unit_type = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL 
      )
      ,dongTong AS (
        SELECT sd.min_limit chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type IN (1)
          AND sd.type = 2 
      )
      ,tatCaSP AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT CASE WHEN sd.unit_type = 1 THEN cp.quantity
                      WHEN sd.unit_type = 2 THEN cp.quantity * sd.convfact
                 END soLuong
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND s.type IN (1)
            AND sd.type = 1 
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
                        AND (SELECT t.soLuong FROM tatCaSP t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
     -- Xet dieu kien so luong trung bay Dat hay Khong dat
    CURSOR c_TongTBChungDK 
    IS
      WITH
      spDKTB AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.display_quantity IS NOT NULL 
      )
      ,spThoaDKTB AS(
           
        SELECT dp.pro_display_program_id, count(*) soSPTBDat 
        from pro_display_program dp 
        JOIN pro_display_program_detail dpd 
        on dp.pro_display_program_id = dpd.pro_display_program_id
        JOIN pro_cus_map m ON dp.customer_id = m.object_id and dp.pro_info_id =  m.pro_info_id 
        join pro_structure s ON s.pro_info_id = m.pro_info_id
        join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id and  sd.product_id = dpd.product_id
        where dp.pro_info_id = proinfoid and m.pro_cus_map_id = procusmapid and m.object_type = 1
         AND s.type IN (1)
          AND sd.type = 1 AND sd.display_quantity IS NOT NULL
        and dp.create_date >= TRUNC((select from_date from pro_period where pro_period_id = periodid))
        and dp.create_date < TRUNC((select to_date from pro_period where pro_period_id = periodid)+ 1)
        and sd.display_quantity <= dpd.quantity and dp.image_display = 1
        group by dp.pro_display_program_id  
      )
      ,dongTongTB AS (
        SELECT sd.display_quantity chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type IN (1)
          AND sd.type = 2 
      )
      ,tatCaSPTB AS(
        SELECT  dp.pro_display_program_id, sum(dpd.quantity) tongSLSPTBDat 
      from pro_display_program dp 
      JOIN pro_display_program_detail dpd on dp.pro_display_program_id = dpd.pro_display_program_id
      JOIN pro_cus_map m ON dp.customer_id = m.object_id and dp.pro_info_id =  m.pro_info_id 
      join pro_structure s ON s.pro_info_id = m.pro_info_id
      join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id and  sd.product_id = dpd.product_id
      where dp.pro_info_id = proinfoid and m.pro_cus_map_id = procusmapid and m.object_type = 1
          AND s.type = 1
          AND sd.type = 1 
          and dp.create_date >= TRUNC((select from_date from pro_period where pro_period_id = periodid))
          and dp.create_date < TRUNC((select to_date from pro_period where pro_period_id = periodid)+ 1)   
      group by dp.pro_display_program_id 
      )
       -- trong tung lan cham tb: tb dat khi: SP dieu kien thoa dieu kien hoac khong co san pham dieu kien, va tat ca SP  khai bao thoa dieu kien Tong 
      -- ket qua Dat khi: so lan cham tb dat >= so lan cham trung bay quy dinh 
      select case when (
      select count(*)
      from spThoaDKTB RIGHT JOIN tatCaSPTB ON spThoaDKTB.pro_display_program_id = tatCaSPTB.pro_display_program_id
      where (soSPTBDat = (SELECT count(1) FROM spDKTB) or (SELECT count(1) FROM spDKTB) = 0 ) and tongSLSPTBDat >=(SELECT t.chanDuoi FROM dongTongTB t)
     
      )    >= 
                       (  select QUANLIFIED_QUANTITY from pro_structure where pro_info_id = proinfoid) 
                  then 1 else 0 end ketQua
      FROM dual;
    
    -- Tinh he so
    CURSOR c_TongChung 
    IS
      WITH
      tongSoSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 
      )
      ,spKhaiBao AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 5 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL 
      )
      ,heSoSPKB AS(
        SELECT MIN(t.heSo) heSo
        FROM (
          SELECT sd.pro_structure_detail_id
                , CASE WHEN sd.unit_type = 1 THEN FLOOR(NVL(cp.quantity, 0) / sd.min_limit) 
                       WHEN sd.unit_type = 2 THEN FLOOR(NVL(cp.quantity, 0) * sd.convfact / sd.min_limit) 
                  END heSo
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.min_limit != 0 
            AND cp.product_id IN (SELECT t.product_id FROM spKhaiBao t)
            )t
      )
      ,heSoSPChuaKB AS(
        SELECT FLOOR(NVL(t.heSo, 0) / t.min_limit) heSo
        FROM(  
          -- Chi co 1 dong
          SELECT sd.min_limit
                , CASE WHEN sd.unit_type = 1 THEN
                        NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM tongSoSP t)), 0)
                        WHEN sd.unit_type = 2 THEN
                        NVL((SELECT SUM(cp.quantity * sd.convfact) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM tongSoSP t)), 0)
                  END heSo
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND s.type IN (1) AND sd.type = 2 AND i.type = 5 AND i.status = 1 AND sd.min_limit != 0 
        )t
      )
      SELECT
        CASE 
            WHEN (SELECT product_id FROM spKhaiBao WHERE ROWNUM = 1) IS NULL THEN (SELECT heSo FROM heSoSPChuaKB)
            ELSE LEAST((SELECT heSo FROM heSoSPKB), (SELECT heSo FROM heSoSPChuaKB))
        END heSo
      FROM dual;

    -- Thong tin ho tro 
    CURSOR c_HoTro 
    IS
      SELECT gd.pro_gift_detail_id, gd.unit_cost donGia, gd.quantity soLuong, pi.object_type loaiHoTro
      FROM pro_structure s
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
      JOIN product_info pi ON pi.product_info_id = gd.type_support
      WHERE s.pro_info_id = proinfoid  AND pi.type = 1
      ;
    -- Lay ra han muc, han muc co the = NULL hoac > 0 (khong co han muc = 0)
    CURSOR c_HanMuc
    IS
      SELECT s.quota giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;

    -- Loai ap dung cho So luong Mua: 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
    CURSOR c_LoaiADMua
    IS
      SELECT CASE WHEN sd.min_limit IS NULL THEN 1
                  WHEN s.type = 2 THEN 2
                  ELSE 3 END loaiAD
      FROM pro_info i
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND i.pro_info_id = proinfoid AND sd.type = 2;
    
     -- Loai ap dung cho So luong Trung bay: 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
    CURSOR c_LoaiADTB
    IS
      SELECT CASE WHEN sd.display_quantity IS NULL THEN 1
                  WHEN s.type = 2 THEN 2
                  ELSE 3 END loaiAD
      FROM pro_info i
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND i.pro_info_id = proinfoid AND sd.type = 2;
  
    -- Danh cho loai cap nhat bang tay: Lay PRO_CUS_REWARD_ID
    CURSOR c_ProCusReward
    IS
      SELECT pro_cus_reward_id
      FROM pro_cus_reward
      WHERE pro_cus_map_id = procusmapid AND pro_period_id = periodid;
    
    loaiApDungMua NUMBER(3,0);
    loaiApDungTB NUMBER(3,0);
    rewardID NUMBER(20,0);
    rewardDetailID NUMBER(20,0);
    soLuong NUMBER(20,0);
    donGia NUMBER(20,0);
    result NUMBER(1,0);
    resulttmp NUMBER(1,0);
    totalAmount NUMBER(20,0);
    isAchieved NUMBER(20, 0) := 0;
    hanMuc NUMBER(20, 0);
    heSoTmp NUMBER(20,0);
    heSo NUMBER(20,0);
    soSuatThuong NUMBER(20,0) := 0;
    datTB NUMBER(1,0) := 0;
    ----------------------------------------------------------------------------------------------------------------------------------------
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT05_CUS_REWARD started');
    OPEN c_HanMuc;
    FETCH c_HanMuc INTO hanMuc;
    CLOSE c_HanMuc;

    totalAmount := 0;
    DBMS_OUTPUT.put_line ('Chay theo tien trinh');
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    
    OPEN c_LoaiADMua;
    FETCH c_LoaiADMua INTO loaiApDungMua;
    CLOSE c_LoaiADMua;
    
    OPEN c_LoaiADTB;
    FETCH c_LoaiADTB INTO loaiApDungTB;
    CLOSE c_LoaiADTB;

    -- SL TB Khong co tong
    IF (loaiApDungTB = 1) THEN
      DBMS_OUTPUT.put_line ('SL TB Loai khong co Tong');
      OPEN c_KTongDKTB;
      FETCH c_KTongDKTB INTO resulttmp;
      IF c_KTongDKTB%notfound THEN resulttmp := 0;
      END IF;
      CLOSE c_KTongDKTB;
    
      IF resulttmp = 1 THEN
       
       -- 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
         
        IF ( loaiApDungMua = 1) THEN
          DBMS_OUTPUT.put_line ('SL mua Loai khong co Tong');
          OPEN c_KTongDKMua;
          FETCH c_KTongDKMua INTO result;
          IF c_KTongDKMua%notfound THEN result := 0;
           DBMS_OUTPUT.put_line ('result := 0;');
          END IF;
          CLOSE c_KTongDKMua;
        ELSIF (loaiApDungMua = 2)THEN
          DBMS_OUTPUT.put_line ('SL mua Loai Tong cho SP chua khai bao');
          OPEN c_TongMuaChuaKBDK;
          FETCH c_TongMuaChuaKBDK INTO result;
          IF c_TongMuaChuaKBDK%notfound THEN result := 0;
          END IF;
          CLOSE c_TongMuaChuaKBDK;
        ELSE
          DBMS_OUTPUT.put_line ('SL mua Loai Tong cho tat ca SP');
          OPEN c_TongMuaChungDK;
          FETCH c_TongMuaChungDK INTO result;
          IF c_TongMuaChungDK%notfound THEN result := 0;
          END IF;
          CLOSE c_TongMuaChungDK; 
        END IF;
      
      END IF;
      
      IF (result = 1)THEN
        OPEN c_KTong;
        FETCH c_KTong INTO heSoTmp;
        CLOSE c_KTong;
         DBMS_OUTPUT.put_line ('He so ' || heSoTmp);
      ELSE 
        heSoTmp := 0;
      END IF; 
    -- Tong ap dung cho SP chua khai bao
    ELSIF (loaiApDungTB = 2) THEN 
      DBMS_OUTPUT.put_line (' SL TB Loai co Tong ap dung cho SP chua khai bao');
      OPEN c_TongTBChuaKBDK ;
      FETCH c_TongTBChuaKBDK INTO resulttmp;
      IF c_TongTBChuaKBDK%notfound THEN resulttmp := 0;
      END IF;
      CLOSE c_TongTBChuaKBDK;
        
      IF resulttmp = 1 THEN
       -- 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
        IF ( loaiApDungMua = 1) THEN
          DBMS_OUTPUT.put_line ('SL mua Loai khong co Tong');
          OPEN c_KTongDKMua;
          FETCH c_KTongDKMua INTO result;
          IF c_KTongDKMua%notfound THEN result := 0;
           DBMS_OUTPUT.put_line ('result := 0;');
          END IF;
          CLOSE c_KTongDKMua;
        ELSIF (loaiApDungMua = 2)THEN
          DBMS_OUTPUT.put_line ('SL mua Loai Tong cho SP chua khai bao');
          OPEN c_TongMuaChuaKBDK;
          FETCH c_TongMuaChuaKBDK INTO result;
          IF c_TongMuaChuaKBDK%notfound THEN result := 0;
          END IF;
          CLOSE c_TongMuaChuaKBDK;
        END IF;
      END IF;
      
      IF (result = 1)THEN
        OPEN c_TongChuaKB;
        FETCH c_TongChuaKB INTO heSoTmp;
        CLOSE c_TongChuaKB;
      ELSE 
        heSoTmp := 0;
      END IF; 
    -- Tong ap dung chung cho tat ca SP
    ELSE
      DBMS_OUTPUT.put_line ('SL TB Loai co Tong ap dung chung cho tat ca SP');
      OPEN c_TongTBChungDK;
      FETCH c_TongTBChungDK INTO resulttmp;
      IF c_TongTBChungDK%notfound THEN resulttmp := 0;
      END IF;
      CLOSE c_TongTBChungDK;
       
      IF resulttmp = 1 THEN
       -- 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
        IF ( loaiApDungMua = 1) THEN
          DBMS_OUTPUT.put_line ('SL mua Loai khong co Tong');
          OPEN c_KTongDKMua;
          FETCH c_KTongDKMua INTO result;
          IF c_KTongDKMua%notfound THEN result := 0;
           DBMS_OUTPUT.put_line ('result := 0;');
          END IF;
          CLOSE c_KTongDKMua;
        ELSIF (loaiApDungMua = 3)THEN
          DBMS_OUTPUT.put_line ('SL mua Loai Tong cho tat ca SP');
          OPEN c_TongMuaChungDK;
          FETCH c_TongMuaChungDK INTO result;
          IF c_TongMuaChungDK%notfound THEN result := 0;
          END IF;
          CLOSE c_TongMuaChungDK; 
        END IF;
      END IF;
   
      IF (result = 1)THEN
        OPEN c_TongChung;
        FETCH c_TongChung INTO heSoTmp;
        CLOSE c_TongChung;
      ELSE 
        heSoTmp := 0;
      END IF;
    END IF;
    datTB := resulttmp;
    -- Suat dat
    IF (result = 1) THEN
      DBMS_OUTPUT.put_line ('Dat thuong');
      isAchieved := 1;
      
      IF ((hanMuc IS NULL) OR (hanMuc >= heSoTmp AND hanMuc IS NOT NULL))THEN
        heSo := heSoTmp;
      ELSIF (hanMuc < heSoTmp AND hanMuc IS NOT NULL) THEN
        heSo := hanMuc;
      END IF;
      
      soSuatThuong := heSo;
      
      FOR v IN c_HoTro 
      LOOP
        totalAmount := totalAmount + v.soLuong*heSo*v.donGia;
        IF (v.loaiHoTro = 4) THEN
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia*heSo, v.soLuong*heSo*v.donGia, sysdate, username);
        ELSE
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong*heSo, v.donGia, v.soLuong*heSo*v.donGia, sysdate, username);
        END IF;
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
      END LOOP;
    ELSE
      DBMS_OUTPUT.put_line (' Khong dat thuong');
    END IF;

    INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, LEVEL_AUTO, CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, RESULT_DISPLAY_PROGRAM, PRO_INFO_ID)
    VALUES(rewardID, procusmapid, totalAmount, soSuatThuong, sysdate, username, periodid, isAchieved, datTB, proinfoid);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    DBMS_OUTPUT.put_line ('soSuatThuong ' || soSuatThuong);
    DBMS_OUTPUT.put_line ('Ket qua ' || isAchieved);
    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT05_CUS_REWARD started');
  END PRO_HT05_CUS_REWARD;
  
  PROCEDURE PRO_HT06_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  -- Tinh thuong HT06 luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  IS
    CURSOR c_DsSPDieuKien
    IS
      select sd.product_id
      from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      where s.pro_info_id = proinfoid and sd.type = 1 and sd.product_id is not NULL
      union
      select p.product_id
      from product p where p.convfact > 1 and p.status = 1 and p.cat_id = 1
      and exists (select 1 from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                  where s.pro_info_id = proinfoid and sd.type = 2);

    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_DieuKien
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Lay danh sach SP trong dieu kien huong ho tro
      -- Neu ton tai sd.type = 2 -> lay tat ca san pham
      ,spDK AS(
        select sd.product_id
        from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        where s.pro_info_id = proinfoid and sd.type = 1 and sd.product_id is not NULL
        union
        select p.product_id
        from product p where p.convfact > 1 and p.status = 1 and p.cat_id = 1
        and exists (select 1 from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                    where s.pro_info_id = proinfoid and sd.type = 2)
      )
      ,dsNgay AS(
        SELECT TO_CHAR(tmp2.thang, 'MM/YYYY') thangStr, tmp2.thang, tmp2.soNgayTrongThang,
              CASE
                  WHEN TRUNC(start_date, 'MM') = TRUNC(end_date, 'MM') THEN TRUNC(end_date) - TRUNC(start_date) + 1
                  WHEN TRUNC(tmp2.thang, 'MM') = TRUNC(end_date, 'MM') THEN EXTRACT(DAY FROM tmp2.thang)
                  ELSE (TRUNC(LAST_DAY(tmp2.thang)) - TRUNC(tmp2.thang) + 1)
              END AS soNgayCanTinh
        FROM
            (SELECT
                    CASE
                        WHEN (TRUNC(start_date, 'MM') = TRUNC(end_date, 'MM')) THEN end_date
                        WHEN (TRUNC(tmp.month) = TRUNC(start_date)) THEN tmp.month
                        WHEN (TRUNC(tmp.month, 'MM') = TRUNC(end_date, 'MM')) THEN end_date
                        ELSE TRUNC(tmp.month, 'MM')
                    END AS thang,
                    TO_NUMBER(TO_CHAR(LAST_DAY(tmp.month), 'DD')) AS soNgayTrongThang,
                    start_date, end_date
            FROM
                (SELECT add_months( start_date, level-1 ) AS month, start_date, end_date
                FROM
                    (select pp.from_date start_date, pp.to_date end_date
                    from pro_period pp where pp.pro_period_id = periodid)
                CONNECT BY level <= months_between(TRUNC(end_date,'MM'), TRUNC(start_date,'MM')) + 1
                ) tmp
            ) tmp2
      )
      ,slKeHoach AS(
        select NVL(SUM(t.khKetThung), -1) khKetThung
        --, SUM(t.khLit) khLit
        from (
          SELECT tmp.product_id, SUM((tmp.khKetThung/tmp.soNgayTrongThang)*tmp.soNgayCanTinh) AS khKetThung, SUM((tmp.khLit/tmp.soNgayTrongThang)*tmp.soNgayCanTinh) AS khLit
          FROM
            (SELECT sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh, SUM(sp.quantity) AS khKetThung, SUM(sp.quantity_lit) AS khLit
            FROM sale_plan sp, dsNgay tmp
            WHERE sp.object_type = 1 AND sp.type = 2
            and sp.object_id = (select cm.object_id from pro_cus_map cm where cm.pro_cus_map_id = procusmapid)
            AND sp.month = extract(MONTH FROM tmp.thang) AND sp.year = extract(YEAR FROM tmp.thang)
            and sp.product_id in (select t.product_id from spDK t)
            GROUP BY sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh
            ) tmp
          WHERE 1=1
            and exists (select 1 from pro_structure s where s.pro_info_id = proinfoid and s.register_type = 1)
          GROUP BY tmp.product_id
          )t
      )
      ,slThucTe AS(
        -- SELECT SUM(rpt.xuat_ban - rpt.nhap_tra_hang) xuatBan, SUM(rpt.nhap_sbc) nhapTra
        select NVL(sum(thucHien), -1) thucHien
        from(
          select case when (select s.type from pro_structure s where s.pro_info_id = proinfoid) = 6 then (rpt.xuat_ban - rpt.nhap_tra_hang)
                      when (select s.type from pro_structure s where s.pro_info_id = proinfoid) = 5 then rpt.nhap_sbc
                else 0
                end thucHien
          FROM rpt_stock_total_day rpt
          WHERE 1=1
            AND rpt.shop_id IN (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid AND object_type = 2)
            AND rpt.rpt_in_date >= (SELECT t.fromDate FROM tg t) AND rpt.rpt_in_date < (SELECT t.toDate FROM tg t) + 1
            AND rpt.product_id in (select t.product_id from spDK t)
          )
      )
      ,mucHoTro AS(
        select NVL(level_number, -1) giaTri
        from (
          select sd.level_number
          FROM pro_cus_process cp
          JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.type = 3 
            and (((s.type = 5 and cp.quantity_return >= sd.min_limit and cp.quantity_return <= sd.max_limit)
                or(s.type = 5 and cp.quantity_return >= sd.min_limit and sd.max_limit is null))
                or
                ((s.type = 6 and cp.quantity >= sd.min_limit and cp.quantity <= sd.max_limit)
                or(s.type = 6 and cp.quantity >= sd.min_limit and sd.max_limit is null)))
            and rownum = 1
            )
      )
      select case when s.register_type = 0 and (select t.giaTri from mucHoTro t) != -1 then 1
            when s.register_type = 1 
              and ((NVL((select t.thucHien from slThucTe t), 0)/NVL((select t.khKetThung from slKeHoach t), 1) >= s.register_quantity)
                  OR ((select t.thucHien from slThucTe t) !=-1 AND (select t.khKetThung from slKeHoach t) = -1))
              and (select t.giaTri from mucHoTro t) != -1 then 1
            when s.register_type = 2 and (select t.thucHien from slThucTe t) >= s.register_quantity and (select t.giaTri from mucHoTro t) != -1 then 1
          else 0 end ketQua
          ,case when (select t.thucHien from slThucTe t) = -1 and (select t.khKetThung from slKeHoach t) = -1 then 0
              when (select t.thucHien from slThucTe t) != -1 and (select t.khKetThung from slKeHoach t) = -1 then 100
          else (select t.thucHien from slThucTe t)/NVL((select t.khKetThung from slKeHoach t), 1) 
          end phanTramHoanThanh
          ,(select t.thucHien from slThucTe t) sanLuongThucHien
      from pro_structure s
      where s.pro_info_id = proinfoid 
      ;
    
    -- Tinh he so
    CURSOR c_MucHoTro
    IS
      select NVL(level_number, -1) giaTri
      from (
        select sd.level_number
        FROM pro_cus_process cp
        JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.type = 3 
          and (((s.type = 5 and cp.quantity_return >= sd.min_limit and cp.quantity_return <= sd.max_limit)
              or(s.type = 5 and cp.quantity_return >= sd.min_limit and sd.max_limit is null))
              or
              ((s.type = 6 and cp.quantity >= sd.min_limit and cp.quantity <= sd.max_limit)
              or(s.type = 6 and cp.quantity >= sd.min_limit and sd.max_limit is null)))
          and rownum = 1
          );


    -- Thong tin ho tro theo suat
    CURSOR c_HoTro(v_level_number IN NUMBER)
    IS
      SELECT gd.pro_gift_detail_id, gd.unit_cost donGia, 0 soLuong
      FROM pro_structure s
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
      JOIN product_info pi ON pi.product_info_id = gd.type_support
      WHERE s.pro_info_id = proinfoid AND pi.type = 1 AND gd.level_number = v_level_number
      ;

    -- Lay ra han muc, han muc co the = NULL hoac > 0 (khong co han muc = 0)
    CURSOR c_HanMuc
    IS
      SELECT s.quota giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;

    CURSOR c_LoaiDangKy
    IS
      SELECT s.register_type giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;
    
    rewardID NUMBER(20,0);
    rewardDetailID NUMBER(20,0);
    soLuong NUMBER(20,0);
    donGia NUMBER(20,0);
    result NUMBER(1,0) := 0;
    totalAmount NUMBER(20,0);
    isAchieved NUMBER(20, 0) := 0;
    phanTramHoanThanh NUMBER(20,2);
    sanLuongThucHien NUMBER(20,0);
    mucHoTro NUMBER(10,0) := 0;
    loaiDangKy NUMBER(1,0) := 0;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT06_CUS_REWARD started');
    OPEN c_MucHoTro;
    FETCH c_MucHoTro INTO mucHoTro;
    CLOSE c_MucHoTro;

    totalAmount := 0;
    DBMS_OUTPUT.put_line ('Chay theo tien trinh');
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    
    -- Neu con han muc
    OPEN c_DieuKien;
    FETCH c_DieuKien INTO result, phanTramHoanThanh, sanLuongThucHien;
    CLOSE c_DieuKien;

    -- Suat dat
    IF (result = 1) THEN
      OPEN c_MucHoTro;
      FETCH c_MucHoTro INTO mucHoTro;
      CLOSE c_MucHoTro;

      DBMS_OUTPUT.put_line ('Dat thuong');
      isAchieved := 1;
      
      FOR v IN c_HoTro(mucHoTro)
      LOOP
        totalAmount := totalAmount + v.soLuong*v.donGia;
        INSERT INTO pro_cus_reward_detail (pro_cus_reward_detail_id, pro_cus_reward_id, pro_gift_detail_id, quantity_auto, unit_cost_auto, amount,                  create_date, create_user)
                          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID,        v.pro_gift_detail_id, v.soLuong,      v.donGia,       v.soLuong*v.donGia,     sysdate,     username);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
      END LOOP;
    ELSE
      DBMS_OUTPUT.put_line ('Khong dat thuong');
    END IF;

    INSERT INTO pro_cus_reward(pro_cus_reward_id, pro_cus_map_id, total_amount, LEVEL_AUTO,   CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
                        VALUES(rewardID,          procusmapid,    totalAmount,  1,            sysdate,      username,   periodid,       isAchieved, proinfoid);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    DBMS_OUTPUT.put_line ('Ket qua ' || isAchieved);

    OPEN c_LoaiDangKy;
    FETCH c_LoaiDangKy INTO loaiDangKy;
    CLOSE c_LoaiDangKy;

    IF(loaiDangKy = 1) THEN
      DBMS_OUTPUT.put_line ('Loai ho tro: % san luong');

      update pro_cus_reward set register_quantity = phanTramHoanThanh where pro_cus_reward_id = rewardID;
      DBMS_OUTPUT.put_line ('updated 1 row into PRO_CUS_REWARD field register_quantity = ' || phanTramHoanThanh);

    ELSIF (loaiDangKy = 2) THEN
      DBMS_OUTPUT.put_line ('Loai ho tro: san luong');
      
      update pro_cus_reward set register_quantity = sanLuongThucHien where pro_cus_reward_id = rewardID;
      DBMS_OUTPUT.put_line ('updated 1 row into PRO_CUS_REWARD field register_quantity = ' || sanLuongThucHien);
               
    END IF;

    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT06_CUS_REWARD finished');
  END PRO_HT06_CUS_REWARD;
  
  PROCEDURE PRO_HT02_N_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  -- Tinh thuong HT02_N luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  IS
    -- Xet dieu kien Dat hay Khong dat cho Customer
    CURSOR c_DieuKien4KhachHang
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Lay danh sach SP trong dieu kien huong ho tro
      -- Neu ton tai sd.type = 2 -> lay tat ca san pham
      ,spDK AS(
        select sd.product_id
        from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        where s.pro_info_id = proinfoid and sd.type = 1 and sd.product_id is not NULL
        union
        select p.product_id
        from product p where p.convfact > 1 and p.status = 1 and p.cat_id = 1
        and exists (select 1 from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                    where s.pro_info_id = proinfoid and sd.type = 2)
      )
      ,xuatBan AS(
        SELECT sod.product_id, SUM(sod.quantity) giaTri
        FROM sale_order so
        JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
        WHERE so.approved = 1 AND type IN (0,1)
            AND so.Approved_Date >= (SELECT t.fromDate FROM tg t) AND so.Approved_Date < (SELECT t.toDate FROM tg t) + 1
            AND so.customer_id in (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid) AND sod.is_free_item = 0
            and sod.product_id in (select t.product_id from spDK t)
        GROUP BY sod.product_id
      )
      ,nhapTraHang AS(
        SELECT sod.product_id, SUM(sod.quantity) giaTri
        FROM sale_order so
        JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
        WHERE so.ORDER_TYPE = 'CM' AND so.approved = 1 AND type = 2
            AND so.CREATE_DATE >= (SELECT t.fromDate FROM tg t) AND so.CREATE_DATE < (SELECT t.toDate FROM tg t) + 1
            AND sod.CREATE_DATE >= (SELECT t.fromDate FROM tg t) AND sod.CREATE_DATE < (SELECT t.toDate FROM tg t) + 1
            AND so.customer_id in (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid) AND sod.is_free_item = 0
            and sod.product_id in (select t.product_id from spDK t)
        GROUP BY sod.product_id
      )
      ,slThucTe AS(
          SELECT sp.product_id
                ,(NVL((SELECT t.giaTri FROM xuatBan t WHERE t.product_id = sp.product_id), 0) 
                    - NVL((SELECT t.giaTri FROM nhapTraHang t WHERE t.product_id = sp.product_id), 0)) thucHien
          FROM spDK sp
      )
      select case when s.register_type = 0 then 1
            when s.register_type = 2 and (select SUM(t.thucHien) from slThucTe t) >= s.register_quantity then 1
          else 0 end ketQua
          , 0 phanTramHoanThanh
          , (select SUM(t.thucHien) from slThucTe t) sanLuongThucHien
          
      from pro_structure s
      where s.pro_info_id = proinfoid 
      ;

    -- Xet dieu kien Dat hay Khong dat cho NPP
    CURSOR c_DieuKien4NPP
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Lay danh sach SP trong dieu kien huong ho tro
      -- Neu ton tai sd.type = 2 -> lay tat ca san pham
      ,spDK AS(
        select sd.product_id
        from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        where s.pro_info_id = proinfoid and sd.type = 1 and sd.product_id is not NULL
        union
        select p.product_id
        from product p where p.convfact > 1 and p.status = 1 and p.cat_id = 1
        and exists (select 1 from pro_structure s join pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
                    where s.pro_info_id = proinfoid and sd.type = 2)
      )
      ,dsNgay AS(
        SELECT TO_CHAR(tmp2.thang, 'MM/YYYY') thangStr, tmp2.thang, tmp2.soNgayTrongThang,
              CASE
                  WHEN TRUNC(start_date, 'MM') = TRUNC(end_date, 'MM') THEN TRUNC(end_date) - TRUNC(start_date) + 1
                  WHEN TRUNC(tmp2.thang, 'MM') = TRUNC(end_date, 'MM') THEN EXTRACT(DAY FROM tmp2.thang)
                  ELSE (TRUNC(LAST_DAY(tmp2.thang)) - TRUNC(tmp2.thang) + 1)
              END AS soNgayCanTinh
        FROM
            (SELECT
                    CASE
                        WHEN (TRUNC(start_date, 'MM') = TRUNC(end_date, 'MM')) THEN end_date
                        WHEN (TRUNC(tmp.month) = TRUNC(start_date)) THEN tmp.month
                        WHEN (TRUNC(tmp.month, 'MM') = TRUNC(end_date, 'MM')) THEN end_date
                        ELSE TRUNC(tmp.month, 'MM')
                    END AS thang,
                    TO_NUMBER(TO_CHAR(LAST_DAY(tmp.month), 'DD')) AS soNgayTrongThang,
                    start_date, end_date
            FROM
                (SELECT add_months( start_date, level-1 ) AS month, start_date, end_date
                FROM
                    (select pp.from_date start_date, pp.to_date end_date
                    from pro_period pp where pp.pro_period_id = periodid)
                CONNECT BY level <= months_between(TRUNC(end_date,'MM'), TRUNC(start_date,'MM')) + 1
                ) tmp
            ) tmp2
      )
      ,slKeHoach AS(
        select NVL(SUM(t.khKetThung), -1) khKetThung
        --, SUM(t.khLit) khLit
        from (
          SELECT tmp.product_id, SUM((tmp.khKetThung/tmp.soNgayTrongThang)*tmp.soNgayCanTinh) AS khKetThung, SUM((tmp.khLit/tmp.soNgayTrongThang)*tmp.soNgayCanTinh) AS khLit
          FROM
            (SELECT sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh, SUM(sp.quantity) AS khKetThung, SUM(sp.quantity_lit) AS khLit
            FROM sale_plan sp, dsNgay tmp
            WHERE sp.object_type = 1 AND sp.type = 2
            and sp.object_id = (select cm.object_id from pro_cus_map cm where cm.pro_cus_map_id = procusmapid)
            AND sp.month = extract(MONTH FROM tmp.thang) AND sp.year = extract(YEAR FROM tmp.thang)
            and sp.product_id in (select t.product_id from spDK t)
            GROUP BY sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh
            ) tmp
          WHERE 1=1
            and exists (select 1 from pro_structure s where s.pro_info_id = proinfoid and s.register_type = 1)
          GROUP BY tmp.product_id
          )t
      )
      ,thucHien AS(
          SELECT SUM(rpt.xuat_ban - rpt.nhap_tra_hang) xuatBan, SUM(rpt.nhap_sbc) nhapTra
          FROM rpt_stock_total_day rpt
          WHERE 1=1
            AND rpt.shop_id IN (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid AND object_type = 2)
            AND rpt.rpt_in_date >= (SELECT t.fromDate FROM tg t) AND rpt.rpt_in_date < (SELECT t.toDate FROM tg t) + 1
            and rpt.product_id in (select t.product_id from spDK t)
      )
      ,slThucTe AS(
          select case when s.type = 5 then (select t.nhapTra from thucHien t)
                        when s.type = 6 then (select t.xuatBan from thucHien t)
                        else 0 end thucHien
          from pro_structure s where s.pro_info_id = proinfoid
      )
      select case when s.register_type = 0 then 1
            when s.register_type = 1 
              and ((NVL((select t.thucHien from slThucTe t), 0)/NVL((select t.khKetThung from slKeHoach t), 1) >= s.register_quantity)
                  OR ((select t.thucHien from slThucTe t) != -1 AND (select t.khKetThung from slKeHoach t) = -1)) then 1
            when s.register_type = 2 and (select t.thucHien from slThucTe t) >= s.register_quantity then 1
          else 0 end ketQua
          ,case when (select t.thucHien from slThucTe t) = -1 and (select t.khKetThung from slKeHoach t) = -1 then 0
              when (select t.thucHien from slThucTe t) != -1 and (select t.khKetThung from slKeHoach t) = -1 then 100
            else (select t.thucHien from slThucTe t)/NVL((select t.khKetThung from slKeHoach t), 1) 
          end phanTramHoanThanh
          ,(select t.thucHien from slThucTe t) sanLuongThucHien
      from pro_structure s
      where s.pro_info_id = proinfoid 
      ;
    
    -- Tinh he so
    CURSOR c_HeSo(v_sosuat IN NUMBER)
    IS
      SELECT MIN(heSo) heSo
      FROM (
      SELECT case when s.type = 5 then FLOOR(NVL(cp.quantity_return, 0) / sd.min_limit)
                  when s.type = 6 then FLOOR(NVL(cp.quantity, 0) / sd.min_limit)
                else 0 end heSo
      FROM pro_cus_process cp
      JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
      WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid 
        AND sd.type = 3 AND sd.min_limit != 0 and sd.min_limit is not null
        and sd.level_number = v_sosuat
        );

    -- Thong tin ho tro theo suat
    CURSOR c_HoTro(v_sosuat IN NUMBER)
    IS
      SELECT gd.pro_gift_detail_id, gd.unit_cost donGia, gd.quantity soLuong, pi.object_type loaiHoTro
      FROM pro_structure s
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
      JOIN product_info pi ON pi.product_info_id = gd.type_support
      WHERE s.pro_info_id = proinfoid AND gd.level_number = v_sosuat AND pi.type = 1
      ;

    -- Lay ra tong so suat
    CURSOR c_Suat
    IS
      SELECT t.soSuat
      FROM (
        SELECT DISTINCT sd.level_number soSuat
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE s.pro_info_id = proinfoid
        )t
      ORDER BY t.soSuat
      ;

    -- Lay ra han muc, han muc co the = NULL hoac > 0 (khong co han muc = 0)
    CURSOR c_HanMuc
    IS
      SELECT s.quota giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;  
    
    CURSOR c_LoaiDT
    IS
      SELECT object_type FROM pro_cus_map WHERE pro_cus_map_id = procusmapid;

    CURSOR c_LoaiDangKy
    IS
      SELECT s.register_type giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;

    rewardID NUMBER(20,0);
    rewardDetailID NUMBER(20,0);
    soLuong NUMBER(20,0);
    donGia NUMBER(20,0);
    result NUMBER(1,0);
    totalAmount NUMBER(20,0);
    hanMuc NUMBER(20, 0);
    heSoTmp NUMBER(20,0);
    isAchieved NUMBER(1, 0) := 0;
    heSo NUMBER(20,0);
    soSuatThuong NUMBER(20,0) := 0;
    loaiDoiTuong NUMBER(3,0);
    phanTramHoanThanh NUMBER(20,2);
    sanLuongThucHien NUMBER(20,0);
    loaiDangKy NUMBER(1,0) := 0;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT02_N_CUS_REWARD started');

    OPEN c_LoaiDT;
    FETCH c_LoaiDT INTO loaiDoiTuong;
    CLOSE c_LoaiDT;

    OPEN c_HanMuc;
    FETCH c_HanMuc INTO hanMuc;
    CLOSE c_HanMuc;

    totalAmount := 0;
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    
    IF(loaiDoiTuong = 1) THEN
      DBMS_OUTPUT.put_line ('Loai khach hang');
      -- customer
      OPEN c_DieuKien4KhachHang;
      FETCH c_DieuKien4KhachHang INTO result, phanTramHoanThanh, sanLuongThucHien;
      IF c_DieuKien4KhachHang%notfound THEN
        result := 0;
        phanTramHoanThanh := 0;
        sanLuongThucHien := 0;
      END IF;
      DBMS_OUTPUT.put_line ('phanTramHoanThanh = ' || phanTramHoanThanh);
      DBMS_OUTPUT.put_line ('sanLuongThucHien = ' || sanLuongThucHien);
      CLOSE c_DieuKien4KhachHang;

    ELSE
      DBMS_OUTPUT.put_line ('Loai NPP');
      -- npp
      OPEN c_DieuKien4NPP;
      FETCH c_DieuKien4NPP INTO result, phanTramHoanThanh, sanLuongThucHien;
      IF c_DieuKien4NPP%notfound THEN 
        result := 0;
        phanTramHoanThanh := 0;
        sanLuongThucHien := 0;
      END IF;
      DBMS_OUTPUT.put_line ('phanTramHoanThanh = ' || phanTramHoanThanh);
      DBMS_OUTPUT.put_line ('sanLuongThucHien = ' || sanLuongThucHien);
      CLOSE c_DieuKien4NPP;

    END IF;

    IF(result = 1) THEN
      FOR s IN c_Suat
      LOOP
        -- Neu con han muc
        IF (hanMuc > 0 OR hanMuc IS NULL) THEN
          OPEN c_HeSo(s.soSuat);
          FETCH c_HeSo INTO heSoTmp;
          CLOSE c_HeSo;

          IF(heSoTmp > 0) THEN
            DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Dat thuong');
            isAchieved := 1;

            IF (hanMuc IS NULL) THEN
              heSo := heSoTmp;
            ELSIF (hanMuc < heSoTmp AND hanMuc IS NOT NULL) THEN
              heSo := hanMuc;
              hanMuc := 0;
            ELSIF (hanMuc >= heSoTmp AND hanMuc IS NOT NULL) THEN
              hanMuc := hanMuc - heSoTmp;
              heSo := heSoTmp;
            END IF;

            soSuatThuong := soSuatThuong + heSo;  
            DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ' he so dat thuong : ' || heSo);
            FOR v IN c_HoTro(s.soSuat)
            LOOP
              totalAmount := totalAmount + v.soLuong*heSo*v.donGia;
              IF (v.loaiHoTro = 4) THEN
                -- Ho tro tien
                INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
                VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia*heSo, v.soLuong*heSo*v.donGia, sysdate, username);
              ELSE
                INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
                VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong*heSo, v.donGia, v.soLuong*heSo*v.donGia, sysdate, username);
              END IF;
              DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
            END LOOP;
          ELSE
            DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Khong dat thuong');
          END IF;

        END IF;
      END LOOP;
    ELSE
      DBMS_OUTPUT.put_line ('Chuong trinh khong dat thuong');
    END IF;

    INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, LEVEL_AUTO, CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
    VALUES(rewardID, procusmapid, totalAmount, soSuatThuong, sysdate, username, periodid, isAchieved, proinfoid);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    DBMS_OUTPUT.put_line ('Ket qua ' || isAchieved);

    OPEN c_LoaiDangKy;
    FETCH c_LoaiDangKy INTO loaiDangKy;
    CLOSE c_LoaiDangKy;

    IF(loaiDangKy = 1) THEN
      DBMS_OUTPUT.put_line ('Loai ho tro: % san luong');

      update pro_cus_reward set register_quantity = phanTramHoanThanh where pro_cus_reward_id = rewardID;
      DBMS_OUTPUT.put_line ('updated 1 row into PRO_CUS_REWARD field register_quantity = ' || phanTramHoanThanh);

    ELSIF (loaiDangKy = 2) THEN
      DBMS_OUTPUT.put_line ('Loai ho tro: san luong');
      
      update pro_cus_reward set register_quantity = sanLuongThucHien where pro_cus_reward_id = rewardID;
      DBMS_OUTPUT.put_line ('updated 1 row into PRO_CUS_REWARD field register_quantity = ' || sanLuongThucHien);
               
    END IF;

    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT02_N_CUS_REWARD finished');

  END PRO_HT02_N_CUS_REWARD;
  
  PROCEDURE PRO_CUS_REWARD (inputDate DATE)
  IS
    CURSOR c_ProCusProcess
    IS
      SELECT DISTINCT cp.pro_cus_map_id, cp.pro_period_id, cp.pro_info_id, pp.to_date
      FROM pro_cus_process cp
      JOIN pro_period pp ON pp.pro_period_id = cp.pro_period_id
      JOIN pro_info pi ON pi.pro_info_id = cp.pro_info_id
      WHERE 1=1
        AND (cp.pro_cus_map_id, cp.pro_period_id) NOT IN (SELECT cr.pro_cus_map_id, cr.pro_period_id FROM pro_cus_reward cr)
      ;

    y_param NUMBER(3,0);
    y_Date DATE;
    loaiCT NUMBER(3,0);
    loaiKH NUMBER(3,0);
  BEGIN
    dbms_output.enable(100000000000000);
    DBMS_OUTPUT.put_line ('PRO_CUS_REWARD started');
    SELECT value INTO y_param FROM ap_param WHERE ap_param_code = 'LAST_DAY_REWARD';

    FOR v IN c_ProCusProcess
    LOOP
      SELECT pi.type INTO loaiCT FROM pro_info pi WHERE pi.pro_info_id = v.pro_info_id;
      BEGIN
        SELECT pic.type INTO loaiKH FROM pro_info_cus pic WHERE pic.pro_info_id = v.pro_info_id AND pic.type = -2;
        EXCEPTION
          WHEN 
            NO_DATA_FOUND
          THEN 
            loaiKH := 1;
      END;
      y_Date := v.to_date + y_param;
      
      IF (inputDate >= TRUNC(y_Date) + 1) THEN
      DBMS_OUTPUT.put_line ('Here');
        IF (loaiCT = 1) THEN
          DBMS_OUTPUT.put_line ('Here');
          PKG_HTBH.PRO_HT01_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 0, 'tien trinh');
        ELSIF (loaiCT = 2) and (loaiKH <> -2) THEN
          PKG_HTBH.PRO_HT02_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 3) THEN
          PKG_HTBH.PRO_HT03_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 4) and (loaiKH <> -2) THEN
          PKG_HTBH.PRO_HT04_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 5) THEN
          PKG_HTBH.PRO_HT05_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 6) THEN
          PKG_HTBH.PRO_HT06_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        ELSIF (loaiCT = 7) THEN
          PKG_HTBH.PRO_HT02_N_CUS_REWARD(v.pro_info_id, v.pro_cus_map_id, v.pro_period_id, 'tien trinh');
        END IF;
      END IF;
    END LOOP;
    DBMS_OUTPUT.put_line ('PRO_CUS_REWARD finished');
  END PRO_CUS_REWARD;
  
  PROCEDURE PRO_HT04_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  IS
    -- Loai 1: Tinh thuong chuong trinh HT04 loai khong co TONG
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_KTongDK (v_soMuc NUMBER, v_loaiSanLuong NUMBER, v_soThang NUMBER)
    IS
      WITH
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 4 AND i.status = 1
          AND s.type IN (1,2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND (
                (cp.quantity >= sd.min_limit and v_loaiSanLuong = 1)
                OR 
                (ROUND(cp.quantity/v_soThang) >= sd.min_limit and v_loaiSanLuong = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
  
    -- Loai 2: Tinh thuong chuong trinh HT04 co TONG tung SP chua khai bao
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChuaKBDK (v_soMuc NUMBER, v_loaiSanLuong NUMBER, v_soThang NUMBER)
    IS
      WITH
      tongSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 4 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.level_number = v_soMuc
      )
      -- MIN_LIMIT IS NOT NULL
      ,spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 4 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND (
              (cp.quantity >= sd.min_limit AND v_loaiSanLuong = 1)
            OR 
              (ROUND(cp.quantity/v_soThang) >= sd.min_limit AND v_loaiSanLuong = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc
      )
      ,dongTong AS (           
        SELECT sd.min_limit chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type = 2
          AND sd.type = 2 
          AND sd.level_number = v_soMuc
      )
      ,spKhongDK AS(
        SELECT  CASE  WHEN v_loaiSanLuong = 1 THEN  SUM(t.soLuong) 
                      WHEN v_loaiSanLuong = 2 THEN  ROUND(SUM(t.soLuong)/v_soThang)
                END soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND cp.product_id NOT IN (SELECT t.product_id FROM spDK t)
            AND sd.type = 1 AND sd.level_number = v_soMuc
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE 
                        ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t)
                        OR
                        (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT count(1) FROM spDK) = (SELECT count(1) FROM tongSP))
                        ) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
  
    -- Loai 3: Tinh thuong chuong trinh HT04 loai co TONG chung cho tat ca SP
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChungDK (v_soMuc NUMBER, v_loaiSanLuong NUMBER, v_soThang NUMBER)
    IS
      WITH
      -- MIN_LIMIT IS NOT NULL
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 4 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
           AND (
              (cp.quantity >= sd.min_limit AND v_loaiSanLuong = 1)
            OR 
              (ROUND(cp.quantity/v_soThang) >= sd.min_limit AND v_loaiSanLuong = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc
      )
      ,dongTong AS (            
        SELECT sd.min_limit chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type IN (1)
          AND sd.type = 2 
          AND sd.level_number = v_soMuc
      )
      -- Do trong bang PRO_CUS_PROCESS chi co 1 muc nen khong can xet muc o day
      ,tatCaSP AS(           
        SELECT  CASE  WHEN v_loaiSanLuong = 1 THEN  SUM(t.soLuong)
                      WHEN v_loaiSanLuong = 2 THEN  ROUND(SUM(t.soLuong)/v_soThang)
                END soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND s.type IN (1)
            AND sd.type = 1 
            AND sd.level_number = v_soMuc
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
                        AND (SELECT t.soLuong FROM tatCaSP t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
  
     -- Thong tin ho tro 
    CURSOR c_HoTro (v_soMuc NUMBER)
    IS
      SELECT gd.pro_gift_detail_id, gd.unit_cost donGia, gd.quantity soLuong, pi.object_type loaiHoTro
      FROM pro_structure s
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
      JOIN product_info pi ON pi.product_info_id = gd.type_support
      WHERE s.pro_info_id = proinfoid  AND pi.type = 1 AND gd.level_number = v_soMuc
      ;
    -- Loai ap dung: 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
    CURSOR c_LoaiAD (v_soMuc NUMBER)
    IS
      SELECT CASE WHEN sd.min_limit IS NULL AND sd.max_limit IS NULL THEN 1
                  WHEN s.type = 2 THEN 2
              ELSE 3 END loaiAD
      FROM pro_info i
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND i.pro_info_id = proinfoid AND sd.level_number = v_soMuc AND sd.type = 2;
    
    -- So muc do nguoi dung dang ky
    CURSOR c_SoMuc
    IS
      SELECT cm.level_number soMuc FROM pro_cus_map cm WHERE cm.pro_cus_map_id = procusmapid;
      
    -- Loai san luong: 1: san luong tong; 2: san luong binh quan; null: khong co tieu chi san luong
    CURSOR c_LoaiSanLuong
    IS
      SELECT pt.quantity_type loaiSanLuong FROM pro_structure pt WHERE pt.pro_info_id = proinfoid;

     -- So thang trong thoi gian dien ra chuong trinh (dung de tinh san luong binh quan)
    CURSOR c_SoThang
    IS
         SELECT extract(month from pr.to_date) - extract(month from pr.from_date)+ 1 soThang
      FROM pro_period pr WHERE pr.pro_period_id = periodid;

    loaiApDung NUMBER(3,0);
    rewardID NUMBER(20,0);
    result NUMBER(1,0);
    totalAmount NUMBER(20,0);
    soMuc NUMBER(20,0);
    
    loaiSanLuong NUMBER(3,0);
    soThang NUMBER(20,0);
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT04_CUS_REWARD started');
    OPEN c_SoMuc;
    FETCH c_SoMuc INTO soMuc;
    CLOSE c_SoMuc;

    OPEN c_LoaiAD (soMuc);
    FETCH c_LoaiAD INTO loaiApDung;
    CLOSE c_LoaiAD;
  
    OPEN c_LoaiSanLuong;
    FETCH c_LoaiSanLuong INTO loaiSanLuong;
    CLOSE c_LoaiSanLuong;
    
     
    OPEN c_SoThang;
    FETCH c_SoThang INTO soThang;
    CLOSE c_SoThang;
    
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    totalAmount := 0;
    
    IF(loaiSanLuong is not null) THEN
    -- Khong co tong
      IF (loaiApDung = 1) THEN
        DBMS_OUTPUT.put_line ('Loai khong co Tong');
        OPEN c_KTongDK (soMuc, loaiSanLuong, soThang);
        FETCH c_KTongDK INTO result;
        IF c_KTongDK%notfound THEN result := 0;
        END IF;
        CLOSE c_KTongDK;
      ELSIF (loaiApDung = 2) THEN
        DBMS_OUTPUT.put_line ('Loai co Tong ap dung cho SP chua khai bao');
        OPEN c_TongChuaKBDK (soMuc, loaiSanLuong, soThang);
        FETCH c_TongChuaKBDK INTO result;
        IF c_TongChuaKBDK%notfound THEN result := 0;
        END IF;
        CLOSE c_TongChuaKBDK;
      ELSE
        DBMS_OUTPUT.put_line ('Loai co Tong ap dung chung cho tat ca SP');
        OPEN c_TongChungDK (soMuc, loaiSanLuong, soThang);
        FETCH c_TongChungDK INTO result;
        IF c_TongChungDK%notfound THEN result := 0;
        END IF;
        CLOSE c_TongChungDK;
      END IF;
    ELSE
      result := 1;
    END IF;
    
    IF (result = 1) THEN
      DBMS_OUTPUT.put_line ('Dat thuong');      
      FOR v IN c_HoTro (soMuc)
      LOOP
        totalAmount := totalAmount + v.soLuong*v.donGia;
        IF (v.loaiHoTro = 4) THEN
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
        ELSE
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
        END IF;
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
      END LOOP;
    ELSE
      DBMS_OUTPUT.put_line (' Khong dat thuong');
    END IF;
    
    INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, CREATE_DATE, CREATE_user, PRO_PERIOD_ID, RESULT_AUTO)
    VALUES(rewardID, procusmapid, totalAmount, sysdate, username, periodid, result);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT04_CUS_REWARD finished');
  END PRO_HT04_CUS_REWARD;

END PKG_HTBH;