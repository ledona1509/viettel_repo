PROCEDURE P_DS_2_2(res OUT SYS_REFCURSOR , shopId CLOB, fromDate date, toDate date) AS
  -- @author: NaLD
  -- Bao cao ban hang cua NVBH theo san pham
  withStm CLOB;
  g_StrSQL CLOB;
  g_StrSQL2 CLOB;
  g_FromDateStr VARCHAR(50 byte);
  g_ToDateStr VARCHAR(50 byte);
  CURSOR c_LstProduct
    is
      SELECT t.product_id
      FROM(
          SELECT p.product_id
          FROM product p
          JOIN product_info cat ON cat.product_info_id = p.cat_id
          WHERE 1=1
            AND cat.object_type = 0
            AND p.convfact > 1
            AND p.status = 1
          UNION
          SELECT sp.product_id
          FROM sale_plan sp
          WHERE sp.create_date >= TRUNC(fromDate) AND sp.create_date < TRUNC(toDate) + 1
            AND sp.object_type = 2
            AND sp.object_id IN(SELECT st.staff_id FROM staff st
                                JOIN channel_type ct ON ct.channel_type_id = st.staff_type_id
                                WHERE ct.type = 2 AND ct.object_type = 1
                                  AND st.shop_id IN (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(shopId)))
                                )
            AND sp.product_id IN(SELECT p.product_id
                                 FROM product p
                                 JOIN product_info cat ON cat.product_info_id = p.cat_id
                                 WHERE cat.type = 1 AND cat.object_type = 0
                                   AND p.convfact > 1 AND p.status = 0
                                 )
          UNION
          SELECT rpt.product_id
          FROM rpt_sale_statistic rpt
          WHERE rpt.rpt_in_date >= TRUNC(fromDate) AND rpt.rpt_in_date < TRUNC(toDate) + 1
            AND rpt.shop_id IN (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(shopId)))
            AND rpt.product_id IN (SELECT p.product_id
                                  FROM product p
                                  JOIN product_info cat ON cat.product_info_id = p.cat_id
                                  WHERE cat.type = 1 AND cat.object_type = 0
                                    AND p.convfact > 1 AND p.status = 0
                                  )
      )t
      JOIN product p ON p.product_id = t.product_id
--      ORDER BY NLSSORT(p.product_name,'NLS_SORT=vietnamese')
      ORDER BY p.sort_order
      ;
  BEGIN
    dbms_output.enable(100000000000000);
    g_FromDateStr := to_char(fromDate,'DD/MM/YYYY');
    g_ToDateStr := to_char(toDate,'DD/MM/YYYY');
    g_StrSQL := '';
    withStm := withStm || ' with dsNgay as( ';
    withStm := withStm || ' SELECT tmp2.thang, ';
    withStm := withStm || ' tmp2.soNgayTrongThang, ';
    withStm := withStm || ' CASE WHEN TRUNC(TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY''), ''MM'') = TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY''), ''MM'') ';
    withStm := withStm || ' THEN TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY'')) - TRUNC(TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY'')) + 1 ';
    withStm := withStm || ' WHEN TRUNC(tmp2.thang, ''MM'') = TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY''), ''MM'') ';
    withStm := withStm || ' THEN EXTRACT(DAY FROM tmp2.thang) ';
    withStm := withStm || ' ELSE (TRUNC(LAST_DAY(tmp2.thang)) - TRUNC(tmp2.thang) + 1) ';
    withStm := withStm || ' END AS soNgayCanTinh ';
    withStm := withStm || ' FROM ';
    withStm := withStm || ' (SELECT CASE WHEN (TRUNC(TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY''), ''MM'') = TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY''), ''MM'')) ';
    withStm := withStm || ' THEN TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY'') ';
    withStm := withStm || ' WHEN (TRUNC(tmp.month) = TRUNC(TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY''))) ';
    withStm := withStm || ' THEN tmp.month ';
    withStm := withStm || ' WHEN (TRUNC(tmp.month, ''MM'') = TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY''), ''MM'')) ';
    withStm := withStm || ' THEN TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY'') ';
    withStm := withStm || ' ELSE TRUNC(tmp.month, ''MM'') ';
    withStm := withStm || ' END AS thang, ';
    withStm := withStm || ' TO_NUMBER(TO_CHAR(LAST_DAY(tmp.month), ''DD'')) AS soNgayTrongThang ';
    withStm := withStm || ' FROM ';
    withStm := withStm || ' (SELECT add_months( start_date, level-1 ) AS month ';
    withStm := withStm || ' FROM (SELECT TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY'') start_date, TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY'') end_date ';
    withStm := withStm || ' FROM dual) ';
    withStm := withStm || ' CONNECT BY level <= months_between(TRUNC(end_date,''MM''), TRUNC(start_date,''MM'')) + 1 ';
    withStm := withStm || ' ) tmp ';
    withStm := withStm || ' ) tmp2 ';
    withStm := withStm || ' ) ';
    withStm := withStm || ' ,TongKeHoach as( ';
    withStm := withStm || ' select tmp.object_id as staff_id, sum(round((tmp.tongKHThang/tmp.soNgayTrongThang)*tmp.soNgayCanTinh)) as tongKeHoach ';
    withStm := withStm || ' from ';
    withStm := withStm || ' (select sp.object_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh, sum(NVL(sp.quantity, 0)) as tongKHThang ';
    withStm := withStm || ' from sale_plan sp, dsNgay tmp ';
    withStm := withStm || ' where sp.object_type = 2 ';
    withStm := withStm || ' and sp.type = 2 ';
    withStm := withStm || ' and sp.month = extract(month from tmp.thang) ';
    withStm := withStm || ' and sp.year = extract(year from tmp.thang) ';
    withStm := withStm || ' group by sp.object_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh ';
    withStm := withStm || ' ) tmp ';
    withStm := withStm || ' group by tmp.object_id ';
    withStm := withStm || ' ) ';
    withStm := withStm || ' , ';
    withStm := withStm || ' TongThucHien as( ';
    withStm := withStm || ' SELECT rpt.staff_id, SUM(NVL(rpt.day_quantity, 0)) tongThucHien ';
    withStm := withStm || ' FROM rpt_sale_statistic rpt ';
    withStm := withStm || ' WHERE 1=1 ';
    withStm := withStm || ' AND rpt.rpt_in_date >= TRUNC(TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY'')) ';
    withStm := withStm || ' AND rpt.rpt_in_date < TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY'')) + 1 ';
    withStm := withStm || ' GROUP BY rpt.staff_id ';
    withStm := withStm || ' ) ';
    withStm := withStm || ' , TongTienDo as( ';
    withStm := withStm || ' select case when t1.staff_id is null then t2.staff_id else t1.staff_id end staff_id, ';
    -- withStm := withStm || ' case when (t1.tongKeHoach is null and t2.tongThucHien is not null) ';
    withStm := withStm || ' case when (t1.tongKeHoach = 0 and t2.tongThucHien != 0) ';
    withStm := withStm || ' then 100 ';
    -- withStm := withStm || ' when (t1.tongKeHoach is not null and t2.tongThucHien is null) ';
    withStm := withStm || ' when (t1.tongKeHoach != 0 and t2.tongThucHien = 0) ';
    withStm := withStm || ' then 0 ';
    withStm := withStm || ' else ';
    withStm := withStm || ' round(t2.tongThucHien / t1.tongKeHoach * 100, 1) ';
    withStm := withStm || ' end as tongTienDo ';
    withStm := withStm || ' from TongKeHoach t1 ';
    withStm := withStm || ' full join TongThucHien t2 on t1.staff_id = t2.staff_id ';
    withStm := withStm || ' ) ';
    withStm := withStm || ' , ';
    withStm := withStm || ' KeHoachSP as( ';
    withStm := withStm || ' select tmp.object_id as staff_id, tmp.product_id, sum(round((tmp.tongKHSP/tmp.soNgayTrongThang)*tmp.soNgayCanTinh)) as tongKHSP ';
    withStm := withStm || ' from ';
    withStm := withStm || ' (select sp.object_id, sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh, sum(NVL(sp.quantity, 0)) as tongKHSP ';
    withStm := withStm || ' from sale_plan sp, dsNgay tmp ';
    withStm := withStm || ' where sp.object_type = 2 ';
    withStm := withStm || ' and sp.type = 2 ';
    withStm := withStm || ' and sp.month = extract(month from tmp.thang) ';
    withStm := withStm || ' and sp.year = extract(year from tmp.thang) ';
    withStm := withStm || ' group by sp.object_id, sp.product_id, tmp.thang, tmp.soNgayTrongThang, tmp.soNgayCanTinh ';
    withStm := withStm || ' ) tmp ';
    withStm := withStm || ' group by tmp.object_id, tmp.product_id ';
    withStm := withStm || ' ) ';
    withStm := withStm || ' , ThucHienSP as( ';
    withStm := withStm || ' SELECT rpt.staff_id, rpt.product_id, SUM(NVL(rpt.day_quantity, 0)) tongTHSP ';
    withStm := withStm || ' FROM rpt_sale_statistic rpt ';
    withStm := withStm || ' WHERE 1=1 ';
    withStm := withStm || ' AND rpt.rpt_in_date >= TRUNC(TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY'')) ';
    withStm := withStm || ' AND rpt.rpt_in_date < TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY'')) + 1 ';
    withStm := withStm || ' GROUP BY rpt.staff_id, rpt.product_id ';
    withStm := withStm || ' ) ';
    withStm := withStm || ' , T1 as( ';
    withStm := withStm || ' select kv.sort_order kvSort,tinh.sort_order tinhSort,npp.sort_order nppSort, nvbh.staff_id, ';
    withStm := withStm || ' nvbh.staff_code as maNVBH, ';
    withStm := withStm || ' nvbh.staff_code || '' - '' || nvbh.staff_name as tenNVBH, ';
    withStm := withStm || ' npp.shop_code as maNPP, ';
    withStm := withStm || ' npp.shop_code || '' - '' || npp.short_name as tenNPP, ';
    withStm := withStm || ' tinh.shop_code as maTinh, ';
    withStm := withStm || ' tinh.shop_code || '' - '' || tinh.short_name as tenTinh, ';
    withStm := withStm || ' kv.shop_code as maKV, ';
    withStm := withStm || ' kv.shop_code || '' - '' || kv.short_name as tenKV, ';
    withStm := withStm || ' ( ';
    withStm := withStm || ' select t.TongKeHoach ';
    withStm := withStm || ' from TongKeHoach t ';
    withStm := withStm || ' where t.staff_id = nvbh.staff_id ';
    withStm := withStm || ' ) TongKeHoach, ';
    withStm := withStm || ' ( ';
    withStm := withStm || ' select t.TongThucHien ';
    withStm := withStm || ' from TongThucHien t ';
    withStm := withStm || ' where t.staff_id = nvbh.staff_id ';
    withStm := withStm || ' ) TongThucHien, ';
    withStm := withStm || ' ( ';
    withStm := withStm || ' select t.TongTienDo ';
    withStm := withStm || ' from TongTienDo t ';
    withStm := withStm || ' where t.staff_id = nvbh.staff_id ';
    withStm := withStm || ' ) TongTienDo ';
    withStm := withStm || ' from staff nvbh ';
    withStm := withStm || ' join shop npp on npp.shop_id = nvbh.shop_id ';
    withStm := withStm || ' join shop tinh on tinh.shop_id = npp.parent_shop_id ';
    withStm := withStm || ' join shop kv on kv.shop_id = tinh.parent_shop_id ';
    withStm := withStm || ' where nvbh.STAFF_ID in ( ';
    withStm := withStm || ' SELECT nvbh.STAFF_ID ';
    withStm := withStm || ' FROM STAFF nvbh ';
    withStm := withStm || ' join channel_type ct on nvbh.STAFF_TYPE_ID = ct.channel_type_id ';
    withStm := withStm || ' WHERE nvbh.SHOP_ID IN (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(''' || shopId ||''')))';
    withStm := withStm || ' and ct.type =2 ';
    withStm := withStm || ' and ct.object_type IN (1) ';
    withStm := withStm || ' ) ';
    withStm := withStm || ' ) ';
    DBMS_OUTPUT.PUT_LINE(withStm);
    for x in c_LstProduct
    loop
      g_StrSQL := g_StrSQL || ' , SanPham_'|| x.product_id ||' as( ';
      g_StrSQL := g_StrSQL || ' select case when t1.staff_id is null then t2.staff_id else t1.staff_id end staff_id ';
      g_StrSQL := g_StrSQL || ' ,case when t1.product_id is null then t2.product_id else t1.product_id end product_id ';
      g_StrSQL := g_StrSQL || ' ,t1.tongKHSP ';
      g_StrSQL := g_StrSQL || ' ,t2.tongTHSP ';
      g_StrSQL := g_StrSQL || ' from ThucHienSP t2 ';
      g_StrSQL := g_StrSQL || ' full join KeHoachSP t1 on t1.staff_id = t2.staff_id and t1.product_id = t2.product_id ';
      g_StrSQL := g_StrSQL || ' where t1.product_id = '|| x.product_id ||' or t2.product_id = '|| x.product_id ||' ';
      g_StrSQL := g_StrSQL || ' ) ';
    end loop;
    g_StrSQL := g_StrSQL || ' , T2 as( ';
    g_StrSQL := g_StrSQL || ' select t.kvSort,t.maKV,t.tenKV,t.tinhSort,t.maTinh,t.tenTinh,t.nppSort,t.maNPP,t.tenNPP,t.maNVBH,t.tenNVBH,t.TongKeHoach,t.TongThucHien,t.TongTienDo ';
   for x in c_LstProduct
   loop
    g_StrSQL := g_StrSQL || ' ,SanPham_'|| x.product_id ||'.tongKHSP as keHoach_'|| x.product_id ||' ';
    g_StrSQL := g_StrSQL || ' ,SanPham_'|| x.product_id ||'.tongTHSP as thucHien_'|| x.product_id ||' ';
   end loop;

   g_StrSQL := g_StrSQL ||' from T1 t ';
   for x in c_LstProduct
   loop
     g_StrSQL := g_StrSQL ||' left join SanPham_'|| x.product_id ||' on t.staff_id = SanPham_'|| x.product_id ||'.staff_id ';
   end loop;
--    g_StrSQL := g_StrSQL || ' order by t.maKV ,t.maTinh ,t.maNPP ,t.maNVBH ';
    g_StrSQL := g_StrSQL || ' ) ';
    g_StrSQL := g_StrSQL || ' , T3 as( ';
    g_StrSQL := g_StrSQL || ' select temp.kvSort,temp.maKV,temp.tenKV,temp.tinhSort,temp.maTinh,temp.tenTinh,temp.nppSort,temp.maNPP,temp.tenNPP,temp.maNVBH,temp.tenNVBH ';
    g_StrSQL := g_StrSQL || ' ,sum(temp.TongKeHoach) as TongKeHoach ';
    g_StrSQL := g_StrSQL || ' ,sum(temp.TongThucHien) as TongThucHien ';
    g_StrSQL := g_StrSQL || ' ,case when (sum(temp.TongKeHoach) is null and sum(temp.TongThucHien) is not null) ';
    g_StrSQL := g_StrSQL || ' then 100 ';
    g_StrSQL := g_StrSQL || ' when (sum(temp.TongKeHoach) is not null and sum(temp.TongThucHien) is null) ';
    g_StrSQL := g_StrSQL || ' then 0 ';
    g_StrSQL := g_StrSQL || ' WHEN (SUM(temp.TongKeHoach) = 0 AND SUM(temp.TongThucHien) != 0) THEN 100 ';
    g_StrSQL := g_StrSQL || ' WHEN (SUM(temp.TongKeHoach) = 0 AND SUM(temp.TongThucHien) = 0) THEN 0 ';
    g_StrSQL := g_StrSQL || ' else ';
    g_StrSQL := g_StrSQL || ' round(sum(temp.TongThucHien) / sum(temp.TongKeHoach) * 100, 1) ';
    g_StrSQL := g_StrSQL || ' end TongTienDo ';
    
   for x in c_LstProduct
   loop
     g_StrSQL := g_StrSQL ||' ,sum(temp.keHoach_'|| x.product_id ||') ';
     g_StrSQL := g_StrSQL ||' ,sum(temp.thucHien_'|| x.product_id ||') ';
   end loop;
 
    g_StrSQL := g_StrSQL || ' from T2 temp ';
    g_StrSQL := g_StrSQL || ' GROUP BY rollup((temp.kvSort, temp.maKV, temp.tenKV), (temp.tinhSort,temp.maTinh, temp.tenTinh), (temp.nppSort,temp.maNPP, temp.tenNPP), (temp.maNVBH, temp.tenNVBH)) ';
    g_StrSQL := g_StrSQL || ' ) ';
    g_StrSQL := g_StrSQL || ' , T4 as( ';
    g_StrSQL := g_StrSQL || ' SELECT T2.* FROM T2 ';
    g_StrSQL := g_StrSQL || ' UNION ';
    g_StrSQL := g_StrSQL || ' SELECT T3.* FROM T3 ';
    g_StrSQL := g_StrSQL || ' WHERE T3.maNVBH is null and T3.tenNVBH IS NULL ';
    g_StrSQL := g_StrSQL || ' ) ';
    g_StrSQL := g_StrSQL || ' select case when temp.maKV is null then to_char(''Tổng'') else to_char(temp.maKV) end maKV ';
    g_StrSQL := g_StrSQL || ' ,temp.tenKV ,temp.maTinh,temp.tenTinh,temp.maNPP,temp.tenNPP,temp.maNVBH,temp.tenNVBH,temp.TongKeHoach,temp.TongThucHien ,temp.TongTienDo ';    
    
    for x in c_LstProduct
    loop
       g_StrSQL := g_StrSQL ||',temp.keHoach_'|| x.product_id || ',temp.thucHien_'|| x.product_id;
    end loop;
    
    g_StrSQL := g_StrSQL || ' from T4 temp ';
    g_StrSQL := g_StrSQL || ' order by temp.kvSort,temp.maKV ,temp.tinhSort,temp.maTinh ,temp.nppSort,temp.maNPP ,temp.maNVBH ';
    DBMS_OUTPUT.PUT_LINE(g_StrSQL);
    
    g_StrSQL := withStm || g_StrSQL;
    OPEN res FOR (g_StrSQL)
    ;
  END P_DS_2_2;
  
  PROCEDURE P_DS_2_3(res OUT SYS_REFCURSOR , shopId CLOB, customerTypeId number, fromDate DATE, toDate DATE)
  AS
   cursor c_product is
    WITH
    dsSPTmp AS(
      SELECT sod.product_id
      FROM sale_order so
      JOIN sale_order_detail sod ON sod.sale_order_id = so.sale_order_id
      WHERE sod.is_free_item = 0 AND so.type = 1
       AND( (so.approved = 1 and so.order_date < trunc(F_GET_DATE_NOT_DEL_ORDER(sysdate))) or(so.approved in(0, 1) and so.order_date >= trunc(F_GET_DATE_NOT_DEL_ORDER(sysdate))))
        AND so.order_date >= TRUNC(fromDate) AND so.order_date < TRUNC(toDate) + 1
        AND sod.order_date >= TRUNC(fromDate) AND sod.order_date < TRUNC(toDate) + 1
        AND so.shop_id IN (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(shopId)))
      )
    SELECT p.product_id
    FROM product p
    JOIN product_info cat ON cat.product_info_id = p.cat_id
    WHERE cat.type= 1 AND cat.object_type = 0 AND p.convfact > 1 
      AND (p.status = 1 OR (p.status = 0 AND EXISTS(SELECT 1 FROM dsSPTmp WHERE dsSPTmp.product_id=p.product_id)))
--        AND t.product_id = 10087
    ORDER BY p.sort_order
    ;
  stm CLOB;
  fromDateStr varchar2(20);
  toDateStr varchar2(20);
  approvedDateStr varchar2(20);
  BEGIN
    approvedDateStr := to_char(F_GET_DATE_NOT_DEL_ORDER(sysdate),'dd/mm/yyyy');
    dbms_output.enable(100000000000000);
    fromDateStr := to_char(fromDate,'dd/mm/yyyy');
    toDateStr := to_char(toDate,'dd/mm/yyyy');
    stm:= 'select makhuvuc, khuvuc, machinhanh, chinhanh, manpp, npp, staff_Code, staff_Name, short_code, customer_name, address,Tong ';
     for v_product in c_product
    loop
      stm:= stm || ', p_' || v_product.product_id;
    end loop;
    stm:= stm || ' from(';
    stm:= stm || '     select kv.sort_order sortKV,kv.shop_code makhuvuc,kv.short_name khuvuc,cn.sort_order sortCN,cn.shop_code machinhanh,cn.short_name chinhanh,sh.sort_order sortNPP,sh.shop_code manpp,sh.short_name npp,';
    stm:= stm || '       st.staff_Code, st.staff_Name,';
    stm:= stm || '       cu.short_code, cu.customer_name, cu.address, sum(total) Tong';
    for v_product in c_product
    loop
      stm:= stm || ', sum(p_' || v_product.product_id ||') p_' || v_product.product_id;
    end loop;
    stm:= stm || '       ,grouping_id(kv.sort_order, kv.shop_code, kv.short_name, cn.sort_order, cn.shop_code, cn.short_name, sh.sort_order, sh.shop_code, sh.short_name, st.staff_Code, st.staff_Name, cu.short_code, cu.customer_name, cu.address) gId';
    stm:= stm || '     from(';
    stm:= stm || '     SELECT so.customer_id,';
    stm:= stm || '     sum(nvl(so.total_quantity,0)) total';
    for v_product in c_product
    loop
      stm:= stm || ',sum((select sum(nvl(sod.quantity,0)) from sale_order_detail sod where sod.is_free_item = 0 and sod.sale_order_id = so.sale_order_id and sod.product_id = ' || v_product.product_id ||')) p_'|| v_product.product_id;
    end loop;
    
    stm:= stm || '     FROM sale_order so ';
    stm:= stm || '    WHERE ((so.approved = 1 and so.order_date < to_date(''' || approvedDateStr ||''', ''dd/mm/yyyy'')) or (so.approved in(0,1) and so.order_date >= to_date(''' || approvedDateStr ||''', ''dd/mm/yyyy'')) ) ';
    stm:= stm || '     AND so.type            = 1 ';
    stm:= stm || '    and so.order_date >= to_date(''' || fromDateStr ||''', ''dd/mm/yyyy'')';
    stm:= stm || '    and so.order_date < to_date(''' || toDateStr ||''', ''dd/mm/yyyy'') + 1';
    stm:= stm || '    and so.shop_id in (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(''' || shopId ||''')))';
    
    stm:= stm || '     group by so.customer_id';
    stm:= stm || '     ) cuTmp ';
    stm:= stm || '     join customer cu on cu.customer_id = cuTmp.customer_id';
    stm:= stm || '     join shop sh on cu.shop_id = sh.shop_id';
    stm:= stm || '     JOIN shop cn ON sh.parent_shop_id = cn.shop_id';
    stm:= stm || '     JOIN shop kv ON cn.parent_shop_id = kv.shop_id';
    stm:= stm || '     left join (select staff_id, customer_id from (';
    stm:= stm || '         select vp.staff_id, rc.customer_id, vp.from_date, r.create_date, RANK() OVER (partition by  rc.customer_id ORDER BY  vp.from_date desc, vp.staff_id desc) rnk';
    stm:= stm || '         from visit_plan vp join routing r on r.routing_id = vp.routing_id and r.type = 0';
    stm:= stm || '         join routing_customer rc on rc.routing_id = r.routing_id';
    stm:= stm || '         where 1 = 1';
    stm:= stm || '         AND(';
    stm:= stm || '         (vp.to_date is null and vp.from_date <= to_date(''' || toDateStr ||''', ''dd/mm/yyyy''))';
    stm:= stm || '         or(vp.to_date  IS not NULL and  vp.from_date <= to_date(''' || toDateStr ||''', ''dd/mm/yyyy'') and vp.to_date >= to_date(''' || fromDateStr ||''', ''dd/mm/yyyy''))';
    stm:= stm || '         )';
    stm:= stm || '         group by vp.staff_id, rc.customer_id, vp.from_date, r.create_date)';
    stm:= stm || '         where rnk = 1) stCu on stCu.customer_id = cu.customer_id';
    stm:= stm || '     left join staff st on st.staff_id = stCu.staff_id';
    stm:= stm || '     left join channel_type ct on ct.channel_type_id = cu.CHANNEL_TYPE_ID and ct.type = 3';
    stm:= stm || '  where 1 = 1 and sh.SHOP_TYPE_ID in(select CHANNEL_TYPE_ID from CHANNEL_TYPE ct where ct.type = 1 and ct.CHANNEL_TYPE_CODE = ''NPP'')';
    IF customerTypeId is not null THEN
      stm:= stm || ' and ct.channel_type_id = ' || customerTypeId;
    END IF;
    stm:= stm || '     GROUP BY rollup((kv.sort_order, kv.shop_code,kv.short_name), (cn.sort_order, cn.shop_code, cn.short_name), (sh.sort_order, sh.shop_code,sh.short_name), (st.staff_Code, st.staff_Name), (cu.short_code, cu.customer_name, cu.address))';
    stm:= stm || '     order by kv.sort_order,kv.shop_code,kv.short_name,cn.sort_order,cn.shop_code,cn.short_name,sh.sort_order,sh.shop_code,sh.short_name,st.staff_Code,st.staff_Name,cu.short_code,cu.customer_name';
    stm:= stm || '     ) where gId != 7';
   dbms_output.put_line(stm);
   open res for stm;
  END P_DS_2_3;
   
  PROCEDURE P_DS_2_4(res OUT SYS_REFCURSOR , shopId CLOB, statusOrder STRING, fromDate DATE, toDate DATE)
  -- statusOrder: 1: Chua duyet; 2: Da duyet; 3: Tra hang; 4: Tu choi; 5: Huy; 6: Qua ngay chua duyet
  AS
  CURSOR c_product 
  IS
    WITH
    status AS(
            SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(statusOrder))
    ),
    dsSPTmp AS(
      SELECT sod.product_id
      FROM sale_order so
      JOIN sale_order_detail sod ON sod.sale_order_id = so.sale_order_id
      WHERE sod.is_free_item = 0
        AND (
            ((SELECT count(1) FROM status WHERE giaTri = 1) >= 1 AND so.approved = 0)
            OR ((SELECT count(1) FROM status WHERE giaTri = 2) >= 1 AND so.approved = 1 AND so.type = 1)
            OR ((SELECT count(1) FROM status WHERE giaTri = 3) >= 1 AND so.approved = 1 AND so.type = 0)
            OR ((SELECT count(1) FROM status WHERE giaTri = 4) >= 1 AND so.approved = 2)
            OR ((SELECT count(1) FROM status WHERE giaTri = 5) >= 1 AND so.approved = 3 AND so.destroy_code != 'AUTO_DEL')
            OR ((SELECT count(1) FROM status WHERE giaTri = 6) >= 1 AND so.approved = 3 AND so.destroy_code = 'AUTO_DEL')
            )
        AND so.order_date >= TRUNC(fromDate) AND so.order_date < TRUNC(toDate) + 1
        AND sod.order_date >= TRUNC(fromDate) AND sod.order_date < TRUNC(toDate) + 1
        AND so.shop_id IN (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(shopId)))
      )
    SELECT p.product_id
    FROM product p
    JOIN product_info cat ON cat.product_info_id = p.cat_id
    WHERE cat.type= 1 AND cat.object_type = 0 AND p.convfact > 1 
      AND (p.status = 1 OR (p.status = 0 AND EXISTS(SELECT 1 FROM dsSPTmp WHERE dsSPTmp.product_id=p.product_id)))
--      AND p.product_id = 10226
    ORDER BY p.sort_order
    ;
  
  CURSOR c_status IS
    SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(statusOrder));
    
  stm CLOB;
  condition_order CLOB;
  fromDateStr VARCHAR2(20);
  toDateStr VARCHAR2(20);
  approvedDateStr VARCHAR2(20);
  BEGIN
    dbms_output.enable(100000000000000);
    approvedDateStr := TO_CHAR(F_GET_DATE_NOT_DEL_ORDER(sysdate),'dd/mm/yyyy');
    fromDateStr := TO_CHAR(fromDate,'dd/mm/yyyy');
    toDateStr := TO_CHAR(toDate,'dd/mm/yyyy');

    condition_order := ' AND(1!=1 ';
    FOR v_status IN c_status
    LOOP
        CASE v_status.giaTri
            WHEN 1 THEN condition_order := condition_order || ' OR (so.approved = 0) ';
            WHEN 2 THEN condition_order := condition_order || ' OR (so.approved = 1 AND so.type = 1) ';
            WHEN 3 THEN condition_order := condition_order || ' OR (so.approved = 1 AND so.type = 0) ';
            WHEN 4 THEN condition_order := condition_order || ' OR (so.approved = 2) ';
            WHEN 5 THEN condition_order := condition_order || ' OR (so.approved = 3 AND so.destroy_code != ''AUTO_DEL'') ';
            WHEN 6 THEN condition_order := condition_order || ' OR (so.approved = 3 AND so.destroy_code = ''AUTO_DEL'') ';
        END CASE;
    END LOOP;
    condition_order := condition_order || ' ) ';

    stm:= 'SELECT khuvuc, chinhanh, npp, ngay, tenkh, diachi, sodonhang, trangThaiDH, Tong, tmp ';
    FOR v_product IN c_product
    LOOP
      stm:= stm || ', (CASE WHEN tmp != 0 AND p_' || v_product.product_id ||' is null then 0 else p_' || v_product.product_id ||' END) p_' || v_product.product_id;
    END LOOP;
    stm:= stm || ' FROM ( ';
    stm:= stm || ' SELECT sortKV,(CASE WHEN khuvuc is null then ''Tổng'' else TO_CHAR(khuvuc) END) khuvuc, sortCN, chinhanh, sortNPP, shop_name npp, orderDate ngay,';
    stm:= stm || ' customerName tenkh, address diachi, order_number sodonhang,order_source nguon, trangThaiDH, SUM(total) Tong ';
    stm:= stm || ' ,grouping_id(khuvuc,chinhanh, shop_name,orderdate, customerName, address, order_number,order_source) as tmp ';
    
    FOR v_product IN c_product
    LOOP
      stm:= stm || ', SUM(p_' || v_product.product_id ||') p_' || v_product.product_id ;
    END LOOP;
    
    stm:= stm || ' FROM(';
    stm:= stm || ' SELECT kv.sort_order sortKV,cn.sort_order sortCN,sh.sort_order sortNPP, kv.shop_code || '' - '' || kv.short_name khuvuc, cn.shop_code || '' - '' || cn.short_name chinhanh, so.shop_id, (sh.shop_code || '' - '' ||sh.short_name) shop_name, ';
    stm:= stm || ' so.staff_id, (st.staff_code || '' - '' || st.staff_name)staff_code, st.staff_name, ';
    stm:= stm || ' TO_CHAR(so.order_date,''dd/mm/yyyy'') orderDate ,so.order_number, (CASE WHEN so.order_source =1 then ''WEB'' else ''TABLET'' END)order_source,';
    stm:= stm || ' (SELECT short_code || '' - '' || customer_name FROM customer cu WHERE cu.customer_id= so.customer_id) customerName,';
    stm:= stm || ' (SELECT address FROM customer cu WHERE cu.customer_id= so.customer_id) address,';
    stm:= stm || ' NVL(so.total_quantity,0) total';
    stm:= stm || ' , CASE  WHEN (so.approved = 0) THEN TO_CHAR(''Chưa duyệt'') ';
    stm:= stm || '         WHEN (so.approved = 1 AND so.type = 1) THEN TO_CHAR(''Đã duyệt'') ';
    stm:= stm || '         WHEN (so.approved = 1 AND so.type = 0) THEN TO_CHAR(''Trả hàng'') ';
    stm:= stm || '         WHEN (so.approved = 2) THEN TO_CHAR(''Từ chối'') ';
    stm:= stm || '         WHEN (so.approved = 3 AND so.destroy_code != ''AUTO_DEL'') THEN TO_CHAR(''Hủy'') ';
    stm:= stm || '         WHEN (so.approved = 3 AND so.destroy_code = ''AUTO_DEL'') THEN TO_CHAR(''Quá ngày chưa duyệt'') ';
    stm:= stm || ' END trangThaiDH ';
    
    FOR v_product IN c_product
    LOOP
      stm:= stm || ',(SELECT SUM(sod.quantity) FROM sale_order_detail sod WHERE sod.is_free_item = 0 AND sod.sale_order_id = so.sale_order_id AND sod.product_id = '|| v_product.product_id ||') p_'|| v_product.product_id ;
    END LOOP;
    
    stm:= stm || ' FROM sale_order so ';
    stm:= stm || ' JOIN staff st on st.staff_id = so.staff_id';
    stm:= stm || ' JOIN shop sh on sh.shop_id = so.shop_id';
    stm:= stm || ' JOIN shop cn on sh.parent_shop_id = cn.shop_id';
    stm:= stm || ' JOIN shop kv on cn.parent_shop_id = kv.shop_id';
    stm:= stm || ' WHERE 1=1';
    stm:= stm || condition_order;
    stm:= stm || ' AND sh.SHOP_TYPE_ID IN(SELECT CHANNEL_TYPE_ID FROM CHANNEL_TYPE ct WHERE ct.type = 1 AND ct.CHANNEL_TYPE_CODE = ''NPP'')';
    stm:= stm || ' AND so.order_source = 1 ';
    stm:= stm || ' AND so.order_date >= TO_DATE(''' || fromDateStr ||''', ''dd/mm/yyyy'')';
    stm:= stm || ' AND so.order_date < TO_DATE(''' || toDateStr ||''', ''dd/mm/yyyy'') + 1';
    stm:= stm || ' AND so.shop_id IN (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(''' || shopId ||''')))';
    stm:= stm || ' )GROUP BY ROLLUP((sortKV,khuvuc),(sortCN,chinhanh), (sortNPP,shop_name),orderdate, (customerName, address, order_number,order_source,trangThaiDH))';
    stm:= stm || ' )';
    stm:= stm || ' ORDER BY sortKV, NLSSORT(khuvuc,''NLS_SORT=vietnamese''), sortCN, NLSSORT(chinhanh,''NLS_SORT=vietnamese''), sortNPP, NLSSORT(npp,''NLS_SORT=vietnamese''), TO_DATE(ngay,''dd/mm/yyyy''), NLSSORT(tenkh,''NLS_SORT=vietnamese'')';
    dbms_output.put_line(stm);
    OPEN res FOR stm;
  END P_DS_2_4;