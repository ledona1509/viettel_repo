create or replace PACKAGE BODY PKG_CRM_REPORT AS
  PROCEDURE P_CRM_3_3(res OUT SYS_REFCURSOR, p_ShopId NUMBER, p_FromDate DATE, p_ToDate DATE)
  AS
  -- @author: NaLD
  -- CRM3.3 - San luong phan phoi theo SKUs
  BEGIN
  OPEN res FOR
  WITH
  nppTmp AS(
    SELECT so.shop_id
    FROM sale_order so
    JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
    WHERE so.shop_id IN (SELECT shop_id FROM shop CONNECT BY PRIOR shop_id = parent_shop_id START WITH shop_id = p_ShopId)
      AND so.order_date >= TRUNC(p_FromDate) AND so.order_date < TRUNC(p_ToDate) + 1
      AND sod.order_date >= TRUNC(p_FromDate) AND sod.order_date < TRUNC(p_ToDate) + 1
      AND sod.is_free_item = 0
      AND so.approved = 1
      AND so.type = 1
  )
  ,
  spTmp AS(
    SELECT sod.product_id
    FROM sale_order so
    JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
    WHERE so.shop_id IN (SELECT shop_id FROM shop CONNECT BY PRIOR shop_id = parent_shop_id START WITH shop_id = p_ShopId)
      AND so.order_date >= TRUNC(p_FromDate) AND so.order_date < TRUNC(p_ToDate) + 1
      AND sod.order_date >= TRUNC(p_FromDate) AND sod.order_date < TRUNC(p_ToDate) + 1
      AND sod.is_free_item = 0
      AND so.approved = 1
      AND so.type = 1
  )
  ,
  npp AS(
    SELECT npp.shop_id
    FROM shop npp
    JOIN channel_type ct ON npp.shop_type_id = ct.channel_type_id
    WHERE ct.type = 1 AND ct.object_type = 3 
    AND npp.shop_id IN(SELECT shop_id FROM shop CONNECT BY PRIOR shop_id = parent_shop_id START WITH shop_id = p_ShopId)
    AND (npp.status = 1 OR (npp.status = 0 AND EXISTS (SELECT 1 FROM nppTmp WHERE npp.shop_id = nppTmp.shop_id)))
  )
  ,
  sp AS(
    SELECT p.product_id
    FROM product p
    JOIN product_info cat ON cat.product_info_id = p.cat_id
    WHERE cat.type = 1 AND cat.object_type = 0 AND p.convfact > 1 
      AND (p.status = 1 OR (p.status = 0 AND EXISTS (SELECT 1 FROM spTmp WHERE spTmp.product_id = p.product_id)))
  )
  ,
  t0 AS(
    select npp.shop_id,sp.product_id
    from npp, sp
  )
  ,
  t1 AS(
    SELECT so.shop_id, sod.product_id, COUNT(distinct so.customer_id) sdl, SUM(nvl(sod.quantity, 0)) sanluong
    FROM sale_order so
    LEFT JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
    WHERE 1=1
      AND so.shop_id in (SELECT npp.shop_id FROM npp)
      AND so.order_date >= TRUNC(p_FromDate) AND so.order_date < TRUNC(p_ToDate) + 1
      AND sod.order_date >= TRUNC(p_FromDate) AND sod.order_date < TRUNC(p_ToDate) + 1
      AND sod.is_free_item = 0
      AND so.approved = 1
      AND so.type = 1
    GROUP BY so.shop_id ,sod.product_id
  )
  ,
  t1_1 AS(
    SELECT so.shop_id, COUNT(distinct so.customer_id) sdl
          ,SUM(nvl(sod.quantity, 0)) sanluong
    FROM sale_order so
    LEFT JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
    WHERE 1=1
      AND so.shop_id in (SELECT npp.shop_id FROM npp)
      AND so.order_date >= TRUNC(p_FromDate) AND so.order_date < TRUNC(p_ToDate) + 1
      AND sod.order_date >= TRUNC(p_FromDate) AND sod.order_date < TRUNC(p_ToDate) + 1
      AND sod.is_free_item = 0
      AND so.approved = 1
      AND so.type = 1
    GROUP BY so.shop_id
  )
  ,
  t2 AS(
    SELECT t0.shop_id, t0.product_id, nvl(t1.sdl, 0) sdl, nvl(t1.sanluong, 0) sanluong, 0 tmp
    FROM t0
    LEFT JOIN t1 ON t1.shop_id = t0.shop_id AND t1.product_id = t0.product_id
    UNION ALL
    SELECT npp.shop_id, null product_id, nvl(t.sdl, 0) sdl, nvl(t.sanluong, 0) sanluong, 1 tmp
    FROM npp
    LEFT JOIN t1_1 t ON t.shop_id = npp.shop_id
  )
  ,
  t3 AS(
    SELECT
          kv.sort_order sortKV
          ,tinh.sort_order sortTinh
          ,npp.sort_order sortNPP
          ,sp.sort_order sortSP
          ,kv.shop_code || ' - ' ||kv.short_name as tenKV
          ,tinh.shop_code || ' - ' || tinh.short_name as tenTinh
          ,npp.shop_code || ' - ' || npp.short_name as tenNPP
          ,sp.short_name as tenSP
          ,t.sdl as sdl
          ,t.sanluong as sanluong
          ,t.tmp
    FROM t2 t
    JOIN shop npp on t.shop_id = npp.shop_id
    JOIN shop tinh on tinh.shop_id = npp.parent_shop_id
    JOIN shop kv on kv.shop_id = tinh.parent_shop_id
    LEFT JOIN product sp on sp.product_id = t.product_id
  )
  ,
  t4 AS(
    SELECT t.sortKV, t.tenKV, t.sortTinh, t.tenTinh, t.sortNPP, t.tenNPP
          ,t.sortSP, t.tenSP, t.sdl, t.sanluong, t.tmp
    FROM t3 t
  )
  ,
  t5 AS(
    SELECT t.sortKV, t.tenKV, t.sortTinh, t.tenTinh, t.sortNPP, t.tenNPP
          ,t.sortSP, t.tenSP, SUM(t.sdl) AS sdl, SUM(t.sanluong) AS sanluong, 3 tmp
    FROM t4 t
    WHERE t.tmp = 1
    GROUP BY ROLLUP ((t.sortKV,t.tenKV), (t.sortTinh,t.tenTinh), (t.sortNPP,t.tenNPP), (t.sortSP,t.tenSP))
  )
  ,
  t6 AS(
    SELECT * FROM t4
    UNION
    SELECT * FROM t5 WHERE t5.tenSP IS NULL
  )
  SELECT CASE WHEN t.tenKV IS NULL THEN UPPER(to_char('To�n qu?c')) ELSE UPPER(to_char(t.tenKV)) END tenKV
        ,UPPER(t.tenTinh) tenTinh
        ,UPPER(t.tenNPP) tenNPP
        ,t.tenSP
        ,t.sdl
        ,t.sanluong
  FROM t6 t
  WHERE t.tmp != 1
  ORDER BY t.sortKV, t.tenKV, t.sortTinh, t.tenTinh, t.sortNPP, t.tenNPP, t.sortSP, t.tenSP
  ;
  END P_CRM_3_3;

  PROCEDURE P_CRM_3_4(res OUT SYS_REFCURSOR, p_LstIdTinh STRING, p_FromDate DATE, p_ToDate DATE)
  AS
  -- @author: NaLD
  -- CRM3.4 - Thong ke so xa chua phat sinh doanh so
  BEGIN
    OPEN res FOR
    WITH
    -- Lay danh sach tat ca Phuong/Xa thuoc p_LstIdTinh
    px AS(
      SELECT px.area_id
      FROM area px
      WHERE 1=1
        AND ((p_LstIdTinh IS NULL AND px.parent_area_id IN (
                            SELECT qh.area_id
                            FROM area qh
                            WHERE qh.parent_area_id IN (
                                                        SELECT tinh.area_id
                                                        FROM area tinh
                                                        WHERE tinh.type = 1
                                                        )
                             )
              )
              OR
              (p_LstIdTinh IS NOT NULL AND px.parent_area_id IN (
                            SELECT qh.area_id
                            FROM area qh
                            WHERE qh.parent_area_id in (
                                                        SELECT TO_NUMBER(TRIM(regexp_substr(p_LstIdTinh,'[^,]+', 1, level)))
                                                        FROM dual
                                                        CONNECT BY regexp_substr(p_LstIdTinh, '[^,]+', 1, level) IS NOT NULL
                                                        )
                              )
              ))
    )
    ,
    -- Lay danh sach phuong/xa phat sinh doanh so
    pxpsds AS(
      SELECT c.area_id
      FROM(
        SELECT distinct so.customer_id
        FROM sale_order so
        JOIN sale_order_detail sod on so.sale_order_id = sod.sale_order_id
        WHERE 1=1
          AND so.order_date >= trunc(p_FromDate) AND so.order_date < trunc(p_ToDate) + 1
          AND sod.order_date >= trunc(p_FromDate) AND sod.order_date < trunc(p_ToDate) + 1
          AND so.approved = 1
          AND so.type = 1
      )tmp
      JOIN customer c ON c.customer_id = tmp.customer_id
    )
    ,
    -- Lay danh sach Phuong/Xa khong phat sinh doanh so
    pxkpsds AS(
      SELECT px.area_id FROM px
      MINUS
      SELECT pxpsds.area_id FROM pxpsds
    )
    ,
    t1 AS(
      SELECT
            tinh.area_name as tenTinh
            ,to_char(qh.area_name) as tenQH
            ,TO_CHAR(px.area_name) as tenPX
            ,(
              SELECT LISTAGG(to_char(tmp.tenShop), ', ') WITHIN GROUP (ORDER BY tmp.tenShop)
              FROM (
                    select distinct s.shop_code || ' - ' || s.short_name as tenShop, sam.area_id
                    from shop_area_map sam
                    join shop s ON s.shop_id = sam.shop_id
                    WHERE sam.status = 1
                    ) tmp
              WHERE tmp.area_id IN (SELECT a.area_id FROM area a START WITH a.area_id = t.area_id  CONNECT BY a.area_id = PRIOR a.parent_area_id)
              ) npp
              , 0 AS temp
      FROM pxkpsds t
      JOIN area px on px.area_id = t.area_id
      JOIN area qh ON qh.area_id = px.parent_area_id
      JOIN area tinh on tinh.area_id = qh.parent_area_id
      ORDER BY tinh.area_name, qh.area_name, px.area_name
    )
    ,
    t2 AS(
      SELECT t.tenTinh
            ,TO_CHAR(COUNT(distinct t.tenQH)) AS tenQH
            ,TO_CHAR(COUNT(t.tenPX)) AS tenPX
            ,NULL AS npp
            , 1 AS temp
      FROM t1 t
      GROUP BY ROLLUP (t.tenTinh)
    )
    ,
    t3 AS(
      SELECT * FROM t1
      UNION
      SELECT * FROM t2
    )
    SELECT CASE WHEN t.tenTinh IS NULL THEN TO_CHAR('T?ng') ELSE TO_CHAR(t.tenTinh) END tenTinh
          ,t.tenQH
          ,t.tenPX
          ,t.npp
    FROM t3 t
    ORDER BY
            NLSSORT(t.tenTinh,'NLS_SORT=vietnamese')
            , t.temp
            ,NLSSORT(t.tenQH,'NLS_SORT=vietnamese')
            ,NLSSORT(t.tenPX,'NLS_SORT=vietnamese')

    ;
  END P_CRM_3_4;

  PROCEDURE P_CRM_3_1(res OUT SYS_REFCURSOR, lstTinhId STRING, fromDate DATE, toDate DATE)
  AS
  BEGIN
    open res for
    with px as (SELECT p.area_id provinceId, p.area_code || ' - ' || p.area_name tinh, nvl(d.area_id,0) districtId, nvl(w.area_id,0) wardId
      FROM area p
      left join area d on d.parent_area_id = p.area_id and d.status               = 1 and d.type = 2
      left join area w on w.parent_area_id = d.area_id and w.status               = 1 and w.type = 3
      WHERE p.status               = 1 and p.type = 1

      AND (lstTinhId is null or p.area_id in (SELECT TO_NUMBER(TRIM(regexp_substr(lstTinhId,'[^,]+', 1, level)))
                                                        FROM dual
                                                        CONNECT BY regexp_substr(lstTinhId, '[^,]+', 1, level) IS NOT NULL) )
    )
    select v_tt.tinh , nvl(v_tt.tsXa,0) tsXa, nvl(v_ds.xaPSDS,0) xaPSDS,
          (nvl(v_tt.tsXa,0) -  nvl(v_ds.xaPSDS,0)) xaCPSDS, nvl(v_ds.khPSDS,0) khPSDS
      from
      (select px.provinceId, px.tinh, sum(case when wardId>0 then 1 else 0 end) tsXa
          from px
          group by px.provinceId, px.tinh
      )v_tt
      left join (select px.provinceId,  count(distinct cu.area_id) xaPSDS, count(distinct cu.customer_id) khPSDS
        from sale_order so
        join customer cu on cu.customer_id = so.customer_id
        join px on px.wardId = cu.area_id
        WHERE 1=1
        AND so.order_date >= trunc(fromDate) AND so.order_date < trunc(toDate) + 1
        AND so.approved = 1 AND so.type = 1
        AND so.amount > 0
        and cu.area_id in (select wardId from px)
        group by px.provinceId
      )v_ds on v_ds.provinceId = v_tt.provinceId
      order by v_tt.tinh, v_tt.provinceId;

  END P_CRM_3_1;

  PROCEDURE P_CRM_3_5(res OUT SYS_REFCURSOR, p_ShopId NUMBER, p_Date DATE)
  -- @author: NaLD
  -- CRM-3.5 - Bao cao thuc hien ke hoach thang va nam theo ke hoach
  AS
  BEGIN
  OPEN res FOR
    WITH
    dsDVTMP AS(
       SELECT s.shop_id
       FROM shop s
       JOIN channel_type ct ON ct.channel_type_id = s.shop_type_id
       WHERE 1=1
       AND ct.type = 1
       AND ct.object_type IN (1, 2)
       AND s.shop_id IN (SELECT shop_id FROM shop WHERE shop.status IN (0,1) START WITH shop_id = p_ShopID CONNECT BY PRIOR shop_id = parent_shop_id)
     )
     ,
     dsDV AS(
      SELECT s.shop_id, ct.object_type loai
      FROM (
          SELECT s.shop_id
          FROM shop s
          JOIN channel_type ct ON ct.channel_type_id = s.shop_type_id
           WHERE ct.type = 1 AND ct.object_type IN (1, 2)
           AND s.shop_id IN (SELECT shop_id FROM shop WHERE shop.status = 1 START WITH shop_id = p_ShopID CONNECT BY PRIOR shop_id = parent_shop_id)
          UNION
          SELECT sp.object_id shop_id
          FROM sale_plan sp
          WHERE sp.object_type = 1
            AND sp.type = 2
            AND sp.month = extract(month FROM p_Date)
            AND sp.year = extract(year FROM p_Date)
            AND sp.object_id IN (SELECT * FROM dsDVTMP)
          UNION
          SELECT s.parent_shop_id shop_id
          FROM rpt_sale_in_month rpt
          JOIN shop s ON s.shop_id = rpt.shop_id
          WHERE 1=1
            AND rpt.status = 1
            AND trunc(rpt.month) = trunc(p_Date, 'mm')
            AND s.parent_shop_id IN (SELECT * FROM dsDVTMP)
          union
          select spf.object_id shop_id
          from sale_plan_frame spf
          where spf.object_type = 1
            AND spf.type = 2
            AND spf.month = extract(month FROM p_Date)
            AND spf.year = extract(year FROM p_Date)
            AND spf.object_id IN (SELECT * FROM dsDVTMP)
          )t
      JOIN shop s ON s.shop_id = t.shop_id
      JOIN channel_type ct ON ct.channel_type_id = s.shop_type_id
     )
      ,
      khttThang AS(
        SELECT sp.object_id shop_id
              ,SUM(sp.quantity) giaTri
        FROM sale_plan sp
        WHERE 1=1
          AND sp.object_type = 1
          AND sp.type = 2
          AND sp.month = extract(month FROM p_Date)
          AND sp.year = extract(year FROM p_Date)
          AND sp.object_id IN (SELECT dsDV.shop_id FROM dsDV)
        GROUP BY sp.object_id
      )
      ,
      lkttThang AS(
        SELECT sh.parent_shop_id shop_id
              ,SUM(rpt.quantity) giaTri
        FROM rpt_sale_in_month rpt
        JOIN staff s ON s.staff_id = rpt.staff_id
        JOIN shop sh ON sh.shop_id = s.shop_id
        WHERE 1=1
          AND rpt.status = 1
          AND trunc(rpt.month) = trunc(p_Date, 'mm')
          AND sh.parent_shop_id IN (SELECT dsDV.shop_id FROM dsDV WHERE dsDV.loai = 2)
        GROUP BY sh.parent_shop_id
      )
      ,
      lkttThangKV AS(
        SELECT kv.shop_id, SUM(lk.giaTri) giaTri
        FROM lkttThang lk
        JOIN shop tinh ON tinh.shop_id = lk.shop_id
        JOIN shop kv ON kv.shop_id = tinh.parent_shop_id
        GROUP BY kv.shop_id
      )
      ,
      khttNam AS(
        SELECT sp.object_id shop_id
              ,SUM(sp.quantity) giaTri
        FROM sale_plan sp
        WHERE 1=1
          AND sp.object_type = 1
          AND sp.type = 2
          AND sp.month >= 1 AND sp.month <= 12
          AND sp.year = extract(YEAR FROM p_Date)
          AND sp.object_id IN (SELECT dsDV.shop_id FROM dsDV)
        GROUP BY sp.object_id
      )
      ,
      lkttNam AS(
        SELECT sh.parent_shop_id shop_id
              ,SUM(rpt.quantity) giaTri
        FROM rpt_sale_in_month rpt
        JOIN staff s ON s.staff_id = rpt.staff_id
        JOIN shop sh ON sh.shop_id = s.shop_id
        WHERE 1=1
          AND rpt.status = 1
          AND trunc(rpt.month) >= TRUNC(p_Date , 'Year')
          AND ((trunc(rpt.month) <= trunc(p_Date, 'mm') AND (extract(year FROM p_Date) = extract(year FROM sysdate)))
              OR
              ((extract(year FROM p_Date) < extract(year FROM sysdate)) AND (TRUNC(rpt.month) <= TRUNC(LAST_DAY(ADD_MONTHS(TRUNC(p_Date , 'Year'),11)), 'mm')))
              )
          AND sh.parent_shop_id IN (SELECT dsDV.shop_id FROM dsDV WHERE dsDV.loai = 2)
        GROUP BY sh.parent_shop_id
      )
      ,
      lkttNamKV AS(
        SELECT kv.shop_id, SUM(lk.giaTri) giaTri
        FROM lkttNam lk
        JOIN shop tinh ON tinh.shop_id = lk.shop_id
        JOIN shop kv ON kv.shop_id = tinh.parent_shop_id
        GROUP BY kv.shop_id
      )
      ,
      lkttNamTruoc AS(
        SELECT sh.parent_shop_id shop_id
              ,SUM(rpt.quantity) giaTri
        FROM rpt_sale_in_month rpt
        JOIN staff s ON s.staff_id = rpt.staff_id
        JOIN shop sh ON sh.shop_id = s.shop_id
        WHERE 1=1
          AND rpt.status = 1
          AND trunc(rpt.month) >= TRUNC(add_months(p_Date, -12) , 'Year') AND trunc(rpt.month) <= trunc(add_months(p_Date, -12), 'mm')
          AND sh.parent_shop_id IN (SELECT dsDV.shop_id FROM dsDV WHERE dsDV.loai = 2)
        GROUP BY sh.parent_shop_id
      )
      ,
      lkttNamTruocKV AS(
        SELECT kv.shop_id, SUM(lk.giaTri) giaTri
        FROM lkttNamTruoc lk
        JOIN shop tinh ON tinh.shop_id = lk.shop_id
        JOIN shop kv ON kv.shop_id = tinh.parent_shop_id
        GROUP BY kv.shop_id
      )
      ,
      khKhungNam as (
        select object_id shop_id, sum(quantity) giaTri
        from sale_plan_frame
        where type = 2 and object_type = 1
          and month >= 1 and month <= 12
          and year = extract(year from p_Date)
          and object_id in (select shop_id from dsDV)
        group by object_id
      )
      ,
      t1 AS(
        SELECT kv.shop_id idKV
              ,tinh.shop_id idTinh
              ,NVL(khthang.giaTri, 0) khttThang
              ,NVL(lkThang.giaTri, 0) lkttThang
              ,nvl(khKNam.giaTri,0) khKhungNam
              ,NVL(khnam.giaTri, 0) khttNam
              ,NVL(lknam.giaTri, 0) lkttNam
              ,NVL(lknamtruoc.giaTri, 0) lkttNamTruoc
        FROM dsDV
        LEFT JOIN shop tinh ON tinh.shop_id = dsDV.shop_id
        LEFT JOIN shop kv ON kv.shop_id = tinh.parent_shop_id
        LEFT JOIN khttThang khthang ON khthang.shop_id = tinh.shop_id
        LEFT JOIN lkttThang lkThang ON lkThang.shop_id = tinh.shop_id
        left join khKhungNam khKNam on khKNam.shop_id = tinh.shop_id
        LEFT JOIN khttNam khnam ON khnam.shop_id = tinh.shop_id
        LEFT JOIN lkttNam lknam ON lknam.shop_id = tinh.shop_id
        LEFT JOIN lkttNamTruoc lknamtruoc ON lknamtruoc.shop_id = tinh.shop_id
        WHERE dsDV.loai = 2
      )
      ,
      t2 AS(
        SELECT
              t.idKV
              ,t.idTinh
              ,t.khttThang
              ,t.lkttThang
              ,CASE WHEN (t.khttThang = 0 AND t.lkttThang != 0) THEN 100
                    WHEN (t.khttThang = 0 AND t.lkttThang = 0) THEN 0
                  ELSE round(t.lkttThang / t.khttThang * 100)
                END thkhThang
              ,t.khKhungNam
              ,t.khttNam
              ,t.lkttNam
              ,t.lkttNamTruoc
              ,CASE WHEN (t.khttNam = 0 AND t.lkttNam != 0) THEN 100
                    WHEN (t.khttNam = 0 AND t.lkttNam = 0) THEN 0
                  ELSE round(t.lkttNam / t.khttNam * 100)
                END thkhNam
              ,CASE WHEN (t.khKhungNam = 0 AND t.lkttNam != 0) THEN 100
                    WHEN (t.khKhungNam = 0 AND t.lkttNam = 0) THEN 0
                  ELSE round(t.lkttNam / t.khKhungNam * 100)
                END thkhKhungNam
              ,CASE WHEN (t.lkttNamTruoc = 0 AND t.lkttNam != 0) THEN 100
                    WHEN (t.lkttNamTruoc = 0 AND t.lkttNam = 0) THEN 0
                  ELSE round((t.lkttNam - t.lkttNamTruoc) / t.lkttNamTruoc * 100)
                END tangTruong
        FROM t1 t
      )
      -- Chi nh�nh T?nh
      ,
      t3 AS(
        SELECT t.idKV
              ,t.idTinh
              ,t.khttThang
              ,t.lkttThang
              ,t.thkhThang
              ,DENSE_RANK() OVER(PARTITION BY t.idKV ORDER BY t.thkhThang DESC) xhThang
              ,t.khKhungNam
              ,t.khttNam
              ,t.lkttNam
              ,t.lkttNamTruoc
              ,t.thkhKhungNam
              ,t.thkhNam
              ,DENSE_RANK() OVER(PARTITION BY t.idKV ORDER BY t.thkhNam DESC) xhNam
              ,t.tangTruong
              ,1 tmp
        FROM t2 t
      )
      ,
      t4 AS(
        SELECT
              t.idKV
              ,t.idTinh
              ,t.khttThang
              ,t.lkttThang
              ,CASE WHEN (t.khttThang = 0 AND t.lkttThang != 0) THEN 100
                    WHEN (t.khttThang = 0 AND t.lkttThang = 0) THEN 0
                  ELSE round(t.lkttThang / t.khttThang * 100)
                END thkhThang
              ,t.khKhungNam
              ,t.khttNam
              ,t.lkttNam
              ,t.lkttNamTruoc
              ,CASE WHEN (t.khttNam = 0 AND t.lkttNam != 0) THEN 100
                    WHEN (t.khttNam = 0 AND t.lkttNam = 0) THEN 0
                  ELSE round(t.lkttNam / t.khttNam * 100)
                END thkhNam
              ,CASE WHEN (t.khKhungNam = 0 AND t.lkttNam != 0) THEN 100
                    WHEN (t.khKhungNam = 0 AND t.lkttNam = 0) THEN 0
                  ELSE round(t.lkttNam / t.khKhungNam * 100)
                END thkhKhungNam
              ,CASE WHEN (t.lkttNamTruoc = 0 AND t.lkttNam != 0) THEN 100
                    WHEN (t.lkttNamTruoc = 0 AND t.lkttNam = 0) THEN 0
                  ELSE round((t.lkttNam - t.lkttNamTruoc) / t.lkttNamTruoc * 100)
                END tangTruong
        FROM(
            SELECT
                  kv.shop_id idKV
                  ,kv.shop_id idTinh
                  ,NVL(khthang.giaTri, 0) khttThang
                  ,NVL(lkThang.giaTri, 0) lkttThang
                  ,nvl(khKNam.giaTri,0) khKhungNam
                  ,NVL(khnam.giaTri, 0) khttNam
                  ,NVL(lknam.giaTri, 0) lkttNam
                  ,NVL(lknamtruoc.giaTri, 0) lkttNamTruoc
            FROM dsDV
            LEFT JOIN shop kv ON kv.shop_id = dsDV.shop_id
            LEFT JOIN khttThang khthang ON khthang.shop_id = kv.shop_id
            LEFT JOIN lkttThangKV lkThang ON lkThang.shop_id = kv.shop_id
            left join khKhungNam khKNam on khKNam.shop_id = kv.shop_id
            LEFT JOIN khttNam khnam ON khnam.shop_id = kv.shop_id
            LEFT JOIN lkttNamKV lknam ON lknam.shop_id = kv.shop_id
            LEFT JOIN lkttNamTruocKV lknamtruoc ON lknamtruoc.shop_id = kv.shop_id
            WHERE dsDV.loai = 1
            )t
      )
      -- Khu v?c
      ,
      t5 AS(
        SELECT t.idKV
              ,t.idTinh
              ,t.khttThang
              ,t.lkttThang
              ,t.thkhThang
              ,DENSE_RANK() OVER(ORDER BY t.thkhThang DESC) xhThang
              ,t.khKhungNam
              ,t.khttNam
              ,t.lkttNam
              ,t.lkttNamTruoc
              ,t.thkhKhungNam
              ,t.thkhNam
              ,DENSE_RANK() OVER(ORDER BY t.thkhNam DESC) xhNam
              ,t.tangTruong
              ,0 tmp
        FROM t4 t
      )
      -- D?ng t?ng
      ,
      t6 AS(
        SELECT NULL idKV
              ,NULL idTinh
              ,t.khttThang
              ,t.lkttThang
              ,CASE WHEN (t.khttThang = 0 AND t.lkttThang != 0) THEN 100
                    WHEN (t.khttThang = 0 AND t.lkttThang = 0) THEN 0
                  ELSE round(t.lkttThang / t.khttThang * 100)
                END thkhThang
              ,NULL xhThang
              ,t.khKhungNam
              ,t.khttNam
              ,t.lkttNam
              ,t.lkttNamTruoc
              ,CASE WHEN (t.khKhungNam = 0 AND t.lkttNam != 0) THEN 100
                    WHEN (t.khKhungNam = 0 AND t.lkttNam = 0) THEN 0
                  ELSE round(t.lkttNam / t.khttNam * 100)
                END thkhKhungNam
              ,CASE WHEN (t.khttNam = 0 AND t.lkttNam != 0) THEN 100
                    WHEN (t.khttNam = 0 AND t.lkttNam = 0) THEN 0
                  ELSE round(t.lkttNam / t.khttNam * 100)
                END thkhNam
              ,NULL xhNam
              ,CASE WHEN (t.lkttNamTruoc = 0 AND t.lkttNam != 0) THEN 100
                    WHEN (t.lkttNamTruoc = 0 AND t.lkttNam = 0) THEN 0
                  ELSE round((t.lkttNam - t.lkttNamTruoc) / t.lkttNamTruoc * 100)
                END tangTruong
              ,2 tmp
        FROM(
            SELECT
                  (SELECT SUM(kh.giaTri) FROM khttThang kh WHERE kh.shop_id IN (SELECT dsDV.shop_id FROM dsDV WHERE dsDV.loai = 1)) khttThang
                 ,(SELECT SUM(lk.giaTri) FROM lkttThangKV lk) lkttThang
                 ,(SELECT SUM(khKNam.giaTri) FROM khKhungNam khKNam WHERE khKNam.shop_id IN (SELECT dsDV.shop_id FROM dsDV WHERE dsDV.loai = 1)) khKhungNam
                 ,(SELECT SUM(kh.giaTri) FROM khttNam kh WHERE kh.shop_id IN (SELECT dsDV.shop_id FROM dsDV WHERE dsDV.loai = 1)) khttNam
                 ,(SELECT SUM(lk.giaTri) FROM lkttNamKV lk) lkttNam
                 ,(SELECT SUM(lk.giaTri) FROM lkttNamTruocKV lk) lkttNamTruoc
            FROM dual
            )t
      )
      ,
      t7 AS(
        SELECT * FROM t3
        UNION ALL
        SELECT * FROM t5
        UNION ALL
        SELECT * FROM t6
      )
      SELECT
              CASE WHEN t.tmp = 2 THEN TO_CHAR('T?ng')
                  ELSE TO_CHAR(kv.short_name)
              END tenKV
              ,CASE WHEN t.tmp = 2 THEN NULL
                  WHEN t.tmp = 0 THEN TO_CHAR('T?ng ' || kv.short_name)
                  WHEN t.tmp = 1 THEN TO_CHAR(tinh.short_name)
              END tenTinh
              ,CASE WHEN t.tmp = 2 THEN NVL(t.khttThang, 0) ELSE t.khttThang END khttThang
              ,CASE WHEN t.tmp = 2 THEN NVL(t.lkttThang, 0) ELSE t.lkttThang END lkttThang
--              ,t.lkttThang
              ,CASE WHEN t.tmp = 2 THEN NVL(t.thkhThang, 0) ELSE t.thkhThang END thkhThang
--              ,t.thkhThang
              ,t.xhThang
              ,CASE WHEN t.tmp = 2 THEN NVL(t.khKhungNam, 0) ELSE t.khKhungNam END khKhungNam
              ,CASE WHEN t.tmp = 2 THEN NVL(t.khttNam, 0) ELSE t.khttNam END khttNam
--              ,t.khttNam
              ,CASE WHEN t.tmp = 2 THEN NVL(t.lkttNam, 0) ELSE t.lkttNam END lkttNam
--              ,t.lkttNam
              ,CASE WHEN t.tmp = 2 THEN NVL(t.lkttNamTruoc, 0) ELSE t.lkttNamTruoc END lkttNamTruoc
--              ,t.lkttNamTruoc
              ,CASE WHEN t.tmp = 2 THEN NVL(t.thkhKhungNam, 0) ELSE t.thkhKhungNam END thkhKhungNam
              ,CASE WHEN t.tmp = 2 THEN NVL(t.thkhNam, 0) ELSE t.thkhNam END thkhNam
--              ,t.thkhNam
              ,t.tmp
              ,t.xhNam
              ,CASE WHEN t.tmp = 2 THEN NVL(t.tangTruong, 0) ELSE t.tangTruong END tangTruong
--              ,t.tangTruong
      FROM t7 t
      LEFT JOIN shop tinh ON tinh.shop_id = t.idTinh
      LEFT JOIN shop kv ON kv.shop_id = t.idKV
      ORDER BY kv.shop_code, t.tmp, tinh.shop_code
      ;
  END P_CRM_3_5;

  PROCEDURE P_CRM_3_2(res OUT SYS_REFCURSOR, p_LstIdTinh STRING, p_FromDate DATE, p_ToDate DATE)
  AS
  -- @author: NaLD
  -- CRM3.2 - Bao cao chi tiet bao phu phuong/xa theo san luong
  g_StrSQL CLOB;
  gSQL1 CLOB;
  gSQL1_2 CLOB;
  gSQL1_3 CLOB;
  gSQL1_4 CLOB;
  gSQL1_5 CLOB;
  gSQL1_6 CLOB;
  gSQL2 CLOB;
  g_FromDateStr VARCHAR(50 byte);
  g_ToDateStr VARCHAR(50 byte);
  -- Danh sach san pham BSG (status = 0 hoac 1)
  CURSOR c_ds_sp
  IS
    SELECT t.product_id
    FROM(
      SELECT p.product_id
      FROM product p
      JOIN product_info pi on p.cat_id = pi.product_info_id
      WHERE pi.type = 1 AND p.convfact > 1 AND pi.object_type= 0 AND p.status = 1
      UNION
      SELECT sod.product_id
      FROM sale_order so
      JOIN sale_order_detail sod on so.sale_order_id = sod.sale_order_id
      JOIN customer c on c.customer_id = so.customer_id
      WHERE 1=1
        AND sod.is_free_item = 0
        AND so.approved = 1 AND so.type = 1
        AND sod.product_id IN (SELECT p.product_id
                                FROM product p
                                JOIN product_info pi on p.cat_id = pi.product_info_id
                                WHERE pi.type = 1 AND p.convfact > 1 AND pi.object_type= 0 AND p.status = 0
                              )
        AND c.area_id IN (SELECT a.area_id FROM area a
                          START WITH a.area_id IN (
                                                  SELECT TO_NUMBER(TRIM(REGEXP_SUBSTR(p_LstIdTinh,'[^,]+', 1, level)))
                                                  FROM dual
                                                  CONNECT BY REGEXP_SUBSTR( p_LstIdTinh, '[^,]+', 1, level) IS NOT NULL
                                                  )
                          CONNECT BY PRIOR a.area_id = a.parent_area_id)
      )t
    JOIN product p ON p.product_id = t.product_id
--    ORDER BY NLSSORT(p.product_name,'NLS_SORT=vietnamese')
    ORDER BY p.sort_order
      ;
  -- Danh sach san pham bia doi thu dang hoat dong
  CURSOR c_ds_spdt
  IS
    SELECT op.op_product_id product_id
    FROM op_product op
    WHERE op.status = 1
    ORDER BY NLSSORT(op.product_name,'NLS_SORT=vietnamese')
        ;
  BEGIN
    dbms_output.enable(100000000);
    g_FromDateStr := to_char(p_FromDate,'DD/MM/YYYY');
    g_ToDateStr := to_char(p_ToDate,'DD/MM/YYYY');
    gSQL1 := gSQL1 ||' WITH ';
    gSQL1 := gSQL1 ||' dsPX AS( ';
    gSQL1 := gSQL1 ||' SELECT area_id ';
    gSQL1 := gSQL1 ||' ,area_name ';
    gSQL1 := gSQL1 ||' ,parent_area_id ';
    gSQL1 := gSQL1 ||' ,district_name ';
    gSQL1 := gSQL1 ||' ,district ';
    gSQL1 := gSQL1 ||' ,province_name ';
    gSQL1 := gSQL1 ||' ,province ';
    gSQL1 := gSQL1 ||' FROM area px ';
    gSQL1 := gSQL1 ||' WHERE px.province IN( ';
    gSQL1 := gSQL1 ||' SELECT tinh.area_code ';
    gSQL1 := gSQL1 ||' FROM area tinh ';
    gSQL1 := gSQL1 ||' WHERE 1=1 ';
    DBMS_OUTPUT.PUT_LINE(gSQL1);
    gSQL1_2 := gSQL1_2 ||' AND ((''' || p_LstIdTinh || ''' IS NULL AND tinh.type = 1) ';
    DBMS_OUTPUT.PUT_LINE(gSQL1_2);
    gSQL1_3 := gSQL1_3 ||' OR (''' || p_LstIdTinh || ''' IS NOT NULL AND tinh.area_id IN ';
    DBMS_OUTPUT.PUT_LINE(gSQL1_3);
    gSQL1_4 := gSQL1_4 ||' (SELECT TO_NUMBER(TRIM(REGEXP_SUBSTR(''' || p_LstIdTinh || ''',''[^,]+'', 1, level))) FROM dual ';
    DBMS_OUTPUT.PUT_LINE(gSQL1_4);
    gSQL1_5 := gSQL1_5 ||' CONNECT BY REGEXP_SUBSTR(''' || p_LstIdTinh || ''', ''[^,]+'', 1, level) IS NOT NULL))) ';
    DBMS_OUTPUT.PUT_LINE(gSQL1_5);
    gSQL1_6 := gSQL1_6 ||' ) ';
    gSQL1_6 := gSQL1_6 ||' AND px.type = 3 ';
    gSQL1_6 := gSQL1_6 ||' ) ';
    gSQL1_6 := gSQL1_6 ||' , ';
    gSQL1_6 := gSQL1_6 ||' slBSG AS( ';
    gSQL1_6 := gSQL1_6 ||' SELECT ';
    gSQL1_6 := gSQL1_6 ||' c.area_id ';
    gSQL1_6 := gSQL1_6 ||' ,sod.product_id ';
    gSQL1_6 := gSQL1_6 ||' ,SUM(sod.quantity) giaTri ';
    gSQL1_6 := gSQL1_6 ||' FROM sale_order so ';
    gSQL1_6 := gSQL1_6 ||' JOIN sale_order_detail sod on so.sale_order_id = sod.sale_order_id ';
    gSQL1_6 := gSQL1_6 ||' JOIN customer c on c.customer_id = so.customer_id ';
    gSQL1_6 := gSQL1_6 ||' WHERE 1=1 ';
    gSQL1_6 := gSQL1_6 ||' AND sod.is_free_item = 0 ';
    gSQL1_6 := gSQL1_6 ||' AND so.approved = 1 ';
    gSQL1_6 := gSQL1_6 ||' AND so.type = 1 ';
    gSQL1_6 := gSQL1_6 ||' AND so.order_date >= TRUNC(TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY'')) AND so.order_date < TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY'')) +1 ';
    gSQL1_6 := gSQL1_6 ||' AND sod.order_date >= TRUNC(TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY'')) AND sod.order_date < TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY'')) +1 ';
    gSQL1_6 := gSQL1_6 ||' GROUP BY c.area_id, sod.product_id ';
    gSQL1_6 := gSQL1_6 ||' ) ';
    gSQL1_6 := gSQL1_6 ||' , ';
    gSQL1_6 := gSQL1_6 ||' slDT AS( ';
    gSQL1_6 := gSQL1_6 ||' SELECT c.area_id ';
    gSQL1_6 := gSQL1_6 ||' ,osv.op_product_id product_id ';
    gSQL1_6 := gSQL1_6 ||' ,SUM(osv.quantity) giaTri ';
    gSQL1_6 := gSQL1_6 ||' FROM op_sale_volume osv ';
    gSQL1_6 := gSQL1_6 ||' JOIN customer c on c.customer_id = osv.customer_id ';
    gSQL1_6 := gSQL1_6 ||' WHERE 1=1 ';
    gSQL1_6 := gSQL1_6 ||' AND osv.sale_date >= TRUNC(TO_DATE('''||g_FromDateStr||''',''DD/MM/YYYY'')) AND osv.sale_date < TRUNC(TO_DATE('''||g_ToDateStr||''',''DD/MM/YYYY'')) +1 ';
    gSQL1_6 := gSQL1_6 ||' GROUP BY c.area_id, osv.op_product_id ';
    gSQL1_6 := gSQL1_6 ||' ) ';
    gSQL1_6 := gSQL1_6 ||' ,t1 AS( ';
    gSQL1_6 := gSQL1_6 ||' SELECT ';
    gSQL1_6 := gSQL1_6 ||' px.province || '' - '' || px.province_name tenTinh ';
    gSQL1_6 := gSQL1_6 ||' ,px.district maQH ';
    gSQL1_6 := gSQL1_6 ||' ,TO_CHAR(px.district_name) tenQH ';
    gSQL1_6 := gSQL1_6 ||' ,TO_CHAR(px.area_name) tenPX ';
    DBMS_OUTPUT.PUT_LINE(gSQL1_6);
    FOR x IN c_ds_sp
    LOOP
      gSQL2 := gSQL2 ||'
            ,NVL((SELECT giaTri FROM slBSG WHERE product_id = '|| x.product_id ||' AND area_id = px.area_id), 0) bsg'|| x.product_id ||'';
    END LOOP;

    FOR x IN c_ds_spdt
    LOOP
      gSQL2 := gSQL2 ||'
            ,NVL((SELECT giaTri FROM slDT WHERE product_id = '|| x.product_id ||' AND area_id = px.area_id), 0) dt'|| x.product_id ||'';
    END LOOP;

    gSQL2 := gSQL2 ||' ,0 tmp ';
    gSQL2 := gSQL2 ||' FROM dsPX px ';
    gSQL2 := gSQL2 ||' ) ';

--    FOR x IN c_ds_sp
--    LOOP
--      g_StrSQL := g_StrSQL ||'
--      LEFT JOIN slBSG_'|| x.product_id ||' ON px.area_id = slBSG_'|| x.product_id ||'.area_id';
--    END LOOP;
--
--    FOR x IN c_ds_spdt
--    LOOP
--      g_StrSQL := g_StrSQL ||'
--      LEFT JOIN slDT_'|| x.product_id ||' ON px.area_id = slDT_'|| x.product_id ||'.area_id';
--    END LOOP;
    gSQL2 := gSQL2 ||' , ';
    gSQL2 := gSQL2 ||' t2 AS( ';
    gSQL2 := gSQL2 ||' SELECT ';
    gSQL2 := gSQL2 ||' t.tenTinh ';
    gSQL2 := gSQL2 ||' ,NULL AS maQH ';
    gSQL2 := gSQL2 ||' ,TO_CHAR(COUNT(DISTINCT t.maQH)) AS tenQH ';
    gSQL2 := gSQL2 ||' ,TO_CHAR(COUNT(t.tenPX)) AS tenPX ';
    FOR x IN c_ds_sp
    LOOP
      gSQL2 := gSQL2 ||'
            ,SUM(t.bsg'|| x.product_id ||') AS bsg'|| x.product_id ||'';
    END LOOP;

    FOR x IN c_ds_spdt
    LOOP
      gSQL2 := gSQL2 ||'
            ,SUM(t.dt'|| x.product_id ||') AS dt'|| x.product_id ||'';
    END LOOP;
    gSQL2 := gSQL2 ||' ,1 tmp ';
    gSQL2 := gSQL2 ||' FROM t1 t ';
    gSQL2 := gSQL2 ||' GROUP BY ROLLUP (t.tenTinh) ';
    gSQL2 := gSQL2 ||' ) ';
    gSQL2 := gSQL2 ||' , ';
    gSQL2 := gSQL2 ||' t3 AS( ';
    gSQL2 := gSQL2 ||' SELECT * FROM t1 ';
    gSQL2 := gSQL2 ||' UNION ';
    gSQL2 := gSQL2 ||' SELECT * FROM t2 ';
    gSQL2 := gSQL2 ||' ) ';
    gSQL2 := gSQL2 ||' SELECT CASE WHEN t.tenTinh IS NULL THEN TO_CHAR(''T?ng'') ELSE TO_CHAR(t.tenTinh) END tenTinh ';
    gSQL2 := gSQL2 ||' ,t.tenQH ';
    gSQL2 := gSQL2 ||' ,t.tenPX ';
    FOR x IN c_ds_sp
    LOOP
    gSQL2 := gSQL2 ||' ,t.bsg'|| x.product_id ||'';
    END LOOP;
    FOR x IN c_ds_spdt
    LOOP
    gSQL2 := gSQL2 ||' ,t.dt'|| x.product_id ||'';
    END LOOP;
    gSQL2 := gSQL2 ||' FROM t3 t ';
    gSQL2 := gSQL2 ||' ORDER BY t.tenTinh, t.tmp ';
    DBMS_OUTPUT.PUT_LINE(gSQL2);

    g_StrSQL := gSQL1 || gSQL1_2 || gSQL1_3 || gSQL1_4 || gSQL1_5 || gSQL1_6 || gSQL2;
    OPEN res FOR (g_StrSQL);
  END P_CRM_3_2;
  
  PROCEDURE P_CRM_3_7(res OUT SYS_REFCURSOR, lstShopId CLOB, fromDate DATE, toDate DATE)
  -- @author: TrungNT
  -- CRM-3.7 - Bao cao theo doi chi so thi truong
  AS
    sql1 CLOB;
    v_soNgayLamViec number := trunc(toDate) - trunc(fromDate) + 1;
    v_ngayDuocDuyetHang date := trunc(F_GET_DATE_NOT_DEL_ORDER(sysdate));
    cursor c_product_id is
      select distinct product_id from rpt_sale_statistic 
      where (fromDate is null or rpt_in_date >= trunc(fromDate)) and (toDate is null or rpt_in_date < trunc(toDate + 1))
      order by product_id;
  begin
  DBMS_OUTPUT.ENABLE(100000000000000);
  sql1:=sql1 || ' with shopIdTB as( ';
  sql1:=sql1 || '       select shop_id from shop start with shop_id in (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(''' || lstShopId || '''))) ';
  sql1:=sql1 || '       connect by prior shop_id = parent_shop_id ';
  sql1:=sql1 || '     ) ';
  sql1:=sql1 || '     ,nppTB as( ';
  sql1:=sql1 || '     select shop_id, shop_code, short_name, parent_shop_id  ';
  sql1:=sql1 || '     from shop s join channel_type ct on s.shop_type_id = ct.channel_type_id ';
  sql1:=sql1 || '     where s.shop_id in ( ';
  sql1:=sql1 || '       select shop_id from shopIdTB ';
  sql1:=sql1 || '     ) and ct.type = 1 and ct.object_type = 3) ';
  sql1:=sql1 || '     ,nvkdTB as( ';
  sql1:=sql1 || '     select * from ( ';
  sql1:=sql1 || '       select s.staff_id ';
  sql1:=sql1 || '           ,s.staff_code ';
  sql1:=sql1 || '           ,s.staff_name ';
  sql1:=sql1 || '           ,sh.shop_id ';
  sql1:=sql1 || '           ,case when sh.staff_owner_id is not null then sh.staff_owner_id ';
  sql1:=sql1 || '           else s.staff_owner_id end staff_owner_id ';
  sql1:=sql1 || '           ,row_number() over (partition by s.staff_id, sh.shop_id order by nvl(sh.to_date,sysdate) desc) rn, 0 priority ';
  sql1:=sql1 || '       from staff_history sh join staff s on sh.staff_id = s.staff_id and s.status in (0,1) ';
  sql1:=sql1 || '         join channel_type ct on sh.staff_type_id = ct.channel_type_id ';
  sql1:=sql1 || '       where sh.istree = 1 and sh.status in (0,1) and (''' || toDate || ''' is null or (trunc(sh.from_date) <= trunc(to_date(''' || toDate || ''')) and (sh.to_date is null or ''' || fromDate || ''' is null or trunc(sh.to_date) >= trunc(to_date(''' || fromDate || '''))))) ';
  sql1:=sql1 || '         and sh.shop_id in (select shop_id from nppTB) and ct.type = 2 and ct.object_type = 1) ';
  sql1:=sql1 || '     where rn = 1 ';
  sql1:=sql1 || '     ) ';
  sql1:=sql1 || '     ,ktnppTB as( ';
  sql1:=sql1 || '       select s.staff_id ';
  sql1:=sql1 || '             ,null staff_code ';
  sql1:=sql1 || '             ,s.staff_name ';
  sql1:=sql1 || '             ,s.shop_id ';
  sql1:=sql1 || '             ,sg.staff_id staff_owner_id ';
  sql1:=sql1 || '             ,1 rn, 1 priority ';
  sql1:=sql1 || '       from staff s  ';
  sql1:=sql1 || '         join channel_type ct_s on s.staff_type_id = ct_s.channel_type_id and ct_s.type = 2 and ct_s.object_type = 9 ';
  sql1:=sql1 || '         LEFT join (select sg.shop_id, gs.staff_id from staff_group sg ';
  sql1:=sql1 || '         join staff_group_detail sgd on sg.staff_group_id = sgd.staff_group_id and sgd.status = 1 ';
  sql1:=sql1 || '         join staff gs on sgd.staff_id = gs.staff_id and gs.status = 1 ';
  sql1:=sql1 || '         join channel_type ct_gs on gs.staff_type_id = ct_gs.channel_type_id and ct_gs.status = 1 and ct_gs.type = 2 and ct_gs.object_type = 5 where sg.status = 1 and sg.group_type = 4 and sg.group_level = 5) sg on sg.shop_id = s.shop_id ';
  sql1:=sql1 || '       where s.status in (0,1) and ';
  sql1:=sql1 || '         ct_s.status = 1 and s.shop_id in (select shop_id from nppTB) ';
  sql1:=sql1 || '          ';
  sql1:=sql1 || '     ) ';
  sql1:=sql1 || '     ,nvTB as( ';
  sql1:=sql1 || '     select * from nvkdTB union all select * from ktnppTB ';
  sql1:=sql1 || '     ) ';
  sql1:=sql1 || '     ,totalCustomerTB as( ';
  sql1:=sql1 || ' select distinct vp.staff_id, vp.shop_id, count(distinct rc.customer_id) mcp_customer  ';
  sql1:=sql1 || '   from visit_plan vp join routing_customer rc on vp.routing_id = rc.routing_id  ';
  sql1:=sql1 || '   where vp.status in (0,1) and rc.status in (0,1) and staff_id IN (SELECT staff_id FROM nvkdTB) AND shop_id IN (SELECT shop_id FROM nppTB)  ';
  sql1:=sql1 || '     and (((''' || fromDate || ''' is null or vp.from_date >= trunc(to_date(''' || fromDate || '''))) and (''' || toDate || ''' is null or trunc(vp.from_date) <= trunc(to_date(''' || toDate || '''))))  ';
  sql1:=sql1 || '       or (''' || fromDate || ''' is null or (trunc(vp.from_date) <= trunc(to_date(''' || fromDate || ''')) and (vp.to_date is null or vp.to_date >= trunc(to_date(''' || fromDate || ''')))))) ';
  sql1:=sql1 || '     and (((''' || fromDate || ''' is null or rc.start_date >= trunc(to_date(''' || fromDate || '''))) and (''' || toDate || ''' is null or trunc(rc.start_date) <= trunc(to_date(''' || toDate || '''))))  ';
  sql1:=sql1 || '       or (''' || fromDate || ''' is null or (trunc(rc.start_date) <= trunc(to_date(''' || fromDate || ''')) and (rc.end_date is null or rc.end_date >= trunc(to_date(''' || fromDate || ''')))))) ';
  sql1:=sql1 || '   group by vp.staff_id, vp.shop_id ';
  sql1:=sql1 || '     ) ';
  sql1:=sql1 || '     ,soTB as( ';
  sql1:=sql1 || '       select staff_id, shop_id, count(distinct customer_id) customer_id ';
  sql1:=sql1 || '       from sale_order where (''' || fromDate || ''' is null or order_date >= trunc(to_date(''' || fromDate || '''))) ';
  sql1:=sql1 || '           and (''' || toDate || ''' is null or order_date <= trunc(to_date(''' || toDate || ''') + 1)) and staff_id in (select staff_id from nvTB) ';
  sql1:=sql1 || '           and shop_id in (select shop_id from nppTB) and ((type = 1 and approved = 1) ';
  sql1:=sql1 || '           or (type = 1 and approved = 0 and order_date >=''' || v_ngayDuocDuyetHang || ''')) ';
  sql1:=sql1 || '       group by staff_id, shop_id ';
  sql1:=sql1 || '     ) ';
  sql1:=sql1 || '     ,rsoTB as( ';
  sql1:=sql1 || '       select staff_id, shop_id, sum(success_order) success_order ';
  sql1:=sql1 || '       from rpt_sale_order where shop_id in (select shop_id from nppTB) and staff_id in (select staff_id from nvTB) ';
  sql1:=sql1 || '         and (''' || fromDate || ''' is null or rpt_in_date >= trunc(to_date(''' || fromDate || '''))) and (''' || toDate || ''' is null or rpt_in_date < trunc(to_date(''' || toDate || ''') + 1)) ';
  sql1:=sql1 || '       group by staff_id, shop_id ';
  sql1:=sql1 || '     ) ';
  sql1:=sql1 || '     ,rrcTB as( ';
  sql1:=sql1 || '       select staff_id ';
  sql1:=sql1 || '         ,shop_id ';
  sql1:=sql1 || '         ,sum(visited_customer) visited_customer ';
  sql1:=sql1 || '         ,sum(new_customer) new_customer ';
  sql1:=sql1 || '       from rpt_routing_customer where shop_id in (select shop_id from nppTB) and staff_id in (select staff_id from nvTB) ';
  sql1:=sql1 || '         and (''' || fromDate || ''' is null or rpt_in_date >= trunc(to_date(''' || fromDate || '''))) and (''' || toDate || ''' is null or rpt_in_date < trunc(to_date(''' || toDate || ''') + 1)) ';
  sql1:=sql1 || '       group by staff_id, shop_id ';
  sql1:=sql1 || ' union all ';
  sql1:=sql1 || '   select staff_id, shop_id, null visited_customer, count(customer_id) new_customer ';
  sql1:=sql1 || '   from customer  ';
  sql1:=sql1 || '   where staff_id in (select staff_id from ktnppTB) ';
  sql1:=sql1 || '     and (''' || fromDate || ''' is null or trunc(to_date(''' || fromDate || ''')) <= trunc(approved_date)) ';
  sql1:=sql1 || '     and (''' || toDate || ''' is null or trunc(approved_date) <= trunc(to_date(''' || toDate || '''))) ';
  sql1:=sql1 || '   group by staff_id, shop_id ';
  sql1:=sql1 || '     ) ';
  sql1:=sql1 || '     ,rssTB as( ';
  sql1:=sql1 || '       select staff_id ';
  sql1:=sql1 || '         ,shop_id ';
  sql1:=sql1 || '         ,sum(day_quantity) day_quantity ';
  sql1:=sql1 || '       from rpt_sale_statistic where shop_id in (select shop_id from nppTB) and staff_id in (select staff_id from nvTB) ';
  sql1:=sql1 || '         and (''' || fromDate || ''' is null or rpt_in_date >= trunc(to_date(''' || fromDate || '''))) and (''' || toDate || ''' is null or rpt_in_date < trunc(to_date(''' || toDate || ''') + 1)) ';
  sql1:=sql1 || '       group by staff_id, shop_id ';
  sql1:=sql1 || '     ) ';
  for v_product_id in c_product_id
  loop
    sql1:=sql1 || ' ,productTB' || v_product_id.product_id || ' as ( ';
    sql1:=sql1 || ' SELECT staff_id , shop_id , SUM(day_quantity) day_quantity FROM rpt_sale_statistic ';
    sql1:=sql1 || ' WHERE shop_id IN (SELECT shop_id FROM nppTB) AND staff_id IN (SELECT staff_id FROM nvTB) ';
    sql1:=sql1 || ' and product_id = ''' || v_product_id.product_id || ''' ';
    sql1:=sql1 || ' AND (''' || fromDate || ''' IS NULL OR rpt_in_date >= TRUNC(to_date(''' || fromDate || '''))) ';
    sql1:=sql1 || ' AND (''' || toDate || ''' IS NULL OR rpt_in_date < TRUNC(to_date(''' || toDate || ''') + 1)) ';
    sql1:=sql1 || ' GROUP BY staff_id, shop_id ';
    sql1:=sql1 || ' ) ';
  end loop;
  sql1:=sql1 || ' ,cpvTB as( ';
  sql1:=sql1 || '    select staff_id, shop_id, sum(stock_quantity) stock_quantity, count(distinct customer_id) customer_id from( ';
  sql1:=sql1 || '      select cpv.staff_id ';
  sql1:=sql1 || '        ,cus.shop_id ';
  sql1:=sql1 || '        ,cpv.customer_id ';
  sql1:=sql1 || '        ,cpvd.stock_quantity ';
  sql1:=sql1 || '        ,row_number() over (partition BY cpv.col_product_volume_id, cpvd.product_id, cpv.staff_id, cus.shop_id, cpv.customer_id order by cpv.last_time DESC) rn ';
  sql1:=sql1 || '      from col_product_volume cpv join col_product_volume_detail cpvd on cpv.col_product_volume_id = cpvd.col_product_volume_id ';
  sql1:=sql1 || '        join customer cus on cpv.customer_id = cus.customer_id ';
  sql1:=sql1 || '      where cpv.staff_id in (SELECT staff_id FROM nvTB) and cus.shop_id in (SELECT shop_id FROM nppTB) ';
  sql1:=sql1 || '        and (''' || fromDate || ''' is null or trunc(to_date(''' || fromDate || ''')) <= trunc(cpv.last_time)) ';
  sql1:=sql1 || '        and (''' || toDate || ''' is null or trunc(to_date(''' || toDate || ''')) >= trunc(cpv.last_time)) ';
  sql1:=sql1 || '        and cpv.type = 1 and cpvd.stock_quantity is not null and cpvd.stock_quantity > 0) ';
  sql1:=sql1 || '    where rn = 1 ';
  sql1:=sql1 || '    group by staff_id, shop_id ';
  sql1:=sql1 || '  ), ';
  sql1:=sql1 || ' result as( ';
  sql1:=sql1 || '     select kv.short_name KhuVuc ';
  sql1:=sql1 || '         ,v.short_name Vung ';
  sql1:=sql1 || '         ,case when gsbh.staff_code is not null and gsbh.staff_code <> '' '' then ';
  sql1:=sql1 || '           gsbh.staff_code || '' - '' || gsbh.staff_name ';
  sql1:=sql1 || '         else null end GiamSatBanHang ';
  sql1:=sql1 || '         ,nppTB.shop_code MaNPP ';
  sql1:=sql1 || '         ,nppTB.short_name NPP ';
  sql1:=sql1 || '         ,nv.staff_code maNVKD';
  sql1:=sql1 || '         ,nv.staff_name NVKD';
  sql1:=sql1 || '         ,round(nvl(sum(rso.success_order),0)/''' || v_soNgayLamViec || ''',2) SoLuongDonHangBQ ';
  sql1:=sql1 || '         ,case when nv.priority is null or nv.priority <> 1 then round(nvl(sum(rrc.visited_customer),0)/ ''' || v_soNgayLamViec || ''',2) else null end SoLuongKhachHangGheThamBQ ';
  sql1:=sql1 || '         ,case ';
  sql1:=sql1 || '           when nv.priority = 1 or (nvl(sum(rso.success_order),0) = 0 and nvl(sum(rrc.visited_customer),0) = 0) then null ';
  sql1:=sql1 || '           when nvl(sum(rrc.visited_customer),0) = 0 and nvl(sum(rso.success_order),0) > 0 then 1 ';
  sql1:=sql1 || '           else round(nvl(sum(rso.success_order),0)/nvl(sum(rrc.visited_customer),0),2) end TiLeDonHangThanhCong ';
  sql1:=sql1 || '         ,case when sum(so.customer_id) is not null then nvl(sum(so.customer_id),0) else null end SoKHPhatSinhDonHang ';
  sql1:=sql1 || '         ,case when sum(rrc.new_customer) is not null then nvl(sum(rrc.new_customer),0) else null end SoKHMoMoi ';
  sql1:=sql1 || '         ,case when nv.priority = 1 then null when nvl(sum(tc.mcp_customer),0) - nvl(sum(so.customer_id),0) >=0 then sum(case when NVL(tc.mcp_customer,0) - NVL(so.customer_id,0) >= 0 THEN NVL(tc.mcp_customer,0) - NVL(so.customer_id,0) ELSE 0 end) else 0 end SoKHKhongMuaHang ';
  sql1:=sql1 || '         ,case ';
  sql1:=sql1 || '           when nvl(sum(rso.success_order),0) = 0 then null ';
  sql1:=sql1 || '           else round(nvl(sum(rss.day_quantity),0)/nvl(sum(rso.success_order),0),2) end BQSanLuongDonHang ';
  sql1:=sql1 || ' ,case ';
  sql1:=sql1 || '    when NVL(SUM(tc.mcp_customer),0) = 0 then null ';
  sql1:=sql1 || '    else ROUND(sum(case when NVL(tc.mcp_customer,0) <> 0 then nvl(cpv.stock_quantity,0)/NVL(tc.mcp_customer,0) else null end),2) end BQTonHangTrenKH ';
  sql1:=sql1 || ' ,case ';
  sql1:=sql1 || '    when NVL(SUM(tc.mcp_customer),0) = 0 and NVL(SUM(cpv.customer_id),0) = 0 then null ';
  sql1:=sql1 || '    when NVL(SUM(tc.mcp_customer),0) = 0 and NVL(SUM(cpv.customer_id),0) > 0 then 1 ';
  sql1:=sql1 || '    else ROUND(nvl(sum(cpv.customer_id),0)/NVL(SUM(tc.mcp_customer),0),2) end DoPhuBiaHaNoi ';
  sql1:=sql1 || ' ,GROUPING_ID (kv.shop_code,v.shop_code,gsbh.staff_code,nppTB.shop_code,nv.staff_code) tmp ';
  for v_product_id in c_product_id
  loop
    sql1:=sql1 || '    ,SUM(p' || v_product_id.product_id || '.day_quantity) SanPham_' || v_product_id.product_id || ' ';
  end loop;
  sql1:=sql1 || ' ,gsbh.staff_name GSBH_Name ';
  sql1:=sql1 || '       from nppTB join shop v on nppTB.parent_shop_id = v.shop_id ';
  sql1:=sql1 || '       join shop kv on v.parent_shop_id = kv.shop_id ';
  sql1:=sql1 || '       join nvTB nv on nv.shop_id = nppTB.shop_id ';
  sql1:=sql1 || '       left join staff gsbh on gsbh.staff_id = nv.staff_owner_id and gsbh.status in (0,1) ';
  sql1:=sql1 || '       left join rsoTB rso on rso.shop_id = nv.shop_id and rso.staff_id = nv.staff_id ';
  sql1:=sql1 || '       left join rrcTB rrc on nv.staff_id = rrc.staff_id and rrc.shop_id = nv.shop_id ';
  sql1:=sql1 || '       left join soTB so on nv.staff_id = so.staff_id and nv.shop_id = so.shop_id ';
  sql1:=sql1 || '       left join totalCustomerTB tc on tc.staff_id = nv.staff_id and tc.shop_id = nv.shop_id ';
  sql1:=sql1 || '       left join rssTB rss on rss.staff_id = nv.staff_id and rss.shop_id = nv.shop_id ';
  for v_product_id in c_product_id
  loop
    sql1:=sql1 || '  LEFT JOIN productTB' || v_product_id.product_id || ' p' || v_product_id.product_id || '  ';
    sql1:=sql1 || '  ON p' || v_product_id.product_id || '.staff_id = nv.staff_id AND p' || v_product_id.product_id || '.shop_id = nv.shop_id ';
  end loop;
  sql1:=sql1 || ' left join cpvTB cpv on cpv.staff_id = nv.staff_id and cpv.shop_id = nv.shop_id ';
  sql1:=sql1 || '       group by rollup((kv.shop_code,kv.short_name),(v.shop_code,v.short_name),(gsbh.staff_code,gsbh.staff_name),(nppTB.shop_code,nppTB.short_name),(nv.staff_code,nv.staff_name,nv.priority)) ';
  sql1:=sql1 || '       order by  kv.shop_code,v.shop_code,gsbh.staff_code,nppTB.shop_code,tmp,priority';
  sql1:=sql1 || ' ) ';
  sql1:=sql1 || ' select case  ';
  sql1:=sql1 || '         when tmp = 15 then ''T?ng '' || result.khuvuc ';
  sql1:=sql1 || '         when tmp = 31 then ''T?ng'' || result.khuvuc ';
  sql1:=sql1 || '         else result.khuvuc end KhuVuc ';
  sql1:=sql1 || '       ,case when tmp = 7 then ''T?ng '' || result.vung else result.vung end Vung ';
  sql1:=sql1 || '       ,case when tmp = 3 then ''T?ng '' || result.GSBH_NAME else result.GiamSatBanHang end GiamSatBanHang ';
  sql1:=sql1 || '       ,result.MaNPP ';
  sql1:=sql1 || '       ,case when tmp = 1 then ''T?ng '' || result.NPP else result.NPP end NPP ';
  sql1:=sql1 || '       ,result.maNVKD ';
  sql1:=sql1 || '       ,result.NVKD ';
  sql1:=sql1 || '       ,result.SoLuongDonHangBQ ';
  sql1:=sql1 || '       ,result.SoLuongKhachHangGheThamBQ ';
  sql1:=sql1 || '       ,result.TiLeDonHangThanhCong ';
  sql1:=sql1 || '       ,result.SoKHPhatSinhDonHang ';
  sql1:=sql1 || '       ,result.SoKHMoMoi ';
  sql1:=sql1 || '       ,result.SoKHKhongMuaHang ';
  sql1:=sql1 || '       ,result.BQSanLuongDonHang ';
  sql1:=sql1 || '       ,result.BQTonHangTrenKH ';
  sql1:=sql1 || '       ,result.DoPhuBiaHaNoi ';
  sql1:=sql1 || '       ,result.tmp ';
  for v_product_id in c_product_id
  loop
    sql1:=sql1 || '       ,result.SanPham_' || v_product_id.product_id || ' ';
  end loop;
  sql1:=sql1 || ' from result ';
  DBMS_OUTPUT.PUT_LINE(sql1);
  OPEN res FOR (sql1);
  END P_CRM_3_7;
  
  PROCEDURE P_CRM_3_8(res OUT SYS_REFCURSOR, lstShopId CLOB, toDate DATE)
  -- @author: TrungNT
  -- CRM-3.8 - B�o c�o tong hop cac chi so thi truong hang ngay
  as
    fromDate date := trunc(toDate,'month');
  begin
    open res for
      with shopIdTB as
      (select shop_id from shop start with shop_id in (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(lstShopId))) 
        connect by prior shop_id = parent_shop_id 
      ) 
      ,nppTB as
      (select shop_id, shop_code, short_name, parent_shop_id  
      from shop s join channel_type ct on s.shop_type_id = ct.channel_type_id 
      where s.shop_id in (
        select shop_id from shopIdTB 
      ) and ct.type = 1 and ct.object_type = 3) 
      ,nvTB as
      (select * from (
        select s.staff_id 
            ,s.staff_code 
            ,s.staff_name 
            ,sh.shop_id 
            ,case when sh.staff_owner_id is not null then sh.staff_owner_id 
            else s.staff_owner_id end staff_owner_id 
            ,row_number() over (partition by s.staff_id order by nvl(sh.to_date,sysdate) desc) rn
        from staff_history sh join staff s on sh.staff_id = s.staff_id and s.status in (0,1)
          join channel_type ct on sh.staff_type_id = ct.channel_type_id 
        where sh.istree = 1 and sh.status in (0,1) and trunc(sh.from_date) <= trunc(toDate) and (sh.to_date is null or trunc(sh.to_date) >= trunc(fromDate))
          and sh.shop_id in (select shop_id from nppTB) and ct.type = 2 and ct.object_type = 1) 
      where rn = 1 
      )
      ,rrcTB as
      (select staff_id, count(rpt_in_date) thLuyKeNCLV, sum(visited_customer) thLuyKeGheTham
      from rpt_routing_customer
      where staff_id in (select staff_id from nvTB) and shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(fromDate) and rpt_in_date < trunc(toDate) + 1 and visited_customer > 0
      group by staff_id
      )
      ,doTB as
      (select staff_id
          ,case when trunc(from_date) >= fromDate then trunc(from_date) else fromDate end from_date
          ,case when trunc(to_date) >= trunc(toDate) then trunc(toDate) else trunc(to_date) end to_date
          ,is_full_time 
      from day_off
      where fromDate <= trunc(to_date) and trunc(toDate) >= trunc(from_date) and status = 1 and staff_id in (select staff_id from nvTB)
      )
      ,dayOffTB as 
      (select staff_id, sum(case when is_full_time is not null and is_full_time = 0 then 0.5 else to_date - from_date + 1 end) ngayNghi
      from doTB group by staff_id
      )
      ,viPhamLuyKeTB as
      (select staff_id, sum(npp_time+first_cus_late+last_cus_early) viPham
      from rpt_staff_fault
      where staff_id in (select staff_id from nvTB) and shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(fromDate) and rpt_in_date < trunc(toDate) + 1
      group by staff_id
      )
      ,sanLuongLuyKeNgayTruocTB as
      (select rss.staff_id, sum(rss.day_quantity*rss.CONVFACT*p.VOLUMN) thLuyKeNgayTruoc 
      from rpt_sale_statistic rss join product p on rss.product_id = p.product_id
      where rss.staff_id in (select staff_id from nvTB) and rss.shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(fromDate) and rpt_in_date < trunc(toDate)
      group by rss.staff_id
      )
      ,sanLuongNgayTB as
      (select rss.staff_id, sum(rss.day_quantity*rss.CONVFACT*p.VOLUMN) thLuyKeNgay
      from rpt_sale_statistic rss join product p on rss.product_id = p.product_id
      where rss.staff_id in (select staff_id from nvTB) and rss.shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(toDate) and rpt_in_date < trunc(toDate) + 1
      group by rss.staff_id
      )
      ,soLuongDonHangNgayTB as
      (select staff_id, sum(success_order) thNgay
      from rpt_sale_order
      where staff_id in (select staff_id from nvTB) and shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(toDate) and rpt_in_date < trunc(toDate) + 1
      group by staff_id
      )
      ,soLuongDonHangLuyKeNgayTruocTB as
      (select staff_id, sum(success_order) thLuyKeNgayTruoc
      from rpt_sale_order
      where staff_id in (select staff_id from nvTB) and shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(fromDate) and rpt_in_date < trunc(toDate)
      group by staff_id
      )
      ,pdpTB as
      (select distinct pdp.staff_id, pdp.customer_id
      from col_product_volume pdp join customer cus on pdp.customer_id = cus.customer_id
        join col_product_volume_media cpvm on pdp.col_product_volume_id = cpvm.col_product_volume_id
          and cpvm.create_date >= trunc(fromDate) and cpvm.create_date < trunc(toDate)
          and cpvm.type = 1 and cpvm.status = 1
      where pdp.staff_id in (select staff_id from nvTB) and cus.shop_id in (select shop_id from nppTB)
        and pdp.collect_date >= trunc(fromDate) and pdp.collect_date < trunc(fromDate) + 1 and pdp.type = 1
      )
      ,soTrungBayLuyKeNgayTruocTB as
      (select pdp.staff_id, count(pdp.customer_id) thLuyKeNgayTruoc
      from pdpTB pdp
      group by pdp.staff_id
      )
      ,soTrungBayNgayTB as
      (select pdp.staff_id, count(distinct pdp.customer_id) thNgay
      from col_product_volume pdp join customer cus on pdp.customer_id = cus.customer_id
        join col_product_volume_media cpvm on pdp.col_product_volume_id = cpvm.col_product_volume_id
          and cpvm.create_date >= trunc(toDate) and cpvm.create_date < trunc(toDate) + 1
          and cpvm.type = 1 and cpvm.status = 1
      where pdp.staff_id in (select staff_id from nvTB) and cus.shop_id in (select shop_id from nppTB)
        and pdp.collect_date >= trunc(fromDate) and pdp.collect_date < trunc(fromDate) + 1 and pdp.type = 1
        and pdp.customer_id not in (select customer_id from pdpTB)
      group by pdp.staff_id
      )
      ,result as
      (select kv.short_name KhuVuc
          ,v.short_name Vung 
          ,case when gsbh.staff_code is not null and gsbh.staff_code <> ' ' then 
            gsbh.staff_code || ' - ' || gsbh.staff_name 
          else null end GSBH
          ,nv.staff_code maNVKD
          ,nv.staff_name NVKD
          ,sum(qd.plan_quantity) NCLVQuiDinh
          ,sum(do.ngayNghi) NCLVSoNgayDangKyNghi
          ,sum(rrc.thLuyKeNCLV) NCLVTHLuyKe
          ,case 
            when nvl(sum(qd.plan_quantity),0) = 0 and nvl(sum(rrc.thLuyKeNCLV),0) = 0 then null
            when nvl(sum(qd.plan_quantity),0) = 0 and nvl(sum(rrc.thLuyKeNCLV),0) > 0 then 1
            else round(sum(rrc.thLuyKeNCLV)/nvl(sum(qd.plan_quantity),0),2) end THQuyDinhNCLV
          ,sum(rsf.npp_time + rsf.first_cus_late + rsf.last_cus_early) SoLanViPhamNgay
          ,sum(vplk.viPham) SoLanViPhamLuyKe
          ,case 
            when sum(slbhKHThang.plan_quantity) is not null and sum(slbhKHThang.plan_quantity) > 0 then sum(slbhKHThang.plan_quantity)
            else null end SanLuongBanHangKHThang
          ,case
            when nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0) > 0 
              then round((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) / (trunc(last_day(toDate)) - trunc(toDate) + 1),2)
              else null end SanLuongBanHangKHNgay
          ,case 
            when nvl(sum(slbhNgay.thLuyKeNgay),0) > 0 then round(nvl(sum(slbhNgay.thLuyKeNgay),0),2)
            else null end SanLuongBanHangTHNgay
          ,case
            when nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0) <= 0
              and nvl(sum(slbhNgay.thLuyKeNgay),0) = 0 then null
            when nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0) <= 0
              and nvl(sum(slbhNgay.thLuyKeNgay),0) > 0 then 1
            else round(nvl(sum(slbhNgay.thLuyKeNgay),0)/((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2)
          end SanLuongBanHangTHKHNgay
          ,case
            when nvl(sum(slbhNgay.thLuyKeNgay),0) = 0 and nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0) = 0 then null
            else (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) end SanLuongBanHangTHLuyKe
          ,case
            when nvl(sum(slbhKHThang.plan_quantity),0) = 0 and (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) = 0
              then null
            when nvl(sum(slbhKHThang.plan_quantity),0) = 0 and (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) > 0
              then 1
            else round((nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) / sum(slbhKHThang.plan_quantity),2)
          end SanLuongBanHangTHKHThang
          ,case
            when trunc(last_day(toDate)) - trunc(toDate) = 0 and sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) > 0
              then round((sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))),2)
            when trunc(last_day(toDate)) - trunc(toDate) = 0 and sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) <= 0
              then 0
            when sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) > 0
             then round((sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)))/(trunc(last_day(toDate)) - trunc(toDate)),2)
            else null end SanLuongBanHangKeHoachConLai
          ,case 
            when nvl(sum(slgtNgay.total_customer),0) > 0 then sum(slgtNgay.total_customer)
            else null end SoLuongDiemBanKHNgay
          ,case
            when nvl(sum(slgtNgay.visited_customer),0) > 0 then sum(slgtNgay.visited_customer)
            else null end SoLuongDiemBanTHNgay
          ,case
            when nvl(sum(slgtNgay.total_customer),0) = 0 and nvl(sum(slgtNgay.visited_customer),0) = 0 then null
            when nvl(sum(slgtNgay.total_customer),0) = 0 and nvl(sum(slgtNgay.visited_customer),0) > 0 then 1
            else round(nvl(sum(slgtNgay.visited_customer),0)/nvl(sum(slgtNgay.total_customer),0),2) end SoLuongDiemBanTHKHNgay
          ,sum(rrc.thLuyKeGheTham) SoLuongDiemBanBQTHNgayLuyKe
          ,case
            when nvl(sum(slgtKHThang.plan_quantity),0) = 0 and nvl(sum(rrc.thLuyKeGheTham),0) = 0 then null
            when nvl(sum(slgtKHThang.plan_quantity),0) = 0 and nvl(sum(rrc.thLuyKeGheTham),0) > 0 then 1
            else round((nvl(sum(rrc.thLuyKeGheTham),0)/sum(slgtKHThang.plan_quantity)),2) end SoLuongDiemBanBQTHLuyKeKH
          ,case
            when nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0 then 0
            else round((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1),2)
          end SoLuongDonHangKHNgay
          ,case 
            when nvl(sum(sldhNgay.thNgay),0) >0 then sum(sldhNgay.thNgay)
            else null end SoLuongDonHangTHNgay
          ,case
            when nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0
              and nvl(sum(sldhNgay.thNgay),0) = 0 then null
            when nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0
              and nvl(sum(sldhNgay.thNgay),0) > 0 then 1
            else round(nvl(sum(sldhNgay.thNgay),0)/round(((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2),2)
          end SoLuongDonHangTHKHNgay
          ,case
            when nvl(sum(sldhNgay.thNgay),0) = 0 and nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) = 0 then null
            else (nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)) end SoLuongDonHangBQTHNgayLuyKe
          ,case
            when nvl(sum(sldhKHThang.plan_quantity),0) = 0 and (nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)) = 0
              then null
            when nvl(sum(sldhKHThang.plan_quantity),0) = 0 and (nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)) > 0
              then 1
            else round((nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)) / sum(sldhKHThang.plan_quantity),2)
          end SoLuongDonHangBQTHLuyKeKH
          ,case
            when nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0 or
            ((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))
              /((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)) <= 0 then null
            else round(round(((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2)
              /round(((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2),2)
          end GiaTriBinhQuanDonHangKHNgay
          ,case
            when nvl(sum(sldhNgay.thNgay),0) = 0 then null
            else round(nvl(sum(slbhNgay.thLuyKeNgay),0)/sum(sldhNgay.thNgay),2) end GiaTriBinhQuanDonHangTHNgay
          ,case
            when nvl(sum(sldhNgay.thNgay),0) = 0 or 
            ((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0 or
            ((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))
              /((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)) <= 0)
              and nvl(sum(slbhNgay.thLuyKeNgay),0)/sum(sldhNgay.thNgay) = 0) then null
            when (nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0 or
            ((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))
              /((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)) <= 0)
              and nvl(sum(slbhNgay.thLuyKeNgay),0)/sum(sldhNgay.thNgay) > 0 then 1
            else round((nvl(sum(slbhNgay.thLuyKeNgay),0)/sum(sldhNgay.thNgay))/
              (((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))
              /((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))),2)
          end GiaTriBinhQuanDonHangTHKH
          ,case
            when nvl(sum(sldhNgay.thNgay),0) = 0 and nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) = 0 then null
            else round((nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)),2)
          end GiaTriBinhQuanDonHangTHLuyKe
          ,case
            when nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0) <= 0 then null
            else round((nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1),2)
          end SoDiemBanTrungBayKHNgay
          ,sum(stbNgay.thNgay) SoDiemBanTrungBayTHNgay
          ,case
            when nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0) <= 0 
              and nvl(sum(stbNgay.thNgay),0) = 0 then null
            when nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0) <= 0 
              and nvl(sum(stbNgay.thNgay),0) > 0 then 1
            else round(nvl(sum(stbNgay.thNgay),0)/((nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2)
          end SoDiemBanTrungBayTHKHNgay
          ,case 
            when nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0)+nvl(sum(stbNgay.thNgay),0) > 0 then nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0)+nvl(sum(stbNgay.thNgay),0)
            else null end SoDiemBanTrungBayBQTHNgayLuyKe
          ,case
            when nvl(sum(stbKHThang.plan_quantity),0) = 0 then null
            else round((nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0)+nvl(sum(stbNgay.thNgay),0))/nvl(sum(stbKHThang.plan_quantity),0),2)
          end SoDiemBanTrungBayBQTHLuyKeKH
          ,GROUPING_ID (kv.shop_code,v.shop_code,gsbh.staff_code,nv.staff_code) tmp
      from nppTB join shop v on nppTB.parent_shop_id = v.shop_id 
        join shop kv on v.parent_shop_id = kv.shop_id 
        join nvTB nv on nv.shop_id = nppTB.shop_id 
        left join staff gsbh on gsbh.staff_id = nv.staff_owner_id and gsbh.status in (0,1)
        left join kpi_result_detail qd on qd.object_type = 2 and qd.object_id = nv.staff_id and qd.kpi_id = 14
          and qd.month = extract(month from toDate) and qd.year = extract(year from toDate)
        left join rrcTB rrc on rrc.staff_id = nv.staff_id
        left join dayOffTB do on do.staff_id = nv.staff_id
        left join rpt_staff_fault rsf on rsf.staff_id = nv.staff_id and rsf.rpt_in_date >= trunc(toDate)
          and rsf.rpt_in_date < trunc(toDate) + 1
        left join viPhamLuyKeTB vplk on vplk.staff_id = nv.staff_id
        left join kpi_result_detail slbhKHThang on slbhKHThang.object_type = 2 and slbhKHThang.object_id = nv.staff_id
          and slbhKHThang.kpi_id = 1 and slbhKHThang.month = extract(month from toDate) and slbhKHThang.year = extract(year from toDate)
        left join sanLuongLuyKeNgayTruocTB slbhNgayTruoc on slbhNgayTruoc.staff_id = nv.staff_id
        left join sanLuongNgayTB slbhNgay on slbhNgay.staff_id = nv.staff_id
        left join rpt_routing_customer slgtNgay on slgtNgay.staff_id = nv.staff_id and slgtNgay.rpt_in_date >= trunc(toDate)
          and slgtNgay.rpt_in_date < trunc(toDate) + 1
        left join kpi_result_detail slgtKHThang on slgtKHThang.object_type = 2 and slgtKHThang.object_id = nv.staff_id 
          and slgtKHThang.kpi_id = 3 and slgtKHThang.month = extract(month from toDate) and slgtKHThang.year = extract(year from toDate)
        left join kpi_result_detail sldhKHThang on sldhKHThang.object_type = 2 and sldhKHThang.object_id = nv.staff_id 
          and sldhKHThang.kpi_id = 15 and sldhKHThang.month = extract(month from toDate) and sldhKHThang.year = extract(year from toDate)
        left join soLuongDonHangLuyKeNgayTruocTB sldhNgayTruoc on sldhNgayTruoc.staff_id = nv.staff_id
        left join soLuongDonHangNgayTB sldhNgay on sldhNgay.staff_id = nv.staff_id
        left join kpi_result_detail stbKHThang on stbKHThang.object_type = 2 and stbKHThang.object_id = nv.staff_id 
          and stbKHThang.kpi_id = 16 and stbKHThang.month = extract(month from toDate) and stbKHThang.year = extract(year from toDate)
        left join soTrungBayLuyKeNgayTruocTB stbNgayTruoc on stbNgayTruoc.staff_id = nv.staff_id
        left join soTrungBayNgayTB stbNgay on stbNgay.staff_id = nv.staff_id
      group by rollup((kv.shop_code,kv.short_name),(v.shop_code,v.short_name),(gsbh.staff_code,gsbh.staff_name),(nv.staff_code,nv.staff_name))
      )
      select case 
              when tmp = 15 then 'T?ng' || KhuVuc
              when tmp = 7 then 'T?ng ' || KhuVuc
              else KhuVuc end KhuVuc
            ,case when tmp = 3 then 'T?ng ' || Vung else Vung end Vung
            ,case when tmp = 1 then 'T?ng ' || GSBH else GSBH end GSBH
            ,maNVKD
            ,NVKD
            ,NCLVQuiDinh
            ,NCLVSoNgayDangKyNghi
            ,NCLVTHLuyKe
            ,THQuyDinhNCLV
            ,SoLanViPhamNgay
            ,SoLanViPhamLuyKe
            ,SanLuongBanHangKHThang
            ,SanLuongBanHangKHNgay
            ,SanLuongBanHangTHNgay
            ,SanLuongBanHangTHKHNgay
            ,SanLuongBanHangTHLuyKe
            ,SanLuongBanHangTHKHThang
            ,SanLuongBanHangKeHoachConLai
            ,SoLuongDiemBanKHNgay
            ,SoLuongDiemBanTHNgay
            ,SoLuongDiemBanTHKHNgay
            ,SoLuongDiemBanBQTHNgayLuyKe
            ,SoLuongDiemBanBQTHLuyKeKH
            ,SoLuongDonHangKHNgay
            ,SoLuongDonHangTHNgay
            ,SoLuongDonHangTHKHNgay
            ,SoLuongDonHangBQTHNgayLuyKe
            ,SoLuongDonHangBQTHLuyKeKH
            ,GiaTriBinhQuanDonHangKHNgay
            ,GiaTriBinhQuanDonHangTHNgay
            ,GiaTriBinhQuanDonHangTHKH
            ,GiaTriBinhQuanDonHangTHLuyKe
            ,SoDiemBanTrungBayKHNgay
            ,SoDiemBanTrungBayTHNgay
            ,SoDiemBanTrungBayTHKHNgay
            ,SoDiemBanTrungBayBQTHNgayLuyKe
            ,SoDiemBanTrungBayBQTHLuyKeKH
            ,tmp
      from result;
  end P_CRM_3_8;
END PKG_CRM_REPORT;