create or replace PROCEDURE P_RPT_STAFF_FAULT (dateInput DATE)
-- @authOR: NaLD
-- Tien trinh chay 15p/lan
-- Tong hop vi pham cua NVBH trong ngay truyen vao dateInput
IS
  rptID NUMBER (20,0);
  checkinsert NUMBER(2,0);
  
  nppTime NUMBER(1);
  firstCusLate NUMBER(1);
  lastCusEarly NUMBER(1);
  routingTime NUMBER(1);
  CONST_ROUTINGTIME NUMBER(10) := 8;

  cc_start VARCHAR2(20);
  cc_end VARCHAR2(20);
  cc_distance VARCHAR2(20);
  dt_start VARCHAR2(20);
  dt_end VARCHAR2(20);
  npp_lat NUMBER(30, 20);
  npp_lng NUMBER(30, 20);

  -- Lay ra tap NVKD
  CURSOR c_DuLieu
  IS
    SELECT st.staff_id, vp.shop_id
    FROM staff st 
    JOIN channel_type ct ON ct.channel_type_id = st.staff_type_id
    join visit_plan vp on vp.staff_id = st.staff_id and trunc(vp.from_date) <= trunc(dateInput)
      and (vp.to_date is null or vp.to_date >= trunc(dateInput))  and vp.status in (0,1)
    WHERE ct.type = 2 AND ct.object_type IN (1) -- NVKD, TTTT
      AND st.status in (0, 1)
      ;
  
  CURSOR c_DeleteData
  IS
    WITH
    dl AS(
      SELECT st.staff_id, vp.shop_id
    FROM staff st 
    JOIN channel_type ct ON ct.channel_type_id = st.staff_type_id
    join visit_plan vp on vp.staff_id = st.staff_id and trunc(vp.from_date) <= trunc(dateInput)
      and (vp.to_date is null or vp.to_date >= trunc(dateInput)) and vp.status in (0,1)
    WHERE ct.type = 2 AND ct.object_type IN (1) -- NVKD, TTTT
      AND st.status in (0, 1)
    )
    SELECT rpt.rpt_staff_fault_id 
    FROM rpt_staff_fault rpt 
    WHERE rpt.rpt_in_date >= TRUNC(dateInput) AND rpt.rpt_in_date < TRUNC(dateInput) + 1
    AND NOT EXISTS (SELECT 1 FROM dl WHERE dl.staff_id = rpt.staff_id AND dl.shop_id = rpt.shop_id)
    ;
  
  -- Lay ra tham so cau hinh SHOP_PARAM cua NPP dua vao id shop v_shopId
  CURSOR c_ShopParam (v_shopId NUMBER)
  IS
    SELECT
    (SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'CC_START') cc_start
    ,(SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'CC_END') cc_end
    ,(SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'CC_DISTANCE') cc_distance
    ,(SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'DT_START') dt_start
    ,(SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'DT_END') dt_end
    ,(SELECT s.lat FROM shop s WHERE s.shop_id = v_shopId) npp_lat
    ,(SELECT s.lng FROM shop s WHERE s.shop_id = v_shopId) npp_lng
    FROM dual
    ;
  
BEGIN
  DBMS_OUTPUT.PUT_LINE('P_RPT_STAFF_FAULT started');
  FOR v IN c_DuLieu
  LOOP
    nppTime := 0;
    firstCusLate := 0;
    lastCusEarly := 0;
    routingTime := 0;

    OPEN c_ShopParam(v.shop_id);
    FETCH c_ShopParam INTO cc_start, cc_end, cc_distance,dt_start,dt_end, npp_lat, npp_lng;
    CLOSE c_ShopParam;

    -- Tinh cho cot NPP_TIME
    if systimestamp > to_timestamp(to_char(dateInput, 'dd/mm/yyyy') || ' ' || to_char(to_date(cc_end, 'hh24:mi'), 'hh24:mi:ss'), 'dd/mm/yyyy HH24:MI:SS') then
      BEGIN
        SELECT spl.staff_position_log_id INTO rptID FROM staff_position_log spl
        WHERE spl.staff_id = v.staff_id AND spl.create_date >= TRUNC(dateInput) AND spl.create_date < TRUNC(dateInput) + 1
        AND to_timestamp(to_char(spl.create_date,'HH24:MI:SS'), 'HH24:MI:SS') > to_timestamp(to_char(to_date(cc_start, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')
        AND to_timestamp(to_char(spl.create_date,'HH24:MI:SS'), 'HH24:MI:SS') < to_timestamp(to_char(to_date(cc_end, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')
        AND F_GET_DISTANCE_COORDINATE(npp_lat, npp_lng, spl.lat, spl.lng) <= cc_distance
        ;
        -- Khi khong tim thay dong nay chung to Staff vi pham noi dung nay
        EXCEPTION WHEN NO_DATA_FOUND THEN nppTime := 1; rptID := null;
        -- Khi tim thay nhieu hon 1 dong thi Staff khong vi pham noi dung nay
                  WHEN TOO_MANY_ROWS THEN nppTime := 0; rptID := null;
      END;
      if rptID is not null then
        nppTime := 0;
      end if;
    else
      nppTime := 0;
    end if;

    -- Tinh cho cot FIRST_CUS_LATE ghe tham khach hang dau tien muon
    if systimestamp > to_timestamp(to_char(dateInput, 'dd/mm/yyyy') || ' ' || to_char(to_date(dt_start, 'hh24:mi'), 'hh24:mi:ss'), 'dd/mm/yyyy HH24:MI:SS') then
      BEGIN
        SELECT action_log_id INTO rptID
        FROM action_log al 
        WHERE al.staff_id = v.staff_id
        AND al.start_time >= TRUNC(dateInput) AND al.start_time < TRUNC(dateInput) + 1
        AND al.is_or = 0 AND al.object_type in (0,1) -- Khach hang trong tuyen, loai ghe tham: ghe tham, ghe tham dong cua
        AND to_timestamp(to_char(al.start_time,'HH24:MI:SS'), 'HH24:MI:SS') < to_timestamp(to_char(to_date(dt_start, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')
        ;
        -- Khi khong tim thay dong nay chung to Staff vi pham noi dung nay
        EXCEPTION WHEN NO_DATA_FOUND THEN firstCusLate := 1; rptID := null;
        -- Khi tim thay nhieu hon 1 dong thi Staff khong vi pham noi dung nay
                  WHEN TOO_MANY_ROWS THEN firstCusLate := 0; rptID := null;
      END;
      if rptID is not null then
        firstCusLate := 0;
      end if;
    else
      firstCusLate := 0;
    end if;

    -- Tinh cho cot LAST_CUS_LATE ket thuc ghe tham khach hang cuoi cung som
    if systimestamp > to_timestamp(to_char(dateInput, 'dd/mm/yyyy') || ' ' || to_char(to_date(dt_end, 'hh24:mi'), 'hh24:mi:ss'), 'dd/mm/yyyy HH24:MI:SS') then
      BEGIN
        SELECT t.action_log_id INTO rptID
        FROM (
        SELECT action_log_id, start_time, end_time,
               RANK() OVER (PARTITION BY staff_id, trunc(start_time) ORDER BY start_time desc) rank
        FROM action_log al
        WHERE al.staff_id = v.staff_id
        AND al.start_time >= TRUNC(dateInput) AND al.start_time < TRUNC(dateInput) + 1
        AND al.is_or = 0 AND al.object_type in (0,1) -- Khach hang trong tuyen, loai ghe tham: ghe tham, ghe tham dong cua
        ) t
        WHERE t.rank = 1
        AND ((t.end_time IS NOT NULL and (to_timestamp(to_char(t.end_time,'HH24:MI:SS'), 'HH24:MI:SS') > to_timestamp(to_char(to_date(dt_end, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')))
          OR (t.end_time IS NULL and to_timestamp(to_char(t.start_time,'HH24:MI:SS'), 'HH24:MI:SS') > to_timestamp(to_char(to_date(dt_end, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')))
        ;
        -- Khi khong tim thay dong nay chung to Staff vi pham noi dung nay
        EXCEPTION WHEN NO_DATA_FOUND THEN lastCusEarly := 1; rptID := null;
        -- Khi tim thay nhieu hon 1 dong thi Staff khong vi pham noi dung nay
                  WHEN TOO_MANY_ROWS THEN lastCusEarly := 0; rptID := null;
      END;
      if rptID is not null then
        lastCusEarly := 0;
      end if;
    else
      lastCusEarly := 0;
    end if;

    -- Tinh cho cot ROUTING_TIME 
    BEGIN
      SELECT 1 INTO rptID
      FROM (
        SELECT
        ((
          SELECT CASE WHEN t.end_time IS NOT NULL THEN t.end_time
                  ELSE t.start_time END end_time
          FROM (
          SELECT action_log_id, start_time, end_time,
                 RANK() OVER (PARTITION BY staff_id, trunc(start_time) ORDER BY start_time desc) rank
          FROM action_log al
          WHERE al.staff_id = v.staff_id
          AND al.start_time >= TRUNC(dateInput) AND al.start_time < TRUNC(dateInput) + 1
          AND al.is_or = 0 AND al.object_type in (0,1) -- Khach hang trong tuyen, loai ghe tham: ghe tham, ghe tham dong cua
          ) t
          WHERE t.rank = 1
        )
        -
        (
          SELECT t.start_time
          FROM (
          SELECT action_log_id, start_time,
                 RANK() OVER (PARTITION BY staff_id, trunc(start_time) ORDER BY start_time) rank
          FROM action_log al
          WHERE al.staff_id = v.staff_id
          AND al.start_time >= TRUNC(dateInput) AND al.start_time < TRUNC(dateInput) + 1
          AND al.is_or = 0 AND al.object_type in (0,1) -- Khach hang trong tuyen, loai ghe tham: ghe tham, ghe tham dong cua
          ) t
          WHERE t.rank = 1
        )) * 24 value
        FROM dual
      )t
      WHERE t.value >= CONST_ROUTINGTIME
      ;
      -- Khi khong tim thay dong nay chung to Staff vi pham noi dung nay
      EXCEPTION WHEN NO_DATA_FOUND THEN routingTime := 1;
    END;

    checkinsert := 0;
    BEGIN
      SELECT rpt.rpt_staff_fault_id INTO rptID FROM rpt_staff_fault rpt 
      WHERE rpt.staff_id = v.staff_id AND rpt.rpt_in_date >= TRUNC(dateInput) AND rpt.rpt_in_date < TRUNC(dateInput) + 1;
      
      EXCEPTION WHEN NO_DATA_FOUND THEN checkinsert := 1;
    END;

    IF (checkinsert = 1) THEN
      INSERT INTO rpt_staff_fault (CREATE_DATE,CREATE_USER,FIRST_CUS_LATE,LAST_CUS_EARLY,NPP_TIME,ROUTING_TIME,RPT_IN_DATE,RPT_STAFF_FAULT_ID,SHOP_ID,STAFF_ID)
      VALUES (
        SYSDATE -- create_date
        ,'P_RPT_STAFF_FAULT' -- CREATE_USER
        ,firstCusLate -- FIRST_CUS_LATE
        ,lastCusEarly -- LAST_CUS_EARLY
        ,nppTime      -- NPP_TIME
        ,routingTime  -- ROUTING_TIME
        ,dateInput  -- RPT_IN_DATE
        ,RPT_STAFF_FAULT_SEQ.nextval -- RPT_STAFF_FAULT_ID
        ,v.shop_id  -- SHOP_ID
        ,v.staff_id -- STAFF_ID
        );
      DBMS_OUTPUT.PUT_LINE('INSERT staff_id = ' || v.staff_id);
    ELSE
      UPDATE rpt_staff_fault rpt
      SET FIRST_CUS_LATE = firstCusLate
          ,LAST_CUS_EARLY = lastCusEarly
          ,NPP_TIME = nppTime
          ,ROUTING_TIME = routingTime
          ,UPDATE_DATE = SYSDATE
          ,UPDATE_USER = 'P_RPT_STAFF_FAULT'
      WHERE rpt.rpt_staff_fault_id = rptID;
      DBMS_OUTPUT.PUT_LINE('UPDATE staff_id = ' || v.staff_id || ' and rpt_staff_fault_id = ' || rptID);
    END IF;
    COMMIT;
  END LOOP;
  
  FOR v IN c_DeleteData
  LOOP
    DELETE FROM rpt_staff_fault rpt WHERE rpt.rpt_staff_fault_id = v.rpt_staff_fault_id;
    DBMS_OUTPUT.PUT_LINE('DELETE rpt_staff_fault_id = ' || v.rpt_staff_fault_id);
  END LOOP;
  COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      BEGIN
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE('Co loi trong qua trinh xu ly');
      END;
  DBMS_OUTPUT.PUT_LINE('P_RPT_STAFF_FAULT finished');
END;